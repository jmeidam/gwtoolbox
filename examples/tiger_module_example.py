#! python

__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

from os import environ,path

from gwtoolbox.tiger import utils as tu

"""
Example demonstrating usage of the tiger module
This can be run on any TIGER project, the path used
in this script should be available on CIT
"""

#if True, the rundir will be scanned
#if False, it is assumed the "outfile" has already been generated and will be read instead
runfromscratch=True

#location of folders like GR and dchi1dchi2
rundir="/home/jmeidam/tiger_runs/RealDataBackground_SpinAndTidalEffects"

#subhypotheses will be constructed assuming these parameters were included
parameters=["dchi1","dchi2","dchi3"]

#The Bayes dir: for a pre-hdf5 lalinference run this can be "engine" or "posterior_files"
bdir="engine"
#The type of the Bayes files, choose 1 if files came from posterior_files, 2 otherwise
bfiletype=2

#File to write the collected data to
outfile=path.join(environ["HOME"],"RealDataBackground.dat")

#This particular example run used timeslides and these can be included as well
timeslidesfile=path.join(rundir,"timeslides_V1H1L1_2500.dat")

#set a filename if a plot of the background should be made
plot=""



if runfromscratch:
  #The main analysis run happens here. All the folders of subhypotheses
  #are scanned for each event to find Bayes files.
  #At the moment this is a slow inefficient process
  p=tu.tiger_project(parameters,rundir=rundir,bdir=bdir,bfiletype=bfiletype,
                     outfile=outfile,timeslidesfile=timeslidesfile,verbose=True)
  #write data to file. By default the specified outfile is used, by some other
  #name may be provided with filename=...
  p.write()

else:
  #Assuming the abaove was already run once, we can instead read all the relevant
  #data from the resulting file.
  p=tu.tiger_project(['dchi1','dchi2','dchi3'],empty=True)
  p.read(outfile)
  p.write(outfile+"REGENERATED")



if plot!="":

  from gwtoolbox.visualization import plotting as pl
  #some events are known to be bad due to glitches
  glitches=[967152570,968938823,967876799,969182998,970804812,969100904,966944343,971392667,970687764]

  settings={"projectdatafile":outfile,
            "figpath":plot,
            "parameters":parameters,
            "corruptionlimit":1000.0,
            "BayesCut":32.0,
            "gps_glitches":glitches}

  blue = pl.TableauColors_Tableau20[0]

  def plot_background(events,settings,xlim=[],bins=40):
    odds=[]
    for e in events:
      if e.hypotheses['GR'].Bayes > settings["BayesCut"] and e.gps not in settings["gps_glitches"]:
        odds.append(e.LogOdds)
    fig=pl.custom_figure()
    fig.add_hist(odds,label=r"$%d\,\,{\rm sources}$"%len(odds),bins=bins)
    fig.set_ylabel(r"$P(\ln \mathcal{O}_{\rm GR}^{\rm modGR})$")
    fig.set_xlabel(r"$\ln \mathcal{O}_{\rm GR}^{\rm modGR}$")
    fig.add_grid()
    if len(xlim)==2:
      fig.set_xlimit(xmin=xlim[0],xmax=xlim[1])
    fig.save_fig(settings["figpath"])

  #plot the background
  plot_background(p.events,settings,xlim=[-10.0,30.0],bins=40)
