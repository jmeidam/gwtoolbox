import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "gwtoolbox",
    version = "0.9.0",
    author = "Jeroen Meidam",
    author_email = "jmeidam@nikhef.nl",
    description = ("Collection of tools most of it is only useful to me. More generally useful part are the roq tools"),
    license = "MIT",
    keywords = "roq rom lalsuite gravitational waves",
    url = "https://jmeidam@bitbucket.org/jmeidam/gwtoolbox.git",
    packages=['gwtoolbox','toolboxtests','gwtoolbox.toolboxtypes','gwtoolbox.waveforms','gwtoolbox.roqpipe','gwtoolbox.visualization','gwtoolbox.tiger'],#,'waveforms', 'toolboxtests', 'toolboxtypes'],
    scripts=['scripts/roq_pipe.py','scripts/roq_generate_validation_file.py','scripts/roq_validate.py','scripts/bins_from_data.py','scripts/roq_padbasis.py','scripts/tiger_collect_bayes.py'],
    long_description=read('README'),
    classifiers=[
        "Development Status :: 1 - Planning",
        "Topic :: Utilities",
        "License :: OSI Approved :: MIT License",#what's OSI?
    ],
)
