__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"


"""
Utilities typically only useful for waveform generation and manipulation
"""

import numpy as np

from gwtoolbox.genutils import NextPow2

import lal
import lalsimulation as lalsim

import scipy.interpolate as ip

import random

################################################################################
####                      MASS PARAMETER CONVERSIONS
################################################################################

def convert_q_to_eta(q):
  """Convert mass ratio (which is >= 1) to symmetric mass ratio"""
  x = 1+q
  return q/x/x

def convert_eta_to_q(nu):
  """Convert symmetric mass ratio to mass ratio (which is >= 1)"""
  x = 1.-4.*nu
  return (1.+x**(1./2.)-2.*nu)/(2.*nu)

def convert_Meta_to_m1m2(M,eta):
  """Convert total mass and symm mass ratio to m1,m2"""
  x = 1.0-4.0*eta
  m1 = 0.5*M*(1.0+x**(1./2.))
  m2 = M-m1
  return m1,m2

def convert_Meta_to_Mc(M, eta):
  """Convert total mass and symmetric mass ratio to chirp mass"""
  return M*eta**(3./5.)

def convert_Mceta_to_M(Mc,eta):
  """Convert chirp mass and symmetric mass ratio to total mass"""
  return Mc*eta**(-3./5.)

def convert_Mceta_to_m1m2(Mc,eta):
  """Convert chirp mass and symmetric mass ratio to component masses"""
  M = Mc*eta**(-3./5.)
  m1,m2 = convert_Meta_to_m1m2(M,eta)
  return m1,m2

def convert_Mq_to_Mc(M, q):
  """Convert mass ratio, total mass pair to chirp mass"""
  return M*convert_q_to_eta(q)**(3./5.)

def convert_Mq_to_m1m2(M, q):
  """Convert total mass, mass ratio pair to m1, m2"""
  m2 = M/(1.+q)
  m1 = M-m2
  return [m1, m2]

def convert_Mcq_to_M(Mc, q):
  """Convert mass ratio, chirp mass to total mass"""
  return Mc*convert_q_to_eta(q)**(-3./5.)

def convert_Mcq_to_m1m2(Mc,q):
  """Convert mass ratio, chirp mass to component masses"""
  eta = convert_q_to_eta(q)
  M = Mc*eta**(-3./5.)
  m1,m2 = convert_Meta_to_m1m2(M,eta)
  return m1,m2

def convert_m1m2_to_Mc(m1,m2):
  """Chirp mass from m1, m2"""
  return (m1*m2)**(3./5.)/(m1+m2)**(1./5.)

def convert_m1m2_to_nu(m1,m2):
  """Symmetric mass ratio from m1, m2"""
  return m1*m2/(m1+m2)**2

def convert_m1m2_to_q(m1,m2):
  """mass ratio (which is >= 1) from m1, m2"""
  if m1 < m2:
    q = m2/m1
  else:
    q = m1/m2
  return q

def convert_Meta_to_chirptimes(M,eta,f0):
  """chirptimes tau_0 and tau_3 from mchirp,eta and starting frequency f_0"""
  etainv=eta**(-1.0)
  X0 = etainv*5.0/(256.0*lal.PI*f0)
  Y0 = M*lal.PI*f0
  t0 = X0*Y0**(-5.0/3.0)
  X3 = etainv/(8.0*f0)
  Y3 = M*lal.PI*f0
  t3 = X3*Y3**(-2.0/3.0)
  return t0,t3

def convert_m1m2_to_chirptimes(m1,m2,f0):
  """chirptimes tau_0 and tau_3 from m1,m2 and starting frequency f_0"""
  M = m1+m2
  eta = convert_m1m2_to_nu(m1,m2)
  t0,t3 = convert_Meta_to_chirptimes(M,eta,f0)
  return t0,t3

def fISCO(M):
  """Calculate ISCO frequency for a particular mass M"""
  M=M*lal.MTSUN_SI
  return pow(6.0,-3./2.)/(np.pi*M)

################################################################################
####                 IMRPhenomP Parameters and conversions
################################################################################

def SpinVectorsToPhenomPParams(m1,m2,S1,S2,iota,f_ref):
  """
  Function to calculate IMRPhenomPv2 parameters
  from spin vectors and inclination angle
  """
  # rotate from L frame into N frame
  iota, s1x, s1y, s1z, s2x, s2y, s2z = lalsim.SimInspiralInitialConditionsPrecessingApproxs(iota, S1[0], S1[1], S1[2], S2[0], S2[1], S2[2], m1, m2, f_ref, lalsim.SIM_INSPIRAL_FRAME_AXIS_TOTAL_J)

  lnhatx = np.sin(iota)
  lnhaty = 0.
  lnhatz = np.cos(iota)

  chi1_l,chi2_l,chip,thetaJ,alpha0 = lalsim.SimIMRPhenomPCalculateModelParameters(m1*lal.MSUN_SI,m2*lal.MSUN_SI,
                                                                                f_ref,lnhatx,lnhaty,lnhatz,
                                                                                s1x,s1y,s1z,
                                                                                s2x,s2y,s2z,lalsim.IMRPhenomPv2_V)

  PhenomParams = {"chi1_l":chi1_l,"chi2_l":chi2_l,"chip":chip,"thetaJ":thetaJ,"alpha0":alpha0}

  return PhenomParams


def PhenomPmap(iota, m1, m2, S1, S2, f_ref):
  """
  Interface to map inclination iota to IMRPhenomP parameter thetaJ.
  """
  axis = lalsim.SIM_INSPIRAL_FRAME_AXIS_TOTAL_J
  # rotate from L frame into PhenomP frame
  iota, s1x, s1y, s1z, s2x, s2y, s2z = lalsim.SimInspiralInitialConditionsPrecessingApproxs(iota, S1[0], S1[1], S1[2], S2[0], S2[1], S2[2], m1, m2, f_ref, axis)

  # now calculate PP parameters
  PhenomParams = SpinVectorsToPhenomPParams(m1,m2,S1,S2,iota,f_ref)

  return PhenomParams["thetaJ"]

def PhenomPParamsToSpinVectors(m1,m2,chi1L,chi2L,chip,thetaJ,alpha0,f_ref):
  """
  Function to calculate an inclination and spin vectors S1,S2 that are
  compatible with the provided IMRPhenomPv2 parameters.
  """

  #initial choice
  S1x = np.cos(alpha0) * chip
  S1y = np.sin(alpha0) * chip
  S1z = chi1L
  S2x = 0
  S2y = 0
  S2z = chi2L

  iotas = np.linspace(0,np.pi)
  thetaJNs = map(lambda iota: PhenomPmap(iota, m1*lal.MSUN_SI, m2*lal.MSUN_SI, [S1x, S1y, S1z], [S2x, S2y, S2z], f_ref), iotas)
  iotaI = ip.UnivariateSpline(thetaJNs, iotas)
  iota = iotaI(thetaJ).item()

  #iota = PP_iota_for_thetaJN(thetaJ, m1*lal.MSUN_SI, m2*lal.MSUN_SI, S1x, S1y, S1z, S2x, S2y, S2z, f_ref)
  return iota, [S1x,S1y,S1z], [S2x,S2y,S2z]


################################################################################
####                               Multibanding
################################################################################

def bandtimeFromSimInspiralChirpTimeBound(m1,m2,f0,f1,S1z=0.99,S2z=0.99):
  """
  Calculate the time a chirpsignal spends within a frequency band (f0,f1).
  Only aligned spins can be provided. Default is to create longest possible
  chirp for given masses and frequencies (strong aligned spins).
  """
  Tf0 = lalsim.SimInspiralChirpTimeBound(f0,m1*lal.MSUN_SI,m2*lal.MSUN_SI,S1z,S2z)
  Tf1 = lalsim.SimInspiralChirpTimeBound(f1,m1*lal.MSUN_SI,m2*lal.MSUN_SI,S1z,S2z)
  T = Tf0-Tf1
  return T

def GenerateMultibandFrequencyVector(m1,m2,bands,verbose=0,fudge=1.0):
  """
  Generate a frequency vector that samples according to time spent in bands.
  Output is a vector of frequencies and corresponding deltaF.
  """
  F=[]
  deltaFs=[]
  f=bands[0]
  if verbose:
    print "\nCalculating multiband frequency series (m1=%.2f, m2=%.2f)"%(m1,m2)
    print " f0 =< f < f1  : T -> NextPow2(T)"
  for i in range(1,len(bands)):
    f0 = bands[i-1]
    f1 = bands[i]
    T = fudge*bandtimeFromSimInspiralChirpTimeBound(m1,m2,f0,f1)
    if T < 0.5:
      Tp2 = NextPow2(int(np.floor(1.0/T)))
      df = float(Tp2)
      if verbose:
        print "%3.0f =< f < %3.0f : %7.3f -> 1/%d (df = %.0f)"%(f0,f1,T,Tp2,Tp2)
    else:
      Tp2 = NextPow2(int(np.ceil(T)))
      df = 1.0/float(Tp2)
      if verbose:
        print "%3.0f =< f < %3.0f : %7.3f -> %d (df = 1/%.0f)"%(f0,f1,T,Tp2,Tp2)
    while f < f1:
      F.append(f)
      deltaFs.append(df)
      f+=df

  #add the maximum frequency. This is essential for IMRPhenomPv2, it uses interpolation methods that depend on f_max
  if bands[-1] not in F:
    F.append(bands[-1])
    deltaFs.append(F[-1]-F[-2])

  print "multibanded frequency vector has %d samples\n"%len(F)

  return np.array(F),np.array(deltaFs)


################################################################################
####                                 MISC
################################################################################

def ZeroPNChirpTime(flow,m1,m2):
  """Calculate the 0PN chirp time in seconds
    t = 2.18*(1.21/mchirp)^(5/3)*(100/flow)^(8/3)

  Parameters
  ----------
  flow: float
      starting frequency in Hz
  m1: float
      Mass of component 1 in solar masses
  m2: float
      Mass of component 2 in solar masses
  """
  mchirp=convert_m1m2_to_Mc(m1,m2)
  A=(1.21/mchirp)**(5/3.)
  B=(100.0/flow)**(8/3.)
  return 2.18*A*B

def Ncyc(m1,m2,fmin,fmax):
  """
  The amount of cycles between the frequency band fmin < f < fmax
    (Maggiore Eq. 4.23)
  """
  m1 = m1*lal.MTSUN_SI
  m2 = m2*lal.MTSUN_SI
  A = pow(np.pi,-8./3.)/32.0
  Mc = convert_m1m2_to_Mc(m1,m2)
  C = A*pow(Mc,-5./3.)
  if fmin >= fmax:
    return 0.0
  else:
    return C*pow(fmin,-5./3.)-C*pow(fmax,-5./3.)

def generateSpinVector(s,mag=None,theta=None,phi=None):

  """
  Generate a spin vector. Parameters not provided are
  randomised on the unit sphere.
  s: seed
  mag: magnitude
  theta: angle1
  phi: angle2
  """

  random.seed(s)

  if not mag:
    mag = random.random()
  if not theta:
    theta = -1.0 + random.random()*np.pi
  if not phi:
    phi = 2.0*random.random()*np.pi

  x=mag*np.cos(theta)*np.sin(phi)
  y=mag*np.sin(theta)*np.sin(phi)
  z=mag*np.cos(phi)

  return np.array([x,y,z])