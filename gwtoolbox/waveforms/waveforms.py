__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

import numpy as np
from subprocess import Popen, PIPE

from gwtoolbox.waveforms import utils as wfutils
from gwtoolbox.toolboxtypes import toolboxtypes as tbt

import lal
import lalsimulation as lalsim


def lalsim_inspiral(mass1,mass2,spin1,spin2,iota,f_min,f_ref,nonGRdict,approximant="IMRPhenomPv2",phiref=0.0,outputdomain="time",amp_phase=False,f_max=0.0,lalsim_inspiral_bin="lalsim-inspiral",verbose=False,srate=4096.0,distance=1.0):
  """
  masses in solar mass
  distance in Mpc
  """

  command=lalsim_inspiral_bin+" -a %s --m1 %.2f --m2 %.2f --f-min %.2f --fRef %.2f"%(approximant,mass1,mass2,f_min,f_ref)
  command+=" --spin1x %.2f --spin1y %.2f --spin1z %.2f"%(spin1[0],spin1[1],spin1[2])
  command+=" --spin2x %.2f --spin2y %.2f --spin2z %.2f"%(spin2[0],spin2[1],spin2[2])
  command+=" --phiRef %.2f"%phiref
  command+=" --sample-rate %.2f"%srate
  command+=" --distance %.2f"%distance

  if f_max > 0.:
    command+=" --f-max %.2f"%f_max

  if len(nonGRdict.keys())>0:
    command += " --nonGRpar "
    for param,i in zip(nonGRdict.keys(),range(len(nonGRdict))):
      val = nonGRdict[param]
      if i>0: command+=","
      command += "%s=%.2f"%(param,val)

  if outputdomain == "frequency":
    command+=" -F"
  else:
    command+=" -c" #use conditioning only in time domain
  if amp_phase:
    command+=" -Q"

  if verbose:
    print command
#  print command
  p = Popen(command, stdout=PIPE, stderr=PIPE,shell=True)
  lines = p.stdout.readlines()
#  lines = lines[1:]
#  if "{" in lines[0]: lines=lines
  imin=0
  while True:
    try:
      float(lines[imin].split()[0])
      break
    except:
      imin+=1
  lines = lines[imin:]
  if verbose: print "first line in data array:\n%s"%lines[0]
  lines = np.array([k.strip('\n') for k in lines])
  x = []
  hp = []
  hc = []
  if outputdomain == "time":
    for l in lines:
      x.append(float(l.split()[0]))
      hp.append(float(l.split()[1]))
      hc.append(float(l.split()[2]))
  elif outputdomain == "frequency":
    for l in lines:
      if amp_phase:
        x.append(float(l.split()[0]))
        hp.append(float(l.split()[1])) #amp
        hc.append(float(l.split()[2])) #phase
      else:
        x.append(float(l.split()[0]))
        hp.append(float(l.split()[1])*1.0j+float(l.split()[2]))
        hc.append(float(l.split()[3])*1.0j+float(l.split()[4]))
  else:
    print "Input Error: Only use \"time\" or \"frequency\""
    return None,None,None

  return np.array(x),np.array(hp),np.array(hc)

def IMRPhenomPv2(mass1,mass2,spin1,spin2,iota,fmin,fmax,fref,nonGRdict,deltaF,distance=1.0,phic=0.0):
  """
  Call IMRPhenomPv2 using spin vectors
  """
  extraParams = tbt.nonGRparameters(nonGRdict)


  P = wfutils.SpinVectorsToPhenomPParams(mass1,mass2,spin1,spin2,iota,fref)

  hptilde,hctilde = lalsim.SimIMRPhenomP(P["chi1_l"],P["chi2_l"],P["chip"],
                                                       P["thetaJ"],
                                                       mass1*lal.MSUN_SI,mass2*lal.MSUN_SI,
                                                       distance*lal.PC_SI*1000000.0,P["alpha0"],phic,
                                                       deltaF,fmin,fmax,fref,
                                                       lalsim.IMRPhenomPv2_V,extraParams.get_lal())

  return hptilde.data.data,hctilde.data.data

#def LALSimIMRPhenomPv2(mass1,mass2,chi1L,chi2L,chip,thetaJ,alpha0,fmin,fmax,deltaF,fref=40.0,phic=0.0,distance=1.0,nonGRdict={}):
#  """
#  Call IMRPhenomPv2 using chi1L,chi2L,chip,thetaJ and alpha0
#  distance in Mpc
#  """
#  extraParams = createExtraParamsStruct(nonGRdict,verbose=0)
#
#  hptilde,hctilde = lalsim.SimIMRPhenomP(chi1L,chi2L,chip,
#                                                       thetaJ,
#                                                       mass1*lal.MSUN_SI,mass2*lal.MSUN_SI,
#                                                       distance*lal.PC_SI*1000000.0,alpha0,phic,
#                                                       deltaF,fmin,fmax,fref,
#                                                       lalsim.IMRPhenomPv2_V,extraParams)
#
#  return hptilde.data.data,hctilde.data.data
#
#def IMRPhenomPv2FrequencySequence(mass1,mass2,spin1,spin2,iota,freqs,f_ref,nonGRdict,distance=1.0,phic=0.0):
#  """
#  Call IMRPhenomPv2 using lalsimulation parameters
#  distance in Mpc
#  """
#  extraParams = createExtraParamsStruct(nonGRdict,verbose=0)
#
#  P = CalculatePhenomPParameters(mass1,mass2,spin1,spin2,iota,f_ref)
#
#  hptilde,hctilde = lalsim.SimIMRPhenomPFrequencySequence(freqs,P["chi1_l"],P["chi2_l"],P["chip"],
#                                                       P["thetaJ"],
#                                                       mass1*lal.MSUN_SI,mass2*lal.MSUN_SI,
#                                                       distance*lal.PC_SI*1000000.0,P["alpha0"],phic,f_ref,
#                                                       lalsim.IMRPhenomPv2_V,extraParams)
#
#  return hptilde.data.data,hctilde.data.data
#
#def LALSimIMRPhenomPv2FrequencySequence(mass1,mass2,chi1L,chi2L,chip,thetaJ,alpha0,freqs,f_ref=20.0,nonGRdict={},distance=1.0,phic=0.0):
#  """
#  Call IMRPhenomPv2 using chi1L,chi2L,chip,thetaJ and alpha0
#  distance in Mpc
#  """
#  extraParams = createExtraParamsStruct(nonGRdict,verbose=0)
#
#  hptilde,hctilde = lalsim.SimIMRPhenomPFrequencySequence(freqs,chi1L,chi2L,chip,
#                                                       thetaJ,
#                                                       mass1*lal.MSUN_SI,mass2*lal.MSUN_SI,
#                                                       distance*lal.PC_SI*1.0e6,alpha0,phic,f_ref,
#                                                       lalsim.IMRPhenomPv2_V,extraParams)
#
#  return hptilde.data.data,hctilde.data.data
#
#
#
#
#
#def SpinTaylorT4(m1,m2,S1,S2,iota,f_min,f_max,EOS=None,samplerate = 4096.,distance=1.0):
#
#
#  LNhatx = sin(iota);
#  LNhaty = 0.;
#  LNhatz = cos(iota);
#  E1x = cos(iota);
#  E1y = 0.;
#  E1z = - sin(iota);
#
#  deltaT = 1./samplerate
#
#  Msec = m1*lal.MTSUN_SI+m2*lal.MTSUN_SI
#  m1sec = m1*lal.MTSUN_SI
#  m2sec = m2*lal.MTSUN_SI
#  m1sec5 = m1sec*m1sec*m1sec*m1sec*m1sec
#  m2sec5 = m2sec*m2sec*m2sec*m2sec*m2sec
#
#  lambda1 = XLALSimInspiralEOSLambda(EOS,m1)/m1sec5
#  lambda2 = XLALSimInspiralEOSLambda(EOS,m2)/m2sec5
#  if EOS!=None:
#    quadparam1 = XLALSimInspiralEOSQfromLambda(lambda1)
#    quadparam2 = XLALSimInspiralEOSQfromLambda(lambda2)
#  else:
#    quadparam1 = 1.0
#    quadparam2 = 1.0
#
#  #print m1, m2, lambda1, lambda2, quadparam1,quadparam2
#
#  V, Phi, S1x, S1y, S1z, S2x, S2y, S2z, Lx, Ly, Lz, Ex, Ey, Ez = \
#  lalsim.SimInspiralSpinTaylorPNEvolveOrbit(deltaT,#deltaT
#                                            m1 * lal.MSUN_SI, #m1 in kg
#                                            m2 * lal.MSUN_SI, #m2 in kg
#                                            f_min, #fStart
#                                            f_max, #fEnd
#                                            S1[0], S1[1], S1[2], #spin1 x,y,z
#                                            S2[0], S2[1], S2[2], #spin2 x,y,z
#                                            LNhatx, LNhaty, LNhatz, #lnhat x,y,z
#                                            E1x, E1y, E1z, #E1 x,y,z
#                                            lambda1, #lambda1
#                                            lambda2, #lambda2
#                                            quadparam1, #qmparam1
#                                            quadparam2, #qmparam2
#                                            lalsim.SIM_INSPIRAL_SPIN_ORDER_ALL,#twice PN order of spin effects
#                                            lalsim.SIM_INSPIRAL_TIDAL_ORDER_ALL,#twice PN order of tidal effects
#                                            7,#twice post-Newtonian order
#                                            lalsim.SpinTaylorT4)#PN approximant (SpinTaylorT1/T2/T4)
#
#  t = float(V.epoch) + arange( V.data.length ) * V.deltaT
#  f = [v*v*v/lal.PI/Msec for v in V.data.data]
#
#  hp,hc = lalsim.SimInspiralPrecessingPolarizationWaveforms(V, Phi,
#                                                            S1x, S1y, S1z,
#                                                            S2x, S2y, S2z,
#                                                            Lx, Ly, Lz,
#                                                            Ex, Ey, Ez,
#                                                            m1 * lal.MSUN_SI, #m1 in kg
#                                                            m2 * lal.MSUN_SI, #m2 in kg
#                                                            distance,#distance in m
#                                                            1,#v0
#                                                            0)#amplitudeO
#
#  return V.data.data, Phi.data.data, f, t, hp.data.data, hc.data.data
#
#
#
#def TaylorF2(fseries,mass1,mass2,S1z,S2z,nonGRdict=None,EOS=None,f_ref=30.0,phi_ref=0.0,distance=1.0):
#
#  m1_SI = mass1*lal.MSUN_SI
#  m2_SI = mass2*lal.MSUN_SI
#
#  m1sec = mass1*lal.MTSUN_SI
#  m2sec = mass2*lal.MTSUN_SI
#  m1sec5 = m1sec*m1sec*m1sec*m1sec*m1sec
#  m2sec5 = m2sec*m2sec*m2sec*m2sec*m2sec
#
#  lam1 = XLALSimInspiralEOSLambda(EOS,mass1)/m1sec5
#  lam2 = XLALSimInspiralEOSLambda(EOS,mass2)/m2sec5
#  if EOS!=None:
#    qm1 = XLALSimInspiralEOSQfromLambda(lam1)
#    qm2 = XLALSimInspiralEOSQfromLambda(lam2)
#  else:
#    qm1 = 1.0
#    qm2 = 1.0
#
#  extraParams = createExtraParamsStruct(nonGRdict,verbose=0)
#
#  spinO = lalsim.SIM_INSPIRAL_SPIN_ORDER_ALL
#  tideO = lalsim.SIM_INSPIRAL_TIDAL_ORDER_ALL
#  phaseO = 7
#  amplitudeO = 0
#
#  n=len(fseries)
#  freqs = lal.CreateREAL8Sequence(n);
#
#  for i in range(n):
#    freqs.data[i] = fseries[i]
#
#  h = lalsim.SimInspiralTaylorF2Core(freqs, phi_ref, m1_SI, m2_SI,
#                                      S1z, S2z, f_ref, 0.0, distance, qm1, qm2,
#                                      lam1, lam2, spinO, tideO, phaseO, amplitudeO, extraParams)
#
#  return h.data.data
#
#
################################################################################
################################################################################
###############                --- EOB ---                  ###############
################################################################################
################################################################################
#
#def getRDfrequency(m1,m2,spin1,spin2,l,m,n):
#
#  appr = lalsim.SimInspiralGetApproximantFromString("SEOBNRv2")
#  modefreqVec = lal.CreateCOMPLEX16Vector(1)
#  lalsim.SimIMREOBGenerateQNMFreqV2(modefreqVec,m1,m2,spin1,spin2,l,m,n,appr)
#
#def ChooseTD(m1,m2,spin1,spin2,iota,f_min,f_ref=20.0,nonGRdict={},approximant="SEOBNRv2",EOS="",samplerate = 4096.,distance=1.0,phiref=0.0):
#
#  deltaT = 1./samplerate
#
#  m1_SI = m1*lal.MSUN_SI
#  m2_SI = m2*lal.MSUN_SI
#  m1sec = m1*lal.MTSUN_SI
#  m2sec = m2*lal.MTSUN_SI
#  m1sec5 = m1sec*m1sec*m1sec*m1sec*m1sec
#  m2sec5 = m2sec*m2sec*m2sec*m2sec*m2sec
#
#  appr = lalsim.SimInspiralGetApproximantFromString(approximant)
#
#
#  if EOS!="":
#    lambda1 = XLALSimInspiralEOSLambda(EOS,m1)/m1sec5
#    lambda2 = XLALSimInspiralEOSLambda(EOS,m2)/m2sec5
#  else:
#    lambda1 = 0.0
#    lambda2 = 0.0
#
#  nonGRparams = createExtraParamsStruct(nonGRdict,verbose=0)
#
#  frameAxis = lalsim.SIM_INSPIRAL_FRAME_AXIS_ORBITAL_L
#  waveFlags=lalsim.SimInspiralCreateWaveformFlags()
#  lalsim.SimInspiralSetSpinOrder(waveFlags, -1)
#  lalsim.SimInspiralSetTidalOrder(waveFlags, -1)
#  lalsim.SimInspiralSetFrameAxis(waveFlags,frameAxis)
#  hp,hc = \
#  lalsim.SimInspiralChooseTDWaveform(phiref,deltaT,m1_SI,m2_SI,spin1[0],spin1[1],spin1[2],spin2[0],spin2[1],spin2[2],
#                                   f_min,f_ref,1.0,iota,lambda1,lambda2,waveFlags,nonGRparams,0,7,appr)
#
#  return hp.data.data, hc.data.data
#
################################################################################
################################################################################
###############                --- RINGDOWN ---                  ###############
################################################################################
################################################################################
#
#class waveform_mode(object):
#  def __init__(self,l,m):
#    self.l = l
#    self.m = m
#    self.mode = None
#
#class TimeSeries(object):
#  def __init__(self,name,t0,deltaT,length,f0=0.0):
#    self.name = name
#    self.t0 = t0
#    self.deltaT = deltaT
#    self.f0 = f0
#    self.length = length
#    self.data = zeros(length,dtype=complex)
#
#
#def ChiEffRingdown(m1, m2, spin1, spin2):
#  eta = (m1*m2)/(m1 + m2)/(m1 + m2)
#  chi1 = sqrt(spin1[0]*spin1[0] + spin1[1]*spin1[1] + spin1[2]*spin1[2])
#  chi2 = sqrt(spin2[0]*spin2[0] + spin2[1]*spin2[1] + spin2[2]*spin2[2])
#  chim = (m1*chi1 - m2*chi2)/(m1 + m2)
#  chiEff = 1./2.*(sqrt(1. - 4.*eta)*chi1 + chim)
#  return chiEff
#
#def SpinBinaryFinalBHSpin( mass1, mass2, spin1, spin2 ):
#  """
#  Formulas for final spin of an initially spinning or initially
#  non-spinning binary.
#  Barausse & Rezzolla arxiv:0904.2577
#  Equations 6, 7, and 10
#  """
#
#  eta = (mass1*mass2)/(mass1 + mass2)/(mass1 + mass2)
#
#  spin1x = spin1[0]
#  spin1y = spin1[1]
#  spin1z = spin1[2]
#  spin2x = spin2[0]
#  spin2y = spin2[1]
#  spin2z = spin2[2]
#
#  t0 = -2.8904
#  t2 = -3.5171
#  t3 = 2.5763
#  s4 = -0.1229
#  s5 = 0.4537
#  q = mass2 / mass1
#  q2 = q * q
#  q4 = q2 * q2
#  q2sum = 1.0 + q2
#  norma1 = sqrt( spin1x * spin1x + spin1y * spin1y + spin1z * spin1z )
#  norma2 = sqrt( spin2x * spin2x + spin2y * spin2y + spin2z * spin2z )
#  norma12 = norma1 * norma1
#  norma22 = norma2 * norma2
#
#  # lalsuite fixme quote:
#  # The following calculation for Eq. 7 of arxiv:0904.2577
#  # assumes that we have spin (anti-)aligned PhenomB waveforms
#  # aligned in the \hat{z} direction. Furthermore, we assume that
#  # the orbital angular momentum L is along the positive \hat{z}
#  # direction. For the completely general case, we will need to store
#  # the components of \hat{L}.
#  a1sign = sign( spin1z )
#  a2sign = sign( spin2z )
#  cosalpha = float(a1sign * a2sign)
#  cosbeta = float(a1sign)
#  cosgamma = float(a2sign)
#
#  norml = 2.0 * sqrt(3.0) + t2 * eta + t3 * eta * eta + s4  / (q2sum * q2sum) *\
#     (norma12 + norma22 * q4 + 2.0 * norma1 * norma2 * q2 * cosalpha) +\
#     (s5 * eta + t0 + 2.0) / q2sum * (norma1 * cosbeta + norma2 * q2 * cosgamma)
#
#  return 1.0 / ((1.0 + q) * (1.0 + q)) * sqrt(norma12 + norma22 * q4 + 2.0 * norma1 *\
#     norma2 * q2 * cosalpha + 2.0 * (norma1 * cosbeta + norma2 * q2 * cosgamma) * norml *\
#     q + norml * norml * q2)
#
#def NonSpinBinaryFinalBHMass( mass1, mass2 ):
#  eta = (mass1*mass2)/(mass1 + mass2)/(mass1 + mass2)
#  return ( 1 + ( sqrt(8.0/9.0) - 1) * eta - 0.498 * eta * eta) * (mass1 + mass2)
#
#
#def BlackHoleRingdownTiger(
#  hlms,         #list of mode objects
#  t0,           #start time of ringdown
#  phi0,         #initial phase of ringdown (rad)
#  deltaT,       #sampling interval (s)
#  mass,         #black hole mass (kg)
#  a,            #black hole dimensionless spin parameter
#  eta,          #symmetric mass ratio of progenitor
#  spin1,        #initial spin for 1st component
#  spin2,        #initial spin for 2nd component
#  chiEff,       #effective spin parameter for initial spins
#  distance,     #distance to source (m)
#  inclination,  #inclination of source's spin axis (rad)
#  nonGRparams): #testing GR parameters
#
#  maxmodelength=0
#  thismodelength=0
#
#  dtau = 0.0
#  dfreq = 0.0
#
#  ## Go through the list of quasi-normal modes and fill in the structure
#  for thisMode in hlms:
#
#    nonGRParamName = "dtau%d%d"%(thisMode.l,thisMode.m)
#    if nonGRparams.has_key(nonGRParamName):
#      dtau = nonGRparams[nonGRParamName]
#      print "Injecting dtau%d%d = %.2f"%(thisMode.l,thisMode.m,dtau)
#
#    nonGRParamName = "dfreq%d%d"%(thisMode.l,thisMode.m)
#    if nonGRparams.has_key(nonGRParamName):
#      dfreq = nonGRparams[nonGRParamName]
#      print "Injecting dfreq%d%d = %.2f"%(thisMode.l,thisMode.m,dfreq)
#
#    thisMode.mode = BlackHoleRingdownModeTiger(t0, phi0, deltaT, mass, a, distance, inclination, eta, spin1, spin2, chiEff, thisMode.l, thisMode.m, dfreq, dtau)
#
#    if thisMode.mode == None:
#      print "BlackHoleRingdownTiger failed"
#      return None
#    thismodelength = thisMode.mode.length
#
#    if (thismodelength > maxmodelength):
#     maxmodelength = thismodelength
#    else:
#     maxmodelength = maxmodelength
#
#
#  ## Create the plus and cross polarization vectors
#
#  hplus = TimeSeries("hplus", t0, deltaT, maxmodelength)
#  hcross = TimeSeries("hcross", t0, deltaT, maxmodelength)
#
#  ## Fill in the plus and cross polarization vectors
#  hpc = 0.0
#  for thisMode in hlms:
#    for j in range(thisMode.mode.length):
#      hpc = thisMode.mode.data[j]
#      hplus.data[j] += real(hpc)
#      hcross.data[j] -= imag(hpc)
#
#  return hplus,hcross
#
#def BlackHoleRingdownModeTiger(
#  t0,          # start time of ringdown
#  phi0,        # initial phase of ringdown (rad)
#  deltaT,      # sampling interval (s)
#  mass,        # black hole mass (kg)
#  a,           # black hole dimensionless spin parameter
#  distance,    # distance to source (m)
#  inclination, # inclination of source's spin axis (rad)
#  eta,         # symmetric mass ratio of progenitor
#  spin1,       # initial spin for 1st component
#  spin2,       # initial spin for 2nd component
#  chiEff,      # effective spin parameter for initial spins
#  l,           # polar mode number
#  m,           # azimuthal mode number
#  dfreq,       # relative shift in the real frequency parameter
#  dtau):       # relative shift in the damping time parameter
#
#  """
#  Computes the waveform for the ringdown of a black hole
#  quasinormal mode (l,m).
#  The amplitudes and QNM frequen are calculated
#  according to Gossan et. al 2012 [arXiv: 1111.5819]
#
#  Note: The dimensionless spin assumes values between -1 and 1.
#  """
#
#  mass_sec = mass*lal.MTSUN_SI/lal.MSUN_SI
#  dist_sec = distance/lal.C_SI
#
#  name = "h%u%d"%(l, m)
#
#  alphalm = RingdownQNMAmplitudes(l, m, eta, chiEff)
#  A = alphalm*mass_sec/dist_sec
#
#  Yplus = SphericalHarmonicPlus(l, m, inclination)
#  Ycross = SphericalHarmonicCross(l, m, inclination)
#
#  omega = RingdownFitOmega(l, m, 0, a)
#  if omega == None:
#    print "RingdownFitOmega failed"
#    return None
#  tau = QNMTauOfOmega(omega, mass_sec)*(1 + dtau)
#  freq = QNMFreqOfOmega(omega, mass_sec)*(1 + dfreq)
#
#  length = int(ceil( -tau * log(1.0e-16) / deltaT ))
#
#  hlmmode = TimeSeries(name,t0,deltaT,length)
#
#  t = 0.0
#  if (A > 0.0):
#    for j in range(length):
#      t = j*deltaT
#      hlmmode.data[j] = A*Yplus*exp(-t/tau)*cos(t*freq - m*phi0)
#      hlmmode.data[j] -= 1.0j*A*Ycross*exp(-t/tau)*sin(t*freq - m*phi0)
#
#  return hlmmode
#
##NOTE: iota and "0.0" may be the other way around
#def SphericalHarmonicPlus(l, m, iota):
#  Yplus = lal.SpinWeightedSphericalHarmonic(iota, 0.0, -2, l, m) + (1.0 - 2.0*(l % 2))*lal.SpinWeightedSphericalHarmonic(iota, 0.0, -2, l, -m)
#  return Yplus
#
#def SphericalHarmonicCross(l, m, iota):
#  Ycross = lal.SpinWeightedSphericalHarmonic(iota, 0.0, -2, l, m) - (1.0 - 2.0*(l % 2))*lal.SpinWeightedSphericalHarmonic(iota, 0.0, -2, l, -m)
#  return Ycross
#
#def RingdownQNMAmplitudes(l, m, eta, chiEff):
#  """
#  Calculates the amplitudes for the QNM l, m, n=0 for
#  a given symmetric mass-ratio eta of the initial binary.
#  Based on an interpolation for NON-SPINNING binary black
#  holes, derived in Gossan et al (2012) [arXiv: 1111.5819]
#
#  lalsuite TODO quote: "rewrite this properly, add initial (effective) spin dependence"
#  """
#  A22eta = 0.8639*eta
#  if (l==2 and m==2):
#    A = A22eta
#  elif (l==2 and abs(m)==1):
#    A = A22eta*0.43*(sqrt(1.0 - 4.*eta) - chiEff)
#  elif (l==3 and abs(m)==3):
#    A = A22eta*0.44*pow(1.0 - 4.*eta, 0.45)
#  elif (l==3 and abs(m)==2):
#    A = A22eta*3.69*(eta - 0.2)*(eta - 0.2) + 0.053
#  elif (l==4 and abs(m)==4):
#    A = A22eta*(5.41*(eta - 0.22)*(eta - 0.22) + 0.04)
#  else:
#    A = 0.0
#  return A
#
#
#def RingdownFitOmega(l, m, n, a):
#  """
#  Computes the complex dimensionless eigen-frequency for the
#  quasinormal mode (l,m), given a dimensionless spin a \in [-1,1].
#  This is based on the interpolation formula from Berti et al 2006
#  [arXiv: gr-qc/0512160].
#  The dimensionful frequency is Re(omega)/M.
#  The dimensionful damping time is M/Im(omega).
#
#  Note: The dimensionless spin assumes values between -1 and 1.
#  """
#
#  if (abs(a) > 1.):
#    print "ERROR: Dimensionless spin magnitude larger than 1! Aborting..."
#    return None
#
#  if l==2:
#    if abs(m)==2:
#      omega = (1.5251 - 1.1568*pow(1.e0-a,0.1292))
#      omega += 1.0j*omega/(2.*(0.7000 + 1.4187*pow(1.e0-a,-0.4990)))
#    elif abs(m)==1:
#      omega = (0.6000 - 0.2339*pow(1.e0-a,0.4175))
#      omega += 1.0j*omega/(2.*(-0.3000 + 2.3561*pow(1.e0-a,-0.2277)))
#    else:
#      omega=None
#  elif l==3:
#    if abs(m)==3:
#      omega = (1.8956 - 1.3043*pow(1.e0-a,0.1818))
#      omega += 1.0j*omega/(2.*(0.9000 + 2.3430*pow(1.e0-a,-0.4810)))
#    elif abs(m)==2:
#      omega = (1.1481 - 0.5552*pow(1.e0-a,0.3002))
#      omega += 1.0j*omega/(2.*(0.8313 + 2.3773*pow(1.e0-a,-0.3655)))
#    else:
#      omega=None
#  elif l==4:
#    if abs(m)==4:
#      omega = (2.3000 - 1.5056*pow(1.e0-a,0.2244))
#      omega += 1.0j*omega/(2.*(1.1929 + 3.1191*pow(1.e0-a,-0.4825)))
#    else:
#      omega=None
#  else:
#    omega=None
#
#  if (omega == None):
#    print "ERROR: Invalid mode l = %d, m = %d, n = %u\n"%(l, m, n)
#    return None
#
#  return omega
#
#
#def QNMFreqOfOmega(omega, mtot):
#  """
#  Frequency in rad/sec given dimensionless complex frequency and M
#  """
#  return real(omega)/mtot
#
#
#def QNMTauOfOmega(omega, mtot):
#  """
#  Damping time in sec given dimensionless complex frequency and M
#  """
#  return mtot/imag(omega)
#
#
### THE "ENGINE"
#def BlackHoleRingdownTigerWaveform(modes,spin1,spin2,t0=0.0,phi0=0.0,
#                                   nonGRparams={},SampleRate=4096.,distance=1.0,
#                                   inclination=0.0,
#                                   finalBHmass=None,eta=0.25,
#                                   m1=None,m2=None):
#  """
#  Generate a ringdown waveform with optional nonGR parameters
#  Only allowed modes are 22, 21, 33, 32 and 44
#  Either give m1 and m2 or finalBHmass and eta (0.25 default)
#
#  Final BH spin is calculated from initial masses and spins
#
#  Call with e.g.:
#  hp,hc,tseries=BlackHoleRingdownTigerWaveform([[2,2],[3,3]],zeros(3),zeros(3),finalBHmass=70.0,eta=0.25)
#  """
#
#  if finalBHmass != None:
#    mass = finalBHmass*lal.MSUN_SI
#    m1 = 0.5*mass*(1.0+sqrt(1.0-4.0*eta))
#    m2 = mass-m1
#    m1*=lal.MSUN_SI
#    m2*=lal.MSUN_SI
#  else:
#    if m2 == None or m1 == None:
#      print "Error: must specify m1 and m2 if finalBHmass is not specified"
#      return None
#    mass = NonSpinBinaryFinalBHMass(m1,m2)*lal.MSUN_SI
#    eta = (m1*m2)/(m1 + m2)/(m1 + m2)
#
#  a = SpinBinaryFinalBHSpin(m1, m2, spin1, spin2)
#  chiEff = ChiEffRingdown(m1, m2, spin1, spin2)
#
#  hlms = []
#  for m in modes:
#    hlms.append(waveform_mode(m[0],m[1]))
#
#  deltaT = 1.0/SampleRate
#
#  hp,hc = BlackHoleRingdownTiger(hlms, t0, phi0, deltaT, mass,a,eta,spin1,spin2,chiEff,distance,inclination,nonGRparams)
#
#  tseries = [t0 + i*deltaT for i in range(hc.length)]
#
#  return hp.data,hc.data,tseries
#
#__all__ = ["nonGRparameters",
#           "createExtraParamsStruct",
#           "CalculatePhenomPParameters",
#           "XLALSimInspiralEOSLambda",
#           "XLALSimInspiralEOSQfromLambda",
#           "XLALSimInspiralNSRadiusOfLambdaM",
#           "XLALSimInspiralContactFrequency",
#           "lalsim_inspiral",
#           "IMRPhenomPv2",
#           "LALSimIMRPhenomPv2",
#           "IMRPhenomPv2FrequencySequence",
#           "SpinTaylorT4",
#           "TaylorF2",
#           "getRDfrequency",
#           "ChooseTD",
#           "waveform_mode",
#           "TimeSeries",
#           "ChiEffRingdown",
#           "SpinBinaryFinalBHSpin",
#           "NonSpinBinaryFinalBHMass",
#           "BlackHoleRingdownTiger",
#           "BlackHoleRingdownModeTiger",
#           "SphericalHarmonicPlus",
#           "SphericalHarmonicCross",
#           "RingdownQNMAmplitudes",
#           "RingdownFitOmega",
#           "QNMFreqOfOmega",
#           "QNMTauOfOmega",
#           "BlackHoleRingdownTigerWaveform"]
#
#if _ReturnPhaseAvailable:
#  __all__.append("IMRPhenomPv2Test")