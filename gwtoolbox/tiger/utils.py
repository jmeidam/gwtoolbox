#! python

__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

import numpy as np
from os import path,listdir
from glob import glob

from gwtoolbox import genutils as gu

#The two variants of bayes file formats
#The first used to appear in the folder posterior_samples
#The second always appeared in the engine folder
_bayesfilename1="posterior_V1H1L1_GPS-EVENT.dat_B.txt"

#IFOS are hardcoded, which is only a problem for this annoying file format,
#because * has some number that is not trivial and different across hypotheses
_bayesfilename2="lalinferencenest-EVENT-V1H1L1-GPS.0-*.dat_B.txt"


def ParseTimeslideFile(filename):
  """
  The timeslide file must have a header line (no #)
  listing the ifo for each column
  """
  timeslides={}
  if (path.isfile(filename) and path.getsize(filename) > 0):
    f=open(filename,'r')
    lines=f.readlines()
    ifos=lines[0].strip().split()
    print "  Timeslides found for %s"%(" ".join(ifos))
    for ifo in ifos:
      timeslides[ifo]=[]
    for line in lines[1:]:
      times=line.strip().split()
      for i in range(len(ifos)):
        timeslides[ifos[i]].append(int(times[i]))
    f.close()
  else:
    print "  Timeslides file %s not found"%filename

  return timeslides


def CalculateLogOddsmodGRvsGR(hypotheses):
  """
  Compute combined log odds ratio i.e. Log(O^{modGR}_{GR}) for
  one source.
  hypotheses is a dict containing hypothesis objects.
  This function only makes sense if there is at least one GR hypothesis and
  a subhypothesis.

  The logOdds is calculated as:
    log(prefactor) + log(B^hyp1_noise + B^hyp2_noise + ...) - log(B^GR_noise)
  """
  nhyp = len(hypotheses)
  alpha = 1.
  prefactor = alpha/(nhyp-1.)

  keys = hypotheses.keys()
  keys.remove("GR")

  #Calculate the log sum of bayes factors (log(B^hyp1_noise + B^hyp2_noise + ...))
  comlbayes = hypotheses[keys[0]].Bayes
  for key in keys[1:]:
    hypothesis_obj = hypotheses[key]
    comlbayes = gu.lnPLUS(comlbayes, hypothesis_obj.Bayes)

  comlbayes = np.log(prefactor) + comlbayes - hypotheses["GR"].Bayes

  return comlbayes


def GenerateSubhypotheses(list,noGR=False):
  """
  Creates unique combinations from hypotheses in list.
  For example:
    createCombinationsFromList(["dchi1","dchi2"])
      --> dchi1, dchi2, dchi1dchi2
  """
  outputlist = []
  if not noGR:
    outputlist.append("GR")
  for L in xrange(len(list)):
    for subset in gu.Combinations(list, L+1):
      temp = ""
      for subsetitem in subset:
              temp += subsetitem

      outputlist.append(temp)

  return outputlist


def ReadBayesFile(bfile):
  """
  Parse a lalinference Bayes file
  The Bayes value is always log B^hyp_noise
  """
  if (path.isfile(bfile) and path.getsize(bfile) > 0):
    with open(bfile,'r') as f:
      line = f.readline()
      Bayes = line.split()[0]
      B1 = line.split()[1]
      B2 = line.split()[2]
      B3 = line.split()[3]
  else:
    print bfile+" does not exist or is empty. logB is set to 0.0"
    Bayes = B1 = B2 = B3 = 0.0

  return float(Bayes), float(B1), float(B2), float(B3)



class hypothesis():
  """
  Class to store hypothesis specific values

  It is possible to create an empty hypothesis in which case only
  hypname,num and time are required. It is possible to then
  provide Bayes manually.
  """
  def __init__(self,hypname,num,time,bfile="",empty=False):
    self.num=num
    self.gps=time
    self.name=hypname
    self.Bayes=0.0
    self.bfile=""

    if not empty:
      if bfile=="":
        print "Error in hypothesis: Must provide bfile when empty=False"
        return None
      self.bfile=bfile
      self.Bayes, B1, B2, B3 = self.get_bayes()

  def get_bayes(self):
    if (path.isfile(self.bfile) and path.getsize(self.bfile) > 0):
      with open(self.bfile,'r') as f:
        line = f.readline()
        Bayes = line.split()[0]
        B1 = line.split()[1]
        B2 = line.split()[2]
        B3 = line.split()[3]
    else:
      print self.bfile+" does not exist or is empty. logB is set to 0.0"
      Bayes = B1 = B2 = B3 = 0.0

    return float(Bayes), float(B1), float(B2), float(B3)



class event():
  """
  Class to store event specific values, including a set of corresponding
  subhypotheses that are "hypothesis" objects

  It is possible to create an empty event object, when the Bayes factors come
  from somewhere else. In this case, only num and time are required
  and no hypothesis objects are generated.
  """
  def __init__(self,num,time,subhypotheses=[],rundir="",bdir="",filename="",timeslidedata={},empty=False):

    if not empty:
      fail=False
      if rundir=="":
        print "Error in event: Must provide rundir when empty=False"
        fail=True
      if len(subhypotheses)==0:
        print "Error in event: Must provide subhypothesis names when empty=False"
        fail=True
      if filename=="":
        print "Error in event: Must provide bayes filename when empty=False"
        fail=True
      if fail: return

    self.num=num
    self.gps=time
    self.subhypnames=[]
    self.hypotheses={}
    self.Nsubhyps=0
    self.done=False
    self.LogOdds=0.0
    self.timeslides={}

    if not empty:
      self.subhypnames=subhypotheses

      if len(timeslidedata.keys())>0:
        for ifo in timeslidedata.keys():
          self.timeslides[ifo]=timeslidedata[ifo][self.num]

      for subhyp in self.subhypnames:
        if "lalinference" in filename:
          annoyingname="lalinferencenest-%d-V1H1L1-%d.0-*.dat_B.txt"%(self.num,self.gps)
          globbed=glob(path.join(rundir,subhyp,bdir,annoyingname))
          if len(globbed)>0:
            bfilename=globbed[0]
          else:
            bfilename="none"
        else:
          bfilename=path.join(rundir,subhyp,bdir,filename)
        if (path.isfile(bfilename) and path.getsize(bfilename) > 0):
          self.hypotheses[subhyp]=hypothesis(subhyp,self.num,self.gps,bfile=bfilename)
          if self.hypotheses[subhyp] != None:
            self.Nsubhyps+=1

      if self.Nsubhyps == len(self.subhypnames):
        self.done=True
        self.LogOdds = CalculateLogOddsmodGRvsGR(self.hypotheses)



class tiger_project(object):
  """
  A project collects all events, and for each event stores
  subhypothesis objects.
  For each event a log odds is calculated if all subhypotheses
  are finished running, i.e. have a Bayes file.

  When empty=True, no values will be set and everything can be filled
  in manually. Use this setting and call the read() method when reading
  from an existing project file that was already generated with this class.
  Parameters must still be passed on, e.g.:
    p=tiger_project(['dchi1','dchi2'],empty=True)
    p.read(inputfile)


  Parameters
  ----------
  rundir: string
      The location of the subhypothesis folders
  bdir: string
      The subdirectory to get the Bayes files from (typically "posterior_samples")
      Can be empty "" as well if Bayes files are located inside subhyp folders
  bfiletype: int
      1: will assume Bayes file format %s
      2: will assume Bayes file format %s
  parameters: iterable
      A list of nonGR parameters to be considered, i.e. ["dchi1","dchi2"]
      Subhypotheses are automatically generated from these
  outfile: string
      File path to write the project's information to using the write() method
  timeslidesfile: string
      Optional file containing timeslides for each injected event
      The first row must have the names of the detectors
      Each following row is assumed to correspond to the event numbers
      in the Bayes files.

  Methods
  ----------
  write:
      Writes out a file containing gps times, event numbers,
      logB^hyp_noise for all subhypotheses, logOdds and if timeslidesfile is
      provided, the timeslides will be included as well.
  fetch_event_by_num:
      Return an event object by specifying its event number
  fetch_event_by_gps:
      Return an event object by specifying its gps time

  """%(_bayesfilename1,_bayesfilename2)
  def __init__(self,parameters,rundir="",bdir="",bfiletype=1,outfile="",timeslidesfile="",empty=False,verbose=False):

    self.parameters=parameters

    if not empty:
      if rundir=="":
        print "Error in tiger_project: Must provide rundir when empty=False"
        return None

    self.rundir=rundir
    self.bdir=bdir
    self.bfiletype=bfiletype

    self.outfile=outfile
    self.timeslidesfile=timeslidesfile
    self.timeslidedata={}
    self.Nsources=0
    self.Nsourcesdone=0
    self.Nsourcesabovecut=0
    self.events=[]

    if "GR" in self.parameters:
      self.parameters.remove("GR")
    self.subhypotheses=GenerateSubhypotheses(self.parameters)
    if self.outfile == "":
      pstring="_".join(self.parameters)
      self.outfile="BackgroundData_%s.dat"%pstring

    if not empty:
      if self.timeslidesfile!="":
        self.timeslidedata=ParseTimeslideFile(self.timeslidesfile)

      #Need at least the GR hypothesis so this is where it starts
      print "  searching for B files..."
      i=0
      for F in listdir(path.join(self.rundir,"GR",self.bdir)):
        filepath=path.join(self.rundir,"GR",self.bdir,F)
        if "_B.txt" in filepath:
          self.Nsources+=1
          gpstime,eventnum=self._GPSAndNumFromBayesFileName(F)
          if verbose: print "    %d - scanning event %d (gpstime %d)"%(i,eventnum,gpstime)
          i+=1
          thisevent=event(eventnum,gpstime,subhypotheses=self.subhypotheses,
                          rundir=self.rundir,bdir=self.bdir,filename=F,timeslidedata=self.timeslidedata)
          if thisevent==None:
            print "Error in tiger_project: event object is None"
            return None
          if thisevent.done:
            self.events.append(thisevent)
            self.Nsourcesdone+=1
            if thisevent.hypotheses["GR"].Bayes > 32.0:
              self.Nsourcesabovecut+=1

      print "  %d events were found finished in GR folder"%self.Nsources
      print "  %d of those are finished for all subhypotheses"%self.Nsourcesdone
      print "  %d of those have logB^GR_noise > 32.0"%self.Nsourcesabovecut


  def read(self,filename):
    """Read data from a previously generated project file.
    """
    fp=open(filename,'r')
    headerline=fp.readline()
    stringdata=fp.readlines()
    fp.close()

    table={}
    header=headerline.strip().replace('#','').split(' ')
    data=[]
    for d in stringdata:
      data.append(d.strip().split())#strings
    #convert data to a dictionary
    data=np.array(data)
    self.Nsources=len(data[:,0])
    self.Nsourcesdone=self.Nsources
    self.Nsourcesabovecut=0
    for i in range(len(header)):
      key=header[i]
      table[key]=data[:,i]

    keys=table.keys()
    if 'H1' in keys:
      self.timeslidedata['H1']=[]
    if 'L1' in keys:
      self.timeslidedata['L1']=[]
    if 'V1' in keys:
      self.timeslidedata['V1']=[]
    #note, data is still all strings
    for n in range(self.Nsources):
      num=int(table['num'][n])
      gps=int(table['gpstime'][n])
      thisevent=event(num,gps,subhypotheses=self.subhypotheses,empty=True)
      thisevent.Nsubhyps=len(self.subhypotheses)
      thisevent.LogOdds=float(table['LogOdds'][n])
      if 'H1' in keys:
        slide=int(table['H1'][n])
        thisevent.timeslides['H1']=slide
        self.timeslidedata['H1'].append(slide)
      if 'L1' in keys:
        slide=int(table['L1'][n])
        thisevent.timeslides['L1']=slide
        self.timeslidedata['L1'].append(slide)
      if 'V1' in keys:
        slide=int(table['V1'][n])
        thisevent.timeslides['V1']=slide
        self.timeslidedata['V1'].append(slide)
      #add GR hypothesis
      grhyp=hypothesis('GR',num,gps,"",empty=True)
      grhyp.Bayes=float(table['GR'][n])
      thisevent.hypotheses['GR']=grhyp
      if grhyp.Bayes > 32.0:
        self.Nsourcesabovecut+=1
      #add subhypotheses
      for hyp in self.subhypotheses:
        thishyp=hypothesis(hyp,num,gps,"",empty=True)
        thishyp.Bayes=float(table[hyp][n])
        thisevent.hypotheses[hyp]=thishyp
      #add current event
      thisevent.done=True
      self.events.append(thisevent)
    print "  %d events were found in input file"%self.Nsources
    print "  %d of those have logB^GR_noise > 32.0"%self.Nsourcesabovecut

  def write(self,filename=""):
    if filename=="":
      filename=self.outfile
    print "  writing to %s"%filename
    with open(filename,'w') as f:
      if len(self.timeslidedata.keys())==0:
        f.write( "#gpstime num %s LogOdds\n"%(" ".join(self.subhypotheses)) )
        for e in self.events:
          string="%d %d"%(e.gps,e.num)
          for hyp in self.subhypotheses:
            string+=" %.9f"%e.hypotheses[hyp].Bayes
          string+=" %.9f"%e.LogOdds
          f.write(string+"\n")
      else:
        f.write( "#gpstime %s num %s LogOdds\n"%(" ".join(self.timeslidedata.keys())," ".join(self.subhypotheses)) )
        for e in self.events:
          string="%d"%e.gps
          for ifo in self.timeslidedata.keys():
            string+=" %d"%e.timeslides[ifo]
          string+=" %d"%e.num
          for hyp in self.subhypotheses:
            string+=" %.9f"%e.hypotheses[hyp].Bayes
          string+=" %.9f"%e.LogOdds
          f.write(string+"\n")

  def fetch_event_by_num(self,num):
    for e in self.events:
      if e.num == int(num):
        return e
    print "Event %d not found"%int(num)
    return None

  def fetch_event_by_gps(self,gps):
    for e in self.events:
      if e.gps == int(gps):
        return e
    print "Event with gps time %d not found"%int(gps)
    return None

  def _GPSAndNumFromBayesFileName(self,F):
    if self.bfiletype == 1:
      #posterior_V1H1L1_GPS-EVENT.dat_B.txt
      string=F.split('_')[2] #=GPS-EVENT.dat
      string=string.split('.')[0] #=GPS-EVENT
      gpstime=int(string.split('-')[0])
      eventnum=int(string.split('-')[1])
    else:
      #lalinferencenest-EVENT-V1H1L1-GPS.0-NUMBER.dat_B.txt
      string=F.split('_')[0] #=lalinferencenest-EVENT-V1H1L1-GPS.0-NUMBER.dat
      splitdash=string.split('-') #=lalinferencenest,EVENT,V1H1L1,GPS.0,NUMBER.dat
      gpstime=float(splitdash[3])
      eventnum=int(splitdash[1])
    return int(gpstime),eventnum











