__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

import numpy as np
import lalsimulation as lalsim

from scipy.stats import gaussian_kde
from gwtoolbox.waveforms import utils as wfutils

import h5py


class parameter():
  """
  Class to store a parameter name and optionally a latex name.
  The data of the parameter can either be a single value, or a vector of values.

  input
  name : Name of the parameter e.g. "m1" or "fmin"
  data : Either a single value or a numpy array

  (optional)
  texlabel : Latex label for this parameter.
             Will be set to literal name if not provided.
             Meant for plotting purposes
  """
  def __init__(self,name,data,pmin=None,pmax=None,texlabel=""):
    self.name = name
    self.vals=data
    self.min=None
    self.max=None
    if texlabel == "":
      self.texl = r"${\rm %s}$"%self.name
    else:
      self.texl = texlabel

    self.dim = 1
    if type(data) == np.ndarray:
      self.dim = len(data)
      self.type = "array"
    elif type(data) == list:
      self.dim = len(data)
      self.type = "array"
      self.vals = np.array(data)
      self.min = min(self.vals)
      self.max = max(self.vals)
    elif type(data) == float:
      self.type = "float"
    elif type(data) == int:
      self.type = "int"
    else:
      Exception("Error: data does not have type of either numpy array, float or int")
      return None

    if pmin != None:
      self.min = pmin
    if pmax != None:
      self.max = pmax

  def __add__(self, other):
    if isinstance(other,parameter):
      newname = self.name+'+'+other.name
      if self.type == "array" and other.type == "array":
        if self.dim != other.dim:
          Exception("Error: parameters do not have the same dimension, unable to add")
          return None
      datanew = self.vals+other.vals
      return parameter(newname,datanew)
    elif isinstance(other,float):
      newname = self.name+"+%.3f"%other
      datanew = self.vals+other
      return parameter(newname,datanew)
    elif isinstance(other,np.ndarray):
      if self.dim != len(other):
        Exception("Error: parameter does not have the same dimension as array, unable to add")
        return None
      newname = self.name+"+array"
      datanew = self.vals+other
      return parameter(newname,datanew)
    else:
      return NotImplemented

  def __sub__(self, other):
    if isinstance(other,parameter):
      newname = self.name+'-'+other.name
      if self.type == "array" and other.type == "array":
        if self.dim != other.dim:
          Exception("Error: parameters do not have the same dimension, unable to subtract")
          return None
      datanew = self.vals-other.vals
      return parameter(newname,datanew)
    elif isinstance(other,float):
      newname = self.name+"-%.3f"%other
      datanew = self.vals-other
      return parameter(newname,datanew)
    elif isinstance(other,np.ndarray):
      if self.dim != len(other):
        Exception("Error: parameter does not have the same dimension as array, unable to subtract")
        return None
      newname = self.name+"-array"
      datanew = self.vals-other
      return parameter(newname,datanew)
    else:
      return NotImplemented

  def __div__(self, other):
    if isinstance(other,parameter):
      newname = self.name+'/'+other.name
      if self.type == "array" and other.type == "array":
        if self.dim != other.dim:
          Exception("Error: parameters do not have the same dimension, unable to divide")
          return None
      datanew = self.vals/other.vals
      return parameter(newname,datanew)
    elif isinstance(other,float):
      newname = self.name+"/%.3f"%other
      datanew = self.vals/other
      return parameter(newname,datanew)
    elif isinstance(other,np.ndarray):
      if self.dim != len(other):
        Exception("Error: parameter does not have the same dimension as array, unable to divide")
        return None
      newname = self.name+"/array"
      datanew = self.vals/other
      return parameter(newname,datanew)
    else:
      return NotImplemented

#  def __rdiv__(self, other):
#    if isinstance(other,parameter):
#      newname = self.name+'/'+other.name
#      if self.type == "array" and other.type == "array":
#        if self.dim != other.dim:
#          Exception("Error: parameters do not have the same dimension, unable to divide")
#          return None
#      datanew = self.vals/other.vals
#      return parameter(newname,datanew)
#    elif isinstance(other,float):
#      newname = self.name+"/%.3f"%other
#      datanew = self.vals/other
#      return parameter(newname,datanew)
#    elif isinstance(other,np.ndarray):
#      if self.dim != len(other):
#        Exception("Error: parameter does not have the same dimension as array, unable to divide")
#        return None
#      newname = self.name+"/array"
#      datanew = self.vals/other
#      return parameter(newname,datanew)
#    else:
#      return NotImplemented

  def __mul__(self, other):
    if isinstance(other,parameter):
      newname = self.name+'*'+other.name
      if self.type == "array" and other.type == "array":
        if self.dim != other.dim:
          Exception("Error: parameters do not have the same dimension, unable to multiply")
          return None
      datanew = self.vals*other.vals
      return parameter(newname,datanew)
    elif isinstance(other,float):
      newname = self.name+"*%.3f"%other
      datanew = self.vals*other
      return parameter(newname,datanew)
    elif isinstance(other,np.ndarray):
      if self.dim != len(other):
        Exception("Error: parameter does not have the same dimension as array, unable to multiply")
        return None
      newname = self.name+"*array"
      datanew = self.vals*other
      return parameter(newname,datanew)
    else:
      return NotImplemented

  def __rmul__(self, other):
    if isinstance(other,parameter):
      newname = self.name+'*'+other.name
      if self.type == "array" and other.type == "array":
        if self.dim != other.dim:
          Exception("Error: parameters do not have the same dimension, unable to multiply")
          return None
      datanew = self.vals*other.vals
      return parameter(newname,datanew)
    elif isinstance(other,float):
      newname = self.name+"*%.3f"%other
      datanew = self.vals*other
      return parameter(newname,datanew)
    elif isinstance(other,np.ndarray):
      if self.dim != len(other):
        Exception("Error: parameter does not have the same dimension as array, unable to multiply")
        return None
      newname = self.name+"*array"
      datanew = self.vals*other
      return parameter(newname,datanew)
    else:
      return NotImplemented

  def __pow__(self, other):
    if isinstance(other,parameter):
      newname = self.name+'^'+other.name
      if self.type == "array" and other.type == "array":
        if self.dim != other.dim:
          Exception("Error: parameters do not have the same dimension, unable to pow")
          return None
      datanew = self.vals**other.vals
      return parameter(newname,datanew)
    elif isinstance(other,float):
      newname = self.name+"^%.3f"%other
      datanew = self.vals**other
      return parameter(newname,datanew)
    elif isinstance(other,int):
      newname = self.name+"^%d"%other
      datanew = self.vals**other
      return parameter(newname,datanew)
    elif isinstance(other,np.ndarray):
      if self.dim != len(other):
        Exception("Error: parameter does not have the same dimension as array, unable to pow")
        return None
      newname = self.name+"^array"
      datanew = self.vals**other
      return parameter(newname,datanew)
    else:
      return NotImplemented


class basic_posterior():
  def __init__(self,data,kde_min=None,kde_max=None,kde_points=1000,kde_cov_factor=0.25,calculate_kde=True,posteriorlabel=""):
    """
    Container for the posterior samples and its kde

    Parameters
    ----------
    data: numpy array or list
        Array containing the samples

    calculate_kde: bool
        Whether KDE should be calculated and stored (True)

    kde_min: float
        min value of the region on which the KDE is built

    kde_max: float
        max value of the region on which the KDE is built

    kde_points: int
        Number of points the KDE will have

    posteriorlabel: string
        Optional label for identification


    Attributes
    ----------
    posterior: list
        posterior points extracted from the posterior file

    xaxis: numpy array
        the x-axis of the KDE

    median: float
        median of posterior

    mean: float
        mean of posterior

    stdev: float
        stdev of posterior


    Methods
    ----------
    calculate_kde([npoints,cov_factor]): None
        Calculates the KDE so it can be retrieved with get_kde()

    get_kde(): list
        The kde points calculated between xmin and xmax

    get_density(): kde density object
        Get the density object which can be used to calculate the KDE at other points
    """

    self.label=posteriorlabel
    self.posterior=data
    if type(data)!=np.ndarray:
      self.posterior=np.array(data)

    self._kde_calculated=False


    #set kde limits
    if kde_min != None:
      self.kde_min = kde_min
    else:
      self.kde_min = min(self.posterior)
    if kde_max != None:
      self.kde_max = kde_max
    else:
      self.kde_max = max(self.posterior)
    self.kde_points = kde_points
    self.kde_cov_factor = kde_cov_factor

    if calculate_kde:
      self.calculate_kde()

    #some statistics
    self.median = np.median(self.posterior)
    self.mean = np.mean(self.posterior)
    self.stdev = np.std(self.posterior)
    #dx_l,dx_u = np.percentile(d,[5]),np.percentile(d,[95])

  def calculate_kde(self,npoints=0,cov_factor=None):
    if npoints != 0:
      self.kde_points = npoints
    if cov_factor != None:
      self.kde_cov_factor = cov_factor
    self.xaxis = np.linspace(self.kde_min,self.kde_max,self.kde_points)
    self.density = gaussian_kde(self.posterior)
    self.density.covariance_factor = lambda : self.kde_cov_factor
    self.density._compute_covariance()
    self.kde = self.density(self.xaxis)
    self._kde_calculated = True

  def get_kde(self):
    if self._kde_calculated:
      return self.kde
    else:
      print "Warning: No KDE was calculated"
      return []

  def get_density(self):
    if self._kde_calculated:
      return self.density
    else:
      print "Warning: No KDE was calculated"
      return None



class posterior():
  def __init__(self,sourcename,paramname,posteriorfile,kde_min=None,kde_max=None,headerfile="",kde_points=1000,kde_cov_factor=0.25,calculate_kde=True,HDF5=False):
    """
    Container for the posterior samples and its kde

    Parameters
    ----------
    sourcename: string
        Simply a label, not used in the class itself

    paramname: string
        The parameter to extract from the posterior file

    posteriorfile: string
        File containing posterior for at least the requested parameter

    headerfile: string
        Required if the posterior file does not contain a header itself

    calculate_kde: bool
        Whether KDE should be calculated and stored (True)

    kde_min: float
        min value of the region on which the KDE is built

    kde_max: float
        max value of the region on which the KDE is built

    kde_points: int
        Number of points the KDE will have


    Attributes
    ----------
    posterior: list
        posterior points extracted from the posterior file

    xaxis: numpy array
        the x-axis of the KDE

    median: float
        median of posterior

    mean: float
        mean of posterior

    stdev: float
        stdev of posterior


    Methods
    ----------
    calculate_kde([npoints,cov_factor]): None
        Calculates the KDE so it can be retrieved with get_kde()

    get_kde(): list
        The kde points calculated between xmin and xmax

    get_density(): kde density object
        Get the density object which can be used to calculate the KDE at other points
    """

    self.file = posteriorfile
    self.paramname = paramname
    self.sourcename = sourcename
    self.posterior=[]

    self._kde_calculated=False

    # Read the posterior
    if HDF5:
      fp = h5py.File(posteriorfile,'r')
      posterior_samples = fp.get("lalinference/lalinference_nest/posterior_samples")
      if paramname in posterior_samples.keys():
        self.posterior = np.array(posterior_samples.get(paramname))
      elif paramname == "eta":
        q = np.array(posterior_samples.get("q"))
        self.posterior = wfutils.convert_q_to_eta(q)
      elif paramname == "mchirp" or paramname == "mc":
        self.posterior = np.array(posterior_samples.get("chirpmass"))
      elif paramname == "mass1" or paramname == "m1":
        q = np.array(posterior_samples.get("q"))
        mc = np.array(posterior_samples.get("chirpmass"))
        self.posterior,m2 = wfutils.convert_Mcq_to_m1m2(mc,q)
      elif paramname == "mass2" or paramname == "m2":
        q = np.array(posterior_samples.get("q"))
        mc = np.array(posterior_samples.get("chirpmass"))
        m1,self.posterior = wfutils.convert_Mcq_to_m1m2(mc,q)
      else:
        print "parameter %s not present in posterior file"%paramname

    else: # not HDF5
      with open(posteriorfile,'r') as f:
        if headerfile == "":
          header = f.readline().strip().split() #get header
        else:
          header = np.genfromtxt(headerfile,dtype=str)

        #create a parameter:index dictionary
        cols = {}
        for p,i in zip(header,range(len(header))):
          cols[p] = i

        if cols.has_key(paramname):
          data = f.readlines()
          c = cols[paramname]
          for line in data:
            self.posterior.append(float(line.split()[c]))
        elif paramname == "q":
          data = f.readlines()
          self.posterior = self._q_from_eta(data,cols)
        elif paramname == "eta":
          data = f.readlines()
          self.posterior = self._eta_from_q(data,cols)
        elif paramname in ["mass1","mass2"]:
          data = f.readlines()
          print "calculating %s from other parameters"%paramname
          if cols.has_key("logmc"):
            if paramname == "mass1":
              self.posterior = self._mass1_from_logmc_eta(data,cols)
            else:
              self.posterior = self._mass2_from_logmc_eta(data,cols)
          else:
            if paramname == "mass1":
              self.posterior = self._mass1_from_mc_eta(data,cols)
            else:
              self.posterior = self._mass2_from_mc_eta(data,cols)
        else:
          print "parameter %s not present in posterior file"%paramname

    #set kde limits
    if kde_min != None:
      self.kde_min = kde_min
    else:
      self.kde_min = min(self.posterior)
    if kde_max != None:
      self.kde_max = kde_max
    else:
      self.kde_max = max(self.posterior)
    self.kde_points = kde_points
    self.kde_cov_factor = kde_cov_factor

    if calculate_kde:
      self.calculate_kde()

    #some statistics
    self.median = np.median(self.posterior)
    self.mean = np.mean(self.posterior)
    self.stdev = np.std(self.posterior)
    #dx_l,dx_u = np.percentile(d,[5]),np.percentile(d,[95])

  def calculate_kde(self,npoints=0,cov_factor=None):
    if npoints != 0:
      self.kde_points = npoints
    if cov_factor != None:
      self.kde_cov_factor = cov_factor
    self.xaxis = np.linspace(self.kde_min,self.kde_max,self.kde_points)
    self.density = gaussian_kde(self.posterior)
    self.density.covariance_factor = lambda : self.kde_cov_factor
    self.density._compute_covariance()
    self.kde = self.density(self.xaxis)
    self._kde_calculated = True

  def get_kde(self):
    if self._kde_calculated:
      return self.kde
    else:
      print "Warning: No KDE was calculated"
      return []

  def get_density(self):
    if self._kde_calculated:
      return self.density
    else:
      print "Warning: No KDE was calculated"
      return None

  def _eta_from_q(self,data,coldict):
    pos = []
    c = coldict["q"]
    for line in data:
      q = float(line.split()[c])
      eta = wfutils.convert_q_to_eta(q)
      pos.append(eta)
    return pos

  def _q_from_eta(self,data,coldict):
    pos = []
    c = coldict["eta"]
    for line in data:
      eta = float(line.split()[c])
      q = wfutils.convert_eta_to_q(eta)
      pos.append(q)
    return pos

  def _mass1_from_logmc_eta(self,data,coldict):
    pos = []
    c_eta = coldict["eta"]
    c_logmc = coldict["logmc"]
    for line in data:
      mc = np.exp(float(line.split()[c_logmc]))
      eta = float(line.split()[c_eta])
      M = mc*pow(eta,-3./5.)
      mass1,mass2 = wfutils.convert_Meta_to_m1m2(M,eta)
      pos.append(mass1)
    return pos

  def _mass2_from_logmc_eta(self,data,coldict):
    pos = []
    c_eta = coldict["eta"]
    c_logmc = coldict["logmc"]
    for line in data:
      mc = np.exp(float(line.split()[c_logmc]))
      eta = float(line.split()[c_eta])
      M = mc*pow(eta,-3./5.)
      mass1,mass2 = wfutils.convert_Meta_to_m1m2(M,eta)
      pos.append(mass2)
    return pos

  def _mass1_from_mc_eta(self,data,coldict):
    pos = []
    c_eta = coldict["eta"]
    c_logmc = coldict["mc"]
    for line in data:
      mc = float(line.split()[c_logmc])
      eta = float(line.split()[c_eta])
      M = mc*pow(eta,-3./5.)
      mass1,mass2 = wfutils.convert_Meta_to_m1m2(M,eta)
      pos.append(mass1)
    return pos

  def _mass2_from_mc_eta(self,data,coldict):
    pos = []
    c_eta = coldict["eta"]
    c_logmc = coldict["mc"]
    for line in data:
      mc = float(line.split()[c_logmc])
      eta = float(line.split()[c_eta])
      M = mc*pow(eta,-3./5.)
      mass1,mass2 = wfutils.convert_Meta_to_m1m2(M,eta)
      pos.append(mass2)
    return pos


class AvailableNonGRparameters():

  def __init__(self):
    #SPA Inspiral parameters
    self.dchis = ["dchi0","dchi1","dchi2","dchi3","dchi4","dchi5","dchi5l","dchi6","dchi6l","dchi7"]
    #Phenomenological inspiral parameters
    self.dsigmas = ["dsigma1","dsigma2","dsigma3","dsigma4"]
    #Intermediate phase parameters
    self.dbetas = ["dbeta1","dbeta2","dbeta3"]
    #Merger-ringdown parameters
    self.dalphas = ["dalpha1","dalpha2","dalpha3","dalpha4","dalpha5"]

    #all
    self.all_params =  self.dchis+self.dsigmas+self.dbetas+self.dalphas
    #latex labels
    self.paramslatex={}
    for p in self.all_params:
      if p[:-1] == "dxi":
        self.paramslatex[p]=r'$\delta\hat{\xi}_{%s}$'%p[-1]
      elif p[:-1] == "dalpha":
        self.paramslatex[p]=r'$\delta\hat{\alpha}_{%s}$'%p[-1]
      elif p[:-1] == "dbeta":
        self.paramslatex[p]=r'$\delta\hat{\beta}_{%s}$'%p[-1]
      elif p[:-1] == "dsigma":
        self.paramslatex[p]=r'$\delta\hat{\sigma}_{%s}$'%p[-1]
      elif p[:-1] == "dchi":
        self.paramslatex[p]=r'$\delta\hat{\varphi}_{%s}$'%p[-1]
      else:
        if p[:-1] == "dchi5":
          self.paramslatex[p]=r'$\delta\hat{\varphi}^l_5$'
        elif p[:-1] == "dchi6":
          self.paramslatex[p]=r'$\delta\hat{\varphi}^l_6$'
        else:
          self.paramslatex[p]=r'$\delta\hat{\xi}_{%s}$'%p[-1]

  def dchis(self):
    return np.array(self.dchis)
  def dsigmas(self):
    return np.array(self.dsigmas)
  def dbetas(self):
    return np.array(self.dbetas)
  def dalphas(self):
    return np.array(self.dalphas)
  def allparams(self):
    return np.array(self.dchis+self.dsigmas+self.dbetas+self.dalphas)
  def labeldict(self):
    return self.paramslatex


class nonGRparameters():

  def __init__(self,nonGRdict):
    self.available = AvailableNonGRparameters()
    self._dict = nonGRdict
    self._lal = None

  def testlalstruct(self,LALextraParams=None):
    """
    Test if the lal structure is ok by printing all the
    nongr parameters that are included.
    Any other SimInspiralTestGRParams object can be passed in via
    the LALextraParams option
    """
    teststruct=self.get_lal()
    if LALextraParams != None:
       teststruct=LALextraParams
    for p in self.available.allparams():
      if lalsim.SimInspiralTestGRParamExists(teststruct,p):
        shft = lalsim.SimInspiralGetTestGRParam(teststruct,p)
        print p+" = %.3f"%shft


  def _to_lalsim(self,verbose=0):
    """
    Wrapper function to turn dictionary of non-GR parameters
    into a lalsimulation "extraParams" structure required for waveforms.
    """
    extraParams = None
    keys = self._dict.keys()
    if len(keys)>0:
      extraParams = lalsim.SimInspiralCreateTestGRParam(keys[0],self._dict[keys[0]])
      if(verbose): print "adding %s with value %.2f"%(keys[0],self._dict[keys[0]])
      for i in range(1,len(keys)):
        name  = keys[i]
        value = self._dict[ name ]
        if(verbose): print "adding %s with value %.2f"%(name,value)
        extraParams = lalsim.SimInspiralAddTestGRParam(extraParams,name,value)

    return extraParams

  def get_lal(self):
    if self._lal == None:
      self._lal = self._to_lalsim()
      return self._lal
    else:
      return self._lal

  def get_dict(self):
    return self._dict





