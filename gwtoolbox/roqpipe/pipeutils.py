__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

from os import path,system
import numpy as np
import ast

from uuid import uuid4
from gwtoolbox.waveforms import utils as wu
import gwtoolbox.genutils as gu

from glob import glob

def estimate_memory(ts_size,param_dim,max_rb,num_workers,quad_size):
  """Estimate memory in GB for a greedycpp job

  Parameters
  ----------
  ts_size: int
      size of the training set

  param_dim: int
      number of parameters used in model

  max_rb: int
      maximum allowed number of greedy points as set in the config file

  num_workers: int
      number of workers for MPI scenario (1 if using greedyOMP)

  quad_size: int
      length of the frequency vector
  """

  sizeof_double = 8.0e-9 # double in GB
  memory_worker = sizeof_double*( ts_size*param_dim + num_workers + 2*max_rb*ts_size/num_workers + 2*quad_size*ts_size/num_workers )
  memory_master = 2*sizeof_double*( num_workers + quad_size*max_rb + max_rb*max_rb) + sizeof_double*ts_size*param_dim

  return memory_worker*num_workers + memory_master*5.0

def generate_taylorf2_quadratic(fmin,fmax,seglen,outpath):
  """Create the analytic quadratic basis for TaylorF2
  The output are the files B_quadratic.npy and fnodes_quadratic.npy
  which are compatible with lalinference.

  fnodes_quadratic.npy contains 1 node: F_1 = fmin
  B_quadratic.npy contains 1 row vector: B_1 = (f/F_1)^(-14/6)

  Parameters
  ----------
  fmin : float
      lower frequency, this will be the frequency node
  fmax : float
      upper frequency
  seglen : float or int
      this, together with fmin and fmax will determine the length of B
  outpath : string
      The two files are written to this folder. The directory
      is assumed to exist.
  """
  N = int((fmax-fmin)*seglen) + 1
  B = np.array([[pow(f/fmin,-14./6.) for f in np.linspace(fmin,fmax,N)]])
  F = np.array([fmin])
  np.save(path.join(outpath, 'B_quadratic.npy'), B)
  np.save(path.join(outpath, 'fnodes_quadratic.npy'), F)


class roqpipe_file():
  """filenames container to store local paths and remote paths

  Parameters
  ----------
  path_local : string
      A directory that is always accessible from the run location
  path_remote : string
      A directory where files are uploaded to (relative to filesystem home)
  name : string
      Basename of the file
  unpack : bool
      True if the file needs to be unpacked at the run location (default: False)

  Methods
  -------
  get_local : return the full local path
  get_remote : return the full remote path relative to filesystem home
  get_name : return the basename of the file
  set_local : overwrite the local path
  set_remote : overwrite the remote path
  set_name : overwrite the basename

  Notes
  -----
  Currently there is no consistency check if the basenames of all paths
  are the same. set-functions should also make sure of this.
  """
  def __init__(self,path_local,path_remote,name,unpack=False):
    self._path_local = path_local
    self._path_remote = path_remote
    self._name = name
    self.unpack = unpack

  def get_local(self):
    return path.join(self._path_local,self._name)
  def get_remote(self):
    return path.join(self._path_remote,self._name)
  def get_name(self):
    return self._name

  def set_local(self,ldir):
    self._path_local = ldir
  def set_remote(self,rdir):
    self._path_remote = rdir
  def set_name(self,fname):
    self._name = fname

class cfg_entry():
  """
  Container for one entry in the greedy cfg files.
  It will hold information as
    option=value; //comment

  Parameters
  ----------
  option : string
  value : string
  comment : string

  Methods
  -------
  return_string
      returns the option,value,comment set as a properly formatted cfg string
  """
  def __init__(self,option,value,comment=""):
    self.option=option
    self.value=value
    self.comment=comment

  def return_string(self):
    if self.comment != "":
      s = "%s = %s; //%s\n"%(self.option,self.value,self.comment)
    else:
      s = "%s = %s;\n"%(self.option,self.value)
    return s

class greedycpp_cfg():
  """
  Class to create a cfg file for greedycpp
  """
  def __init__(self,description=[]):
    """
    Parameters
    ----------
    description : array of strings
       Each element will be printed as a comment line at the top of the cfg file

    Attributes
    ----------
    location : string
        The location of the file, will be generated with the init method

    Methods
    -------
    init
        Will setup all the file contents and run an automatic sanity check
    add_option
        Adds an entry to the cfg file
    get
        Will retrieve a value
    write
        Writes the cfg to file
    read
        Reads contents from an existing cfg file
    check
        Sanity check of cfg contents
    """

    self.descr=description
    self._options={}
    self._ok=False
    self.location=""

  def init(self,quad_type=1,model_name="PhenomP_plus",weighted_inner=False,load_from_file=True,ts_file=None,
           x_min=20,x_max=1024,quad_points=8033,scales=[],seed=0,tol=1.0e-12,max_RB=5000,export_tspace=False,
           output_dir="",output_data_format="npy",parameters=["mass1","mass2"],params_num=[],params_low=[],params_high=[],
           quad_nodes_file=None,num_weight_file=None,site="nikhefgrid",model_name_quadratic="PhenomP_hpPLUShcSquared",quadraticmode=False):
    """
    The initialization function for the cfg. After running this, a sanity check
    is performed make sure the contents are self-consistent and complete.

    Parameters
    ----------
    quad_type : int
        see greedycpp documentation (type 0 does not work yet)
    model_name : string
        see greedycpp documentation
    model_name_quadratic : string
        replaces model_name if quadraticmode = True
    quadraticmode : bool
        True if this cfg is meant for a quadratic basis
    weighted_inner : bool
        see greedycpp documentation
    load_from_file : bool
        see greedycpp documentation
    ts_file : string
        see greedycpp documentation
    x_min : float
        see greedycpp documentation
    x_max : float
        see greedycpp documentation
    quad_points : int
        see greedycpp documentation
    seed : int
        see greedycpp documentation
    tol : float
        see greedycpp documentation
    max_RB : int
        see greedycpp documentation
    export_tspace : bool
        see greedycpp documentation
    output_dir : string
        see greedycpp documentation
    output_data_format : string
        see greedycpp documentation
    parameters : array_like
        The set of parameters this model uses
    params_num : array_like
        see greedycpp documentation (must be same length as parameters)
    params_low : array_like
        see greedycpp documentation (must be same length as parameters)
    params_high : array_like
        see greedycpp documentation (must be same length as parameters)
    scales : array_like
        see greedycpp documentation (must be same length as parameters)
    quad_nodes_file : string
        see greedycpp documentation (must be same length as parameters)
    num_weight_file : string
        see greedycpp documentation (must be same length as parameters)
    site : string
        Which computing platform to run on. Can be either nikhefgrid
        stoomboot, or local (i.e. no job scheduler)

    """

    self.site = site
    self.add_option("quad_type",int(quad_type))
    if quad_type == 1:
      self.add_option("x_min",x_min)
      self.add_option("x_max",x_max)
      self.add_option("quad_points",quad_points)
    elif quad_type == 2:
      if quad_nodes_file != None:
        if site == "nikhefgrid":
          self.add_option("quad_nodes_file",quad_nodes_file.get_name())
        else:
          self.add_option("quad_nodes_file",quad_nodes_file.get_local())
      else:
        print "Error: quad_type=2, but no quad_nodes_file file is provided"
      if num_weight_file != None:
        if site == "nikhefgrid":
          self.add_option("num_weight_file",num_weight_file.get_name())
        else:
          self.add_option("num_weight_file",num_weight_file.get_local())
      else:
        print "Error: quad_type=2, but no num_weight_file file is provided"
    else:
      print "My Bad: I don't know what to do with quad_type=0"

    if quadraticmode:
      self.add_option("model_name",model_name_quadratic)
    else:
      self.add_option("model_name",model_name)

    if weighted_inner:
      print "My Bad: I don't know what to do with weighted_inner=true"
    else:
      self.add_option("weighted_inner",weighted_inner)
    self.add_option("load_from_file",load_from_file)
    if load_from_file:
      if ts_file != None:
        if self.site == "nikhefgrid":
          self.add_option("ts_file",ts_file.get_name())
        else:
          self.add_option("ts_file",ts_file.get_local())
      else:
        print "Error: \"load_from_file\" is set, but no trainingset file is provided"
    else:
      if len(params_num)==0 or len(params_low)==0 or len(params_high)==0 or len(parameters)==0:
        print "Error: \"load_from_file=false\", but one of parameters,params_num,_low,_high does not have at least on element"
      else:
        if len(params_num) != len(params_low) or  len(params_num) != len(params_high) or  len(params_num) != len(parameters):
          print "Error: \"load_from_file=false\", but parameters,params_num,_low,_high do not have the same length"
        else:
          self.add_option("params_num",params_num)
          self.add_option("params_low",params_low)
          self.add_option("params_high",params_high)

    self.add_option("param_dim",len(parameters))
    if len(scales) == 0:
      scales=np.ones(len(parameters))
    if len(scales) == len(parameters):
      for i in range(len(parameters)):
        self.add_option("p%d_scale"%(i+1),scales[i],comment=parameters[i])

    self.add_option("seed",seed)
    self.add_option("tol",tol)
    self.add_option("max_RB",max_RB)
    self.add_option("export_tspace",export_tspace)
    if output_dir != "":
      self.add_option("output_dir",output_dir)
    else:
      print "Error: No output_dir provided"
    self.add_option("output_data_format",output_data_format)

    #Check if everything is present in the cfg file
    self.check()

  def add_option(self,option,value,comment=""):
    """ Add an option,value,comment set to the cfg
    value is automatically checked for type.
    The set is interpreted as follows:
    option=value;//comment

    Parameters
    ----------
    option : string
    value : bool,string,float or int
        The datatype will be checked and formatted accordingly
    comment : string
    """
    if type(value) == type(True):
      if value == True:
        v = "true"
      else:
        v = "false"
    elif type(value) == type(1.0):
      if value > 1.0e4 or value < 1.0e-4:
        v = "%.9e"%value
      else:
        v = "%.9f"%value
    elif type(value) == type('a'):
      v = "\"%s\""%value
    else:
      try:
        v = str(value)
      except:
        print "Error: Could not convert type to string in adding option \"%s\""%option
        v=""
    if v != "":
      self._options[option]=cfg_entry(option,v,comment)

  def get(self,option):
    """Return a value from a cfg option

    Parameters
    ----------
    option : string
        name of the option to get the value from

    Returns
    -------
    value : string
        The value is always returned as a string
    """
    if self._options.has_key(option):
      entry = self._options[option]
      return entry.value.strip("\"")
    else:
      print "Warning/Error, you deal with it: cfg file has no option \"%s\""%option
      return None

  def write(self,outfile):
    """Write the cfg to file
    The file is only written if the cfg passed the sanity check.

    Parameters
    ----------
    outfile : string
        Full path of the file to write to
    """
    if self._ok:
      with open(outfile,"w") as f:
        if len(self.descr)>0:
          for d in self.descr:
            f.write("// %s\n"%d)
        for k in self._options.keys():
          entry = self._options[k]
          f.write(entry.return_string())
      self.location = outfile
    else:
      print "cfg has not passed check yet. No output is written to file"

  def read(self,inputfile):
    """Read contents from an existing cfg file
    This will overwrite all contents created with the init method.
    A sanity check is automatically performed on the contents.

    Parameters
    ----------
    inputfile : string
        Full path of the file to read
    """
    if path.isfile(inputfile):
      self._options={}
      self._ok=False
      with open(inputfile,"r") as f:
        lines = f.readlines()
        for line in lines:
          if not line.startswith("//"):
            option=line.split("=")[0]
            rest=line.split("=")[1].strip()
            value = rest.split(";")[0]
            comment = rest.split(";")[1].replace("//","")
            self._options[option.strip()]=cfg_entry(option.strip(),value.strip(),comment.strip())
      self.check()
    else:
      print "Error: %s does not exist"%inputfile

  def check(self):
    """Perform a sanity check on the contents of this cfg file
    """
    required_arguments=["model_name","quad_type","weighted_inner","param_dim",
    "load_from_file","seed","tol","max_RB","output_dir","output_data_format"]
    print "checking cfg contents"
    is_ok=True
    for a in required_arguments:
      if not self._options.has_key(a):
        print "Error: cfg is missing required argument %s"%a
        is_ok=False
    if is_ok:
      if self._options["quad_type"].value == "1":
        if not self._options.has_key("x_min"):
          print "Error: cfg is missing required argument x_min"
          is_ok=False
        if not self._options.has_key("x_max"):
          print "Error: cfg is missing required argument x_max"
          is_ok=False
        if not self._options.has_key("quad_points"):
          print "Error: cfg is missing required argument quad_points"
          is_ok=False
      else:
        if not self._options.has_key("quad_nodes_file"):
          print "Error: cfg is missing required argument quad_nodes_file"
          is_ok=False
        if not self._options.has_key("num_weight_file"):
          print "Error: cfg is missing required argument num_weight_file"
          is_ok=False
      if self._options["load_from_file"].value == "true" or self._options["load_from_file"].value == True:
        if not self._options.has_key("ts_file"):
          print "Error: cfg is missing required argument ts_file"
          is_ok=False
      else:
        if not self._options.has_key("params_num"):
          print "Error: cfg is missing required argument params_num"
          is_ok=False
        if not self._options.has_key("params_low"):
          print "Error: cfg is missing required argument params_low"
          is_ok=False
        if not self._options.has_key("params_high"):
          print "Error: cfg is missing required argument params_high"
          is_ok=False

    if is_ok:
      print "cfg is ok"
      self._ok=True
    else:
      print "There were errors while checking the cfg file"
      self._ok=False


class greedycpp_jdl():
  """
  Class to generate a job description file to run greedycpp on the Nikhef GRID
  """
  def __init__(self,script,stdout,stderr,inputsandbox=[],sandboxurl="gsiftp://localhost",memrequest=0,timerequest=0,cpus=0,retry=3):
    """Setup JDL for running a greedycpp job on the Nikhef Grid

    Paramters
    ---------
    script : string
        Location of the script that the job will run
    stdout : string
        filename for the standard output
    stderr : string
        filename for the standard error
    inputsandbox : array_like
        A list of files to upload to the input sandbox
    sandboxurl : string
        The default sandbox url is "gsiftp://localhost", which makes it possible
        to retrieve contents after run with the glite-ce-job-output command
    memrequest : int
        Amount of memory to request in MB
    timerequest : int
        Amount of CPU time to request in minutes
    cpus : int
        Number of threads to use for an OMP run
    retry : int
        Number of retries before job is cancelled
    """
    self._options={}
    self.location = ""

    if script not in inputsandbox:
      inputsandbox.append(script)

    self._options["Type"] = "Job"
    self._options["JobType"] = "Normal"

    self._options["Executable"] = "/bin/sh"
    self._options["Arguments"] = path.basename(script)

    self._options["StdOutput"] = stdout
    self._options["StdError"] = stderr

    self._options["InputSandbox"] = inputsandbox
    self._options["OutputSandbox"] = [stdout,stderr]
    self._options["OutputSandboxBaseDestURI"] = sandboxurl

    CERequirements = []
    if memrequest > 0:
      CERequirements.append("other.GlueHostMainMemoryRAMSize >= %d"%memrequest)
    if timerequest > 0:
      CERequirements.append("other.GlueCEPolicyMaxCPUTime >= %d"%timerequest)
    self._options["CERequirements"]=CERequirements

    if cpus > 0:
      self._options["SMPGranularity"] = cpus
      self._options["CPUNumber"] = cpus

    self._options["ShallowRetryCount"] = retry

  def write(self,outfile):
    """Write the JDL contents to file

    Parameters
    ----------
    outfile : string
        File path to write to
    """
    keys = self._options.keys()
    with open(outfile,"w") as f:
      f.write("[\n")
      for k in keys:
        val = self._options[k]
        if type(val) == type('a'):
          f.write("%s = \"%s\";\n"%(k,val))
        elif type(val) == type(1):
          f.write("%s = %d;\n"%(k,val))
        else:
          if len(val) == 1:
            if type(val[0]) == type(1):
              f.write("%s = %d;\n"%(k,val[0]))
            else:
              f.write("%s = \"%s\";\n"%(k,str(val[0])))
          elif len(val) > 1:
            vals = ["\"%s\""%v for v in val]
            if k != "CERequirements":
              vals = ["\"%s\""%v for v in val]
              f.write("%s = {%s};\n"%( k, ",".join(vals) ))
            else:
              f.write("%s = \"%s\";\n"%( k, " && ".join(val) ))
      f.write("]")
    self.location = outfile


def generate_grid_script(runlabel,lfc_home,lfc_workdir,outputfilename="",remotefiles=[],cfgfile="greedy.cfg",outputtype="npy",
                         lfc_host="lfcserver.cnaf.infn.it",srm_virgo="srm://tbn18.nikhef.nl/dpm/nikhef.nl/home/virgo",
                         out="greedy.sh",numthreads=1):
  """
  Create the bash script that is to be run on the remote machine.
  This is the script that is also passed to the JDL.

  Parameters
  ----------
  runlabel : string
      A label used to create output
  lfc_home : string
      The home directory on the LFC
  lfc_workdir : string
      Directory relative to lfc_home where output is written to
  outputfilename : string
      The filename of the tar.gz file that will contain all the greedycpp output
  remotefiles : array_like
      A list of roqpipe_file objects. These files will be uploaded to a remote
      storage facility and downloaded to the worknode at runtime.
  cfgfile : string
      Name of the greedycpp config file
  outputtype : string
      Type of the greedycpp output (e.g. npy)
  lfc_host : string
      Location of the LFC host
  srm_virgo : string
      Location of the remote storage facility
      including the path to the Virgo directory
  out : string
      Name of this script
  numthreads : int
      The number of threads to use
      (must be <= what is set in JDL, SHOULD be equal)

  Returns
  -------
  remoteout : roqpipe_file
      Location information on the tar.gz containing all the output
  """
  fp = open(out,"w")

  #activate debugging
  fp.write("set -x\n")
  #exit in case of error
  fp.write("set -e\n")
  #Set some env variables
  fp.write("export LFC_HOST='%s'\n"%lfc_host)
  fp.write("export LFC_HOME='%s'\n"%lfc_home)
  fp.write("export SRM_VIRGO='%s'\n"%srm_virgo)

  #get files from remote storage
  for rf in remotefiles:
    fp.write("echo \"fetching remote file \\\"$LFC_HOME/%s\\\"\" \n"%rf.get_remote())
    fp.write("lcg-cp --vo virgo lfn:$LFC_HOME/%s file://$(pwd)/%s \n"%(rf.get_remote(),rf.get_name()))
    if rf.unpack == True:
      fp.write("tar xzf %s \n"%rf.get_name())
  fp.write("echo\n")
  fp.write("echo \"directory contents:\"\n")
  fp.write("ls -l\n")
  fp.write("echo\n")

  #set the library path
  fp.write("export LD_LIBRARY_PATH=\"$(pwd):$LD_LIBRARY_PATH\"\n")
  fp.write("echo \"LD_LIBRARY_PATH:\"\n")
  fp.write("echo \"$LD_LIBRARY_PATH\"\n")
  fp.write("echo\n")

  #check libs
  fp.write("echo \"greedy libs:\"\n")
  fp.write("ldd ./greedyOMP\n")
  fp.write("echo\n")

  #set number of threads
  fp.write("echo \"setting omp_num_threads to %d\"\n"%numthreads)
  fp.write("export OMP_NUM_THREADS=%d\n"%numthreads)
  fp.write("echo \n")

  #run greedy
  fp.write("echo \"==================== greedycpp output ====================\"\n")
  fp.write("echo `date`\n")
  fp.write("./greedyOMP %s\n"%cfgfile)
  fp.write("echo `date`\n")
  fp.write("echo \"================== end greedycpp output ==================\"\n")
  fp.write("echo \n")

  #run eim
  fp.write("echo \"======================= eim output =======================\"\n")
  fp.write("echo `date`\n")
  fp.write("./eim run_settings.cfg ./ %s\n"%outputtype)
  fp.write("echo `date`\n")
  fp.write("echo \"===================== end eim output =====================\"\n")
  fp.write("echo \n")

  #check directory contents
  fp.write("echo \"directory contents after running:\" \n")
  fp.write("ls -l \n")
  fp.write("echo \n")

  #pack in the output
  if outputfilename == "":
    outputfilename = "%s.tar.gz"%runlabel
  #create a unique identifier to prevent write conflicts
  #remoteout = path.join(lfc_workdir,outputfilename+"_%s"%uuid4())
  remoteout = roqpipe_file("",lfc_workdir,outputfilename+"_%s"%uuid4())

  fp.write("echo \"creating output package\"\n")
  filelist = ["ApproxErrors.txt", "GreedyPoints.txt", "run_settings.cfg", "validations_setup.cfg",
              "quad_rule.txt", "EIM_indices.txt", "EIM_nodes.txt" ]
  if outputtype == "npy":
    filelist += ["invV.npy", "Basis.npy", "R.npy"]
  else:
    filelist += ["invV.bin", "Basis.bin", "R.bin"]
  fp.write("tar czf %s %s\n"%(outputfilename," ".join(filelist)))

  #check the packed list of files
  fp.write("echo \"containing:\" \n")
  fp.write("tar -ztvf %s \n"%outputfilename)
  fp.write("echo \n")

  #send off the output
  fp.write("echo \"Sending packed output to srm (lfn:$LFC_HOME/%s)\" \n"%remoteout.get_remote())
  fp.write("lcg-cr --vo virgo -d $SRM_VIRGO/%s -l lfn:$LFC_HOME/%s \"file://$(pwd)/%s\" \n"%(remoteout.get_remote(),remoteout.get_remote(),outputfilename))

  #clean up most files
  fp.write("rm *.so* %s eim greedyOMP %s\n"%(outputfilename," ".join(filelist)))

  fp.write("exit\n")

  return remoteout


class greedycpp_job():

  def __init__(self,config,quadratic=False):
    """Create a complete greedycpp job from a config file

    Parameters
    ----------
    config : ConfigParser
        ConfigParser object containing all the roqpipe settings
        For an example config file see the example directory of gwtoolbox
    quadratic : bool
        True if a job must be built for constructing a quadratic basis

    Attributes
    ----------
    config : ConfigParser
        ConfigParser object - May be updated and different from the one
        that was provided as a parameter to this function.
    """

    self.config=config
    self.site="nikhefgrid"
    self.rundir=""
    self.quadmode=quadratic

    if config.has_option('paths','rundir'):
      self.rundir=config.get('paths','rundir')
    else:
      exit("Error: must provide rundir")

    if config.has_option('filesystem','job-label'):
      self.label = config.get('filesystem','job-label')
      if self.quadmode:
        self.label+="_quad"
        config.set('filesystem','job-label',self.label)
    else:
      print "Warning: no label specified in filesystem section."
      self.label = path.basename(self.rundir)
      if self.quadmode:
        self.label+="_quad"
      print "         Using %s"%(self.label)

    if config.has_option('grid','site'):
      self.site = config.get('grid','site')
      if self.site not in ["nikhefgrid", "stoomboot", "local"]:
        exit("Error: site must be either nikhefgrid, stoomboot or local")
    else:
      exit("Error: must provide site (nikhefgrid, stoomboot or local)")



    ####### Parse model options #######
    self.xmin=20.0
    self.xmax=1024.0
    self.quadpoints=8033
    self.nodesfile=None
    self.weightsfile=None
    self.parameterscales=[]
    self.weighted_inner=False
    self.load_ts_fromfile=True
    self.tsfile=None
    self.params_num=[]
    self.params_low=[]
    self.params_high=[]
    self.multiband=[]

    if config.has_option('model','model-name'):
      self.modelname=config.get('model','model-name')
    else:
      exit("Error: no model-name specified in config file")
    if config.has_option('model','model-quadname'):
      self.modelquadname=config.get('model','model-quadname')
    else:
      exit("Error: no model-quadname specified in config file")

    if config.has_option('model','parameters'):
      self.parameters=ast.literal_eval(config.get('model','parameters'))
    else:
      exit("Error: no parameters specified in config file")

    if config.has_option('model','scales'):
      scales = ast.literal_eval(config.get('model','scales'))
      self.parameterscales=[float(x) for x in scales]

    if config.has_option('model','multiband'):
      multiband = ast.literal_eval(config.get('model','multiband'))
      self.multiband = [float(m) for m in multiband]
      self.quad_type = 2
      config.set('model','quad-type','2')
      #get the filenames to write to
      if not config.has_option('model','nodes-file'):
        self.nodesfile=roqpipe_file(self.rundir,"","multiband_nodes.txt")
        config.set('model','nodes-file',self.nodesfile.get_local())
      else:
        f = config.get('model','nodes-file')
        self.nodesfile=roqpipe_file(path.dirname(f),"",path.basename(f))
      if not config.has_option('model','weights-file'):
        self.weightsfile=roqpipe_file(self.rundir,"","multiband_weights.txt")
        config.set('model','weights-file',self.weightsfile.get_local())
      else:
        f = config.get('model','weights-file')
        self.weightsfile=roqpipe_file(path.dirname(f),"",path.basename(f))
      if config.has_option('model','mchirp-min'):
        mchirpmin=config.getfloat('model','mchirp-min')
      else:
        exit("Error: when multibanding is requested, must specify mchirp-min")
      if config.has_option('model','compmass-min'):
        compmin = config.getfloat('model','compmass-min')
      else:
        compmin = 1.0
      if config.has_option('model','fudge-factor'):
        self.multiband_fudgefactor = config.getfloat('model','fudge-factor')
      else:
        self.multiband_fudgefactor = 5.0
      m1,m2=wu.convert_Mceta_to_m1m2(mchirpmin,0.25)
      if m1 < compmin: m1 = 1.0
      if m2 < compmin: m2 = 1.0
      Fs,deltaFs=wu.GenerateMultibandFrequencyVector(m1,m2,self.multiband,verbose=1,fudge=self.multiband_fudgefactor)
      np.savetxt(self.nodesfile.get_local(),Fs)
      np.savetxt(self.weightsfile.get_local(),deltaFs)
    else:
      if config.has_option('model','quad-type'):
        self.quad_type=config.getint('model','quad-type')
      else:
        exit("Error: no quad-type specified in config file")
      if config.has_option('model','nodes-file'):
        f = config.get('model','nodes-file')
        self.nodesfile=roqpipe_file(path.dirname(f),"",path.basename(f))
      if config.has_option('model','weights-file'):
        f = config.get('model','weights-file')
        self.weightsfile=roqpipe_file(path.dirname(f),"",path.basename(f))

    if config.has_option('model','x-min'):
      self.xmin=config.getfloat('model','x-min')

    if config.has_option('model','x-max'):
      self.xmax=config.getfloat('model','x-max')

    if config.has_option('model','quad-points'):
      self.quadpoints=config.getint('model','quad-points')

    if config.has_option('model','weighted-inner'):
      self.weighted_inner=config.getboolean('model','weighted-inner')
    else:
      config.set('model','weighted-inner',str(self.weighted_inner))

    if config.has_option('model','load-from-file'):
      self.load_ts_fromfile=config.getboolean('model','load-from-file')

    if config.has_option('model','ts-file'):
      f = config.get('model','ts-file')
      self.tsfile=roqpipe_file(path.dirname(f),"",path.basename(f))

    if self.load_ts_fromfile == True and self.tsfile == None:
      exit("Error: load from trainingset is true but no file is provided")
    if self.load_ts_fromfile == False:
      if config.has_option('model','params-num'):
        p = ast.literal_eval(config.get('model','params-num'))
        self.params_num=[float(x) for x in p]
      else:
        exit("Error: must specify params-num when not reading ts from file")
      if config.has_option('model','params-low'):
        p = ast.literal_eval(config.get('model','params-low'))
        self.params_low=[float(x) for x in p]
      else:
        exit("Error: must specify params-low when not reading ts from file")
      if config.has_option('model','params-high'):
        p = ast.literal_eval(config.get('model','params-high'))
        self.params_high=[float(x) for x in p]
      else:
        exit("Error: must specify params-high when not reading ts from file")


    ####### Parse binary locations #######
    self.greedycpp="./greedyOMP"
    self.eim="./eim"

    if self.site!="nikhefgrid":
      if config.has_option('bins','greedycpp'):
        self.greedycpp=config.get('bins','greedycpp')
      else:
        print "Warning: greedy executable should be provided with \"greedycpp\": now using ./greedyOMP"
      if config.has_option('bins','eim'):
        self.eim=config.get('bins','eim')
      else:
        print "Warning: eim executable should be provided with \"eim\": now using ./eim"


    ####### Parse engine options #######
    self.seed=0
    self.tol=1e-12
    self.max_rb=5000
    self.numthreads=1
    self.memory=0
    self.time=0
    self.binarytar=""
    self.ldlibpath=""
    self.pythonpath=""
    self.export_ts=False
    self.output_dir="./"
    self.output_format="npy"

    if config.has_option('greedycppengine','seed'):
      self.seed=config.getint('greedycppengine','seed')
    else:
      config.set('greedycppengine','seed',str(self.seed))

    if config.has_option('greedycppengine','tol'):
      self.tol=config.getfloat('greedycppengine','tol')
    else:
      config.set('greedycppengine','tol',"%.0e"%self.tol)

    if config.has_option('greedycppengine','max-rb'):
      self.max_rb=config.getint('greedycppengine','max-rb')
    else:
      config.set('greedycppengine','max-rb',str(self.max_rb))

    if config.has_option('greedycppengine','numthreads'):
      self.numthreads=config.getint('greedycppengine','numthreads')
    else:
      config.set('greedycppengine','numthreads',str(self.numthreads))

    if config.has_option('greedycppengine','memory'):
      self.memory=config.getint('greedycppengine','memory')
    if config.has_option('greedycppengine','time'):
      self.time=config.getint('greedycppengine','time')

    if config.has_option('greedycppengine','export-tspace'):
      self.export_ts=config.getboolean('greedycppengine','export-tspace')

    if self.site != "nikhefgrid":
      if config.has_option('greedycppengine','ldlibpath'):
        self.ldlibpath=config.get('greedycppengine','ldlibpath')
      else:
        exit("Error: Must provide ldlibpath if site is not nikhefgrid")
      if config.has_option('greedycppengine','pythonpath'):
        self.pythonpath=config.get('greedycppengine','pythonpath')
      else:
        exit("Error: Must provide pythonpath if site is not nikhefgrid")

    if self.site != "nikhefgrid":
      if config.has_option('greedycppengine','output-dir'):
        self.output_dir=path.join(self.rundir,config.get('greedycppengine','output-dir'))
      else:
        config.set('greedycppengine','output-dir',path.join(self.rundir,self.output_dir))
    else:
      if config.has_option('greedycppengine','output-dir'):
        self.output_dir=config.get('greedycppengine','output-dir')
      else:
        config.set('greedycppengine','output-dir',self.output_dir)
    if config.has_option('greedycppengine','output-data-format'):
      self.output_format=config.get('greedycppengine','output-data-format')
    else:
      config.set('greedycppengine','output-data-format',self.output_format)


    ####### Parse filesystem options #######
    self.sandboxurl="gsiftp://localhost"
    self.lfchome=""
    self.lfchost="lfcserver.cnaf.infn.it"
    self.lfcworkdir=""
    self.srmvirgo="srm://tbn18.nikhef.nl/dpm/nikhef.nl/home/virgo"
    if self.site == "nikhefgrid":
      if config.has_option('filesystem','lfc-home'):
        self.lfchome=config.get('filesystem','lfc-home')
      else:
        exit("Error: Must provide lfc-home directory when site is nikhefgrid")

      if config.has_option('filesystem','lfc-workdir'):
        self.lfcworkdir=config.get('filesystem','lfc-workdir')
        if self.quadmode:
          self.lfcworkdir+="_quad"
          config.set('filesystem','lfc-workdir',self.lfcworkdir)
      else:
        print "Warning: No lfc-workdir provided, workdir is now lfc-home"

      if config.has_option('filesystem','sandboxurl'):
        self.sandboxurl=config.get('filesystem','sandboxurl')
      else:
        config.set('filesystem','sandboxurl',self.sandboxurl)

      if config.has_option('filesystem','lfc-host'):
        self.lfchost=config.get('filesystem','lfc-host')
      else:
        config.set('filesystem','lfc-host',self.lfchost)

      if config.has_option('filesystem','srm-virgo'):
        self.srmvirgo=config.get('filesystem','srm-virgo')
      else:
        config.set('filesystem','srm-virgo',self.srmvirgo)
      if config.has_option('filesystem','binarytar'):
        f = config.get('filesystem','binarytar')
        self.binarytar=roqpipe_file(path.dirname(f),"software",path.basename(f),unpack=True)
      else:
        exit("Error: Must provide binarytar if site is nikhefgrid")

    if self.memory == 0:
      #try to estimate the memory usage automatically
      ts_size=0
      quad_size=0
      mem=0.0
      print "No memory specified, trying to estimate..."
      if self.load_ts_fromfile:
        if path.isfile(self.tsfile.get_local()):
          ts_size=gu.file_len(self.tsfile.get_local())
        else:
          print "  TS file not found"
      else:
        ts_size=np.prod(self.params_num)
      if self.quad_type == 2:
        if path.isfile(self.nodesfile.get_local()):
          quad_size=gu.file_len(self.nodesfile.get_local())
        else:
          print "  nodes file not found"
      else:
        quad_size=self.quadpoints
      if ts_size != 0:
        mem=estimate_memory(ts_size,len(self.parameters),self.max_rb,1,quad_size)
        print "Estimated memory requirement is %.2f GB"%mem
        if mem>8.0:
          mem = np.ceil(mem*1.10)
          print "will set memory request to 1.10*estimate = %.0f GB"%mem
        self.memory=int(mem*1000)
      else:
        print "Could not estimate memory"

    ####### Generate the greedycpp cfg file #######
    self.cfg = greedycpp_cfg()
    if self.site == "nikhefgrid":
      name_outdir='./'
#      name_tsfile=path.basename(self.tsfile)
#      name_weightsfile=self.weightsfile
#      name_nodesfile=self.nodesfile
    else:
      name_outdir=self.output_dir
#      name_tsfile=self.tsfile
#      name_weightsfile=self.weightsfile
#      name_nodesfile=self.nodesfile
    self.cfg.init(quad_type=self.quad_type,model_name=self.modelname,weighted_inner=self.weighted_inner,
                  load_from_file=self.load_ts_fromfile,ts_file=self.tsfile,x_min=self.xmin,x_max=self.xmax,
                  quad_points=self.quadpoints,scales=self.parameterscales,seed=self.seed,tol=self.tol,
                  max_RB=self.max_rb,export_tspace=self.export_ts,output_dir=name_outdir,
                  output_data_format=self.output_format,parameters=self.parameters,params_num=self.params_num,
                  params_low=self.params_low,params_high=self.params_high,
                  quad_nodes_file=self.nodesfile,num_weight_file=self.weightsfile,site=self.site,model_name_quadratic=self.modelquadname,quadraticmode=self.quadmode)
    self.cfgfile=path.join(self.rundir,"greedy.cfg")
    self.cfg.write(self.cfgfile)


    ####### Generate the JDL and bash script #######
    if self.site == "nikhefgrid":
      self.scriptpath = path.join(self.rundir,"greedy.sh")
      self.jdlfile = path.join(self.rundir,"greedy.jdl")

      self.remotefiles=[self.binarytar]
      if config.has_option('filesystem','remotefiles'):
        remotes = ast.literal_eval(config.get('filesystem','remotefiles'))
        for k in remotes.keys():
          flocal=k
          dremote=remotes[k]
          self.remotefiles.append(roqpipe_file(path.dirname(flocal),dremote,path.basename(flocal)))
      if config.has_option('filesystem','remotefile-packages'):
        remotes = ast.literal_eval(config.get('filesystem','remotefile-packages'))
        for k in remotes.keys():
          flocal=k
          dremote=remotes[k]
          self.remotefiles.append(roqpipe_file(path.dirname(flocal),dremote,path.basename(flocal)),unpack=True)

#      self.remotefiles={self.binarytar:True}
#      if config.has_option('filesystem','remotefiles'):
#        self.remotefiles.update(ast.literal_eval(config.get('filesystem','remotefiles')))

      #frequency files will need to be present at the run location
      #files smaller than 2MB can go into the sandbox. Others will need to be uploaded
      self.inputsandbox=[self.scriptpath,self.cfgfile]
      self.tsfile.set_remote(self.lfcworkdir)
      if self.quad_type == 2:
        self.nodesfile.set_remote(self.lfcworkdir)
        self.weightsfile.set_remote(self.lfcworkdir)
        if path.isfile(self.nodesfile.get_local()):
          if path.getsize(self.nodesfile.get_local())/1000.0 > 2.0:
            self.inputsandbox.append(self.nodesfile.get_local())
          else:
            self.remotefiles.append(self.nodesfile)
        else:
          exit("Error: %s does not exist"%(self.nodesfile.get_local()))
        if path.isfile(self.weightsfile.get_local()):
          if path.getsize(self.weightsfile.get_local())/1000.0 > 2.0:
            self.inputsandbox.append(self.weightsfile.get_local())
          else:
            self.remotefiles.append(self.weightsfile)
        else:
          exit("Error: %s does not exist"%(self.weightsfile.get_local()))
      if self.load_ts_fromfile:
        self.remotefiles.append(self.tsfile)


      self.jdl = greedycpp_jdl(self.scriptpath,"greedy.out","greedy.err",inputsandbox=self.inputsandbox,
                               sandboxurl=self.sandboxurl,memrequest=self.memory,timerequest=self.time,cpus=self.numthreads)
      self.jdl.write(self.jdlfile)

      self.job_output_tar = ""
      if config.has_option('filesystem','job-output-tar'):
        self.job_output_tar=config.get('filesystem','job-output-tar')




      self.remoteout = generate_grid_script(self.label,self.lfchome,self.lfcworkdir,outputfilename=self.job_output_tar,remotefiles=self.remotefiles,
                                            cfgfile=path.basename(self.cfgfile),outputtype=self.output_format,
                                            out=self.scriptpath,numthreads=self.numthreads)
      self.remoteout.set_local(self.rundir)



class bash_script(object):
  """Parent class for all bash scripts

  Methods
  -------
  get_filename,get_path,get_dirname
      Access parts of the script path
  set_filepath
      Reset location of the script
  add_line
      Add one line to the script
  add_lines
      Add multiple lines to the script
  write_to_file
      Write out the script contents
  """
  def __init__(self,scriptpath="./script.sh",executable="/bin/bash"):
    """
    Create a script object to add lines to

    Parameters
    ----------
    scriptpath : string
        Full path of the save location of the script
    executable : string
        Path to the parser to use for running the script (e.g. /bin/bash)
    """

    self._path = path.normpath(scriptpath)
    self._dir = path.dirname(self._path)
    self._name = path.basename(self._path)

    self._contents="#!%s\n"%executable


  def get_filename(self):
    """
    Returns
    -------
    name : string
        The basename of the script
    """
    return self._name

  def get_path(self):
    """
    Returns
    -------
    path : string
        The full path of the script
    """
    return self._path

  def get_dirname(self):
    """
    Returns
    -------
    dir : string
        The directory name of the script
    """
    return self._dir

  def set_filepath(self,name):
    """Reset the file path of the script

    Parameters
    ----------
    name : string
        full path
    """
    self._path = path.normpath(name)
    self._dir = path.dirname(self._path)
    self._name = path.basename(self._path)

  def addline(self,line=None):
    """Add a single line to the script

    Parameters
    ----------
    line : string
        Contents of the line without the endline symbol
        If None, will add an empty line
    """
    if line == None:
      self._contents+="\n"
    else:
      self._contents+=line+"\n"

  def addlines(self,lines):
    """Add a multiple lines to the script at once

    Parameters
    ----------
    lines : array_like
        Each element of lines will be added to the script as a single line
    """
    for line in lines:
      self.addline(line)

  def writetofile(self,filename=""):
    """Write the script to file

    Parameters
    ----------
    filename : string
        Full path of the script location
        If not provided, will use internal path as set with __init__
        or set_filepath

    Returns
    -------
    name : string
        Location the script is written to
    """
    if filename!="":
      name = filename
    else:
      name = self._path
    with open(name,"w") as f:
      f.write(self._contents)
    return name


class cat_script(bash_script):

  def __init__(self,scriptname,baserundir,jobdir,resultsdir,nonGRparam,Njobs,mcmin=0.0,mcmax=0.0,seglen=0,executable="/bin/bash"):

    self.jobdir = path.normpath(jobdir)
    self.combined_out = path.join(resultsdir,"bad_points_combined.txt")
    self.newtrainingset = path.join(resultsdir,"enriched_trainingset.txt")

    if seglen != 0:
      self.badpointsname = "bad_points_validationpoints_%s_%ds_${iteration}.txt"%(nonGRparam,seglen)
    else:
      self.badpointsname = "bad_points_validationpoints_%s_Mcmin%.2f_Mcmax%.2f_${iteration}.txt"%(nonGRparam,mcmin,mcmax)

    super(cat_script, self).__init__(scriptpath=path.join(self.jobdir,scriptname),executable=executable)

    self.addline("if [ -f %s ]; then cp %s %s_bckp; rm %s; fi"%(self.combined_out,self.combined_out,self.combined_out,self.combined_out))
    self.addline("touch %s"%self.combined_out)
    self.addline("for iteration in {1..100}; do cat %s >> %s ; done"%(path.join(resultsdir,self.badpointsname),self.combined_out))
    self.addline("Npoints=$(cat %s | wc -l)"%self.combined_out)
    self.addline("echo \"number of bad points after this validation run: ${Npoints}\"")
    self.addline("if (( ${Npoints} > 0 )); then cp %s/GreedyPoints.txt %s ; cat %s >> %s; fi"%(baserundir,self.newtrainingset,self.combined_out,self.newtrainingset))

class binning_script(bash_script):

  def __init__(self,scriptname,jobdir,resultsdir,pointsdir,nonGRparam,binparam,binmin,binmax,column,iter_range,bins_from_data_script="",nbins=60,mcmin=0.0,mcmax=0.0,seglen=0,pythonpath="$PYTHONPATH",executable="/bin/bash"):

    self.jobdir = path.normpath(jobdir)
    self.combined_out = path.join(resultsdir,"bad_points_combined.txt")
    self.newtrainingset = path.join(resultsdir,"enriched_trainingset.txt")

    if seglen != 0:
      if binparam in ["err","eim"]:
        self.datafilename = "validationpoints_%s_%ds_ITER.txt"%(nonGRparam,seglen)
      else:
        self.datafilename = "bad_points_validationpoints_%s_%ds_ITER.txt"%(nonGRparam,seglen)
    else:
      if binparam in ["err","eim"]:
        self.datafilename = "validationpoints_%s_Mcmin%.2f_Mcmax%.2f_ITER.txt"%(nonGRparam,mcmin,mcmax)
      else:
        self.datafilename = "bad_points_validationpoints_%s_Mcmin%.2f_Mcmax%.2f_ITER.txt"%(nonGRparam,mcmin,mcmax)

    super(binning_script, self).__init__(scriptpath=path.join(self.jobdir,scriptname),executable=executable)

    if bins_from_data_script == "":
      print "Warning: No script location provided: roq_generate_validationfile.py may not be found."
      self.binscript = "bins_from_data.py"
    else:
      self.binscript = bins_from_data_script

    self.datafile = path.join(resultsdir,self.datafilename)
    self.outputfile = path.join(resultsdir,"bins_%s.txt"%binparam)
    command  = "time %s --in %s --out %s"%(self.binscript,self.datafile,self.outputfile)
    command += " --combine %d,%d --n-bins %d"%(iter_range[0],iter_range[1],nbins)
    command += " --bin-min %.2f --bin-max %.2f"%(float(binmin),float(binmax))
    command += " --column %d"%column
    if binparam in ["err","eim"]:
      command += " --log --square"
    if binparam == "eta":
      command += " --eta"
    if binparam == "mchirp":
      command += " --mchirp"


    self.addline("export PYTHONPATH=%s"%pythonpath)
    self.addline(command)


class enrichment_sweep_script(bash_script):

  def __init__(self,scriptname,jobsdir,trainingset,baserundir,numvalidation,executable="/bin/bash",
               ldlibpath="$LD_LIBRARY_PATH",greedy="",eim="",quadweights="",quadpoints="",quadratic=False):

    self.jobsdir=path.normpath(jobsdir)
    if quadratic:
      self.outdir=path.join(jobsdir,"..","..","quadrun%d"%numvalidation)
    else:
      self.outdir=path.join(jobsdir,"..","..","run%d"%numvalidation)


    self.prev_cfg=path.join(baserundir,"run_settings.cfg")
    self.masterini=path.join(baserundir,"masterconfig.ini")
    self.new_cfg=path.join(baserundir,"run_settings_followup.cfg")

    cfg = greedycpp_cfg()
    cfg.read(self.prev_cfg)
    self.greedyfiletype=cfg.get("output_data_format")
    self.quad_type=int(cfg.get("quad_type"))

    # check paths of quad and weight files if quad_type==2
    if self.quad_type==2:
      ### weights
      if quadweights == "":
        self.quadweights=cfg.get("num_weight_file")
        if not path.isfile(self.quadweights):
          globbed = glob(path.join(baserundir,path.basename(self.quadweights)))
          if len(globbed)>0:
            cfg.add_option("num_weight_file",globbed[0])
          else:
            exit("Error: Cannot find num_weight_file automatically")
      else:
        if path.isfile(quadweights):
          cfg.add_option("num_weight_file",quadweights)
        else:
          exit("Error: manually provided num_weight_file not found")
      ### nodes
      if quadpoints == "":
        self.quadpoints=cfg.get("quad_nodes_file")
        if not path.isfile(self.quadpoints):
          globbed = glob(path.join(baserundir,path.basename(self.quadpoints)))
          if len(globbed)>0:
            cfg.add_option("quad_nodes_file",globbed[0])
          else:
            exit("Error: Cannot find quad_nodes_file automatically")
      else:
        if path.isfile(quadpoints):
          cfg.add_option("quad_nodes_file",quadpoints)
        else:
          exit("Error: manually provided quad_nodes_file not found")
      print "Overwriting validations_setup.cfg in baserun directory to point to quad files"
      cfg.write( path.join(baserundir,"validations_setup.cfg") )

    #change the ts_file for the new_cfg
    cfg.add_option("ts_file",trainingset)
    #The output dir is not used by verifyOMP, but for completeness it is added to the new cfg:
    cfg.add_option("output_dir",self.outdir+"/")
    cfg.write(self.new_cfg)

    #initialise the parent class bash_script
    super(enrichment_sweep_script, self).__init__(scriptpath=path.join(self.jobsdir,scriptname),executable=executable)

    self.addline("export LD_LIBRARY_PATH=%s"%ldlibpath)
    self.addline("export OMP_NUM_THREADS=1")

    if greedy == "":
      print "Warning: No greedyOMP location provided: greedyOMP may not be found."
      self.greedy = "greedyOMP"
    else:
      self.greedy = greedy
    if eim == "":
      print "Warning: No eim location provided: eim may not be found."
      self.eim = "eim"
    else:
      self.eim = eim

    self.addline("time %s %s"%(self.greedy,self.new_cfg))
    self.addline("time %s %s/run_settings.cfg %s/ %s"%(self.eim,self.outdir,self.outdir,self.greedyfiletype))


class validation_script(bash_script):

  def __init__(self,jobsdir,resultsdir,pointsdir,catscript,followupscript,statefile,numvalidation,baserundir,nonGRparam,iteration,modelname,mcmin=0.0,mcmax=0.0,pointsperfile=100000,nfiles=100,kerrbound=0.98,
               nongrmin=None,nongrmax=None,Nlastfile=100,seglen=0,executable="/bin/bash",
               ldlibpath="$LD_LIBRARY_PATH",pythonpath="$PYTHONPATH",
               generatepoints_script="",verifyOMP="",greedyfiletype="npy",generate_points=True):

    self.submitfilename = "submit_validationstudy_%d.sh"%iteration
    if seglen != 0:
      self.outputfilename = "validationpoints_%s_%ds_%d.txt"%(nonGRparam,seglen,iteration)
    else:
      self.outputfilename = "validationpoints_%s_Mcmin%.2f_Mcmax%.2f_%d.txt"%(nonGRparam,mcmin,mcmax,iteration)

    self.jobdir = path.normpath(jobsdir)
    self.resultsdir = path.normpath(resultsdir)
    self.pointsdir = path.normpath(pointsdir)
    self.validationdirname = path.basename(path.dirname(jobsdir))
    self.modelname = modelname

    #initialise the parent class bash_script
    super(validation_script, self).__init__(scriptpath=path.join(self.jobdir,self.submitfilename),executable=executable)


    self.addline()
    self.addline("export LD_LIBRARY_PATH=%s"%ldlibpath)
    self.addline("export PYTHONPATH=%s"%pythonpath)
    if generatepoints_script == "":
      print "Warning: No script location provided: roq_generate_validationfile.py may not be found."
      self.genpointscript = "roq_generate_validationfile.py"
    else:
      self.genpointscript = generatepoints_script
    if verifyOMP == "":
      print "Warning: No verifyOMP location provided: verifyOMP may not be found."
      self.verifyOMP = "verifyOMP"
    else:
      self.verifyOMP = verifyOMP

    if generate_points:
      genpointscommand = "time %s"%self.genpointscript
      genpointscommand += " --model %s"%self.modelname
      genpointscommand += " --out %s"%self.pointsdir
      genpointscommand += " --filename-out %s"%self.outputfilename
      genpointscommand += " --n-points %d"%pointsperfile
      genpointscommand += " --n-files 1"
      genpointscommand += " --file-index %d"%iteration
      genpointscommand += " --nonGRparam %s"%nonGRparam
      if mcmin != 0.0:
        genpointscommand += " --mc-min %s"%mcmin
      if mcmax != 0.0:
        genpointscommand += " --mc-max %s"%mcmax
      if kerrbound != 0.0:
        genpointscommand += " --kerr-bound %s"%kerrbound
      if nongrmin != None:
        genpointscommand += " --nonGRparam-min %s"%nongrmin
      if nongrmax != None:
        genpointscommand += " --nonGRparam-max %s"%nongrmax

      self.addline(genpointscommand)
    self.addline("export OMP_NUM_THREADS=1")

    verifycommand = "time %s"%self.verifyOMP
    verifycommand += " %s/validations_setup.cfg"%baserundir
    verifycommand += " %s/ %s"%(baserundir,greedyfiletype)
    verifycommand += " %s"%path.join(self.pointsdir,self.outputfilename)
    #must be relative to the baserundir
    verifycommand += " %s"%path.join("..",self.validationdirname,"results")

    self.addline(verifycommand)

    #write a line to the statefile after validation job is finished
    self.addline("if [ -f  %s ]; then"%( path.join(resultsdir,"bad_points_%s"%self.outputfilename) ) )
    self.addline("  echo \"validation %d finished\" >> %s"%(iteration,statefile))
    self.addline("fi")

    self.addline()
    self.addline("# execute only if this is the last job")
    self.addline("Ndone=$(cat %s | wc -l)"%statefile)
    self.addline("if (( ${Ndone} == %d )); then"%nfiles)
    self.addline("  %s"%catscript.get_path() )
    self.addline("  # execute only if there were bad points")
    self.addline("  Nbadpoints=$(cat %s | wc -l)"%catscript.newtrainingset)
    self.addline("  if (( ${Nbadpoints} > 0 )); then")
    self.addline("    mkdir -p %s"%followupscript.outdir)
    self.addline("    cp %s %s"%(followupscript.masterini,followupscript.outdir) )
    self.addline("    cp %s %s"%(followupscript.new_cfg,path.join(followupscript.outdir,"run.cfg")) )
    self.addline("    %s"%followupscript.get_path() )
    self.addline("  fi")
    self.addline("fi")


