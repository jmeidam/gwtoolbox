__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

from numpy import pi
from gwtoolbox.waveforms import utils as wfutils

class roq_parameter():
  def __init__(self,name,pmin,pmax,index,latexname):
    self.name = name
    self.min = pmin
    self.max = pmax
    self.index = index
    self.latex = latexname

class roq_model():
  def __init__(self,name,seglen,qmin,qmax,mchirpmin,mchirpmax,dim):
    self.name = name
    self.seglen = seglen
    self.qmin = qmin
    self.qmax = qmax
    self.mchirpmin = mchirpmin
    self.mchirpmax = mchirpmax
    self._parameters = {}
    self.dim = dim

  def add_parameter(self,name,pmin,pmax,index,latexname):
    self._parameters[name] = roq_parameter(name,pmin,pmax,index,latexname)

  def get_parameter(self,name):
    return self._parameters[name]


def roq_get_modeldefaults(modelname,seglen):
  if "PhenomP" in modelname:
    mchirp_ranges={'4s':(12.3,23.0),
                   '8s':(7.9,14.8),
                   '16s':(5.2,9.5),
                   '32s':(3.4,6.2),
                   '64s':(2.2,4.2),
                   '128s':(1.4,2.6)}
    qmin=1.0
    qmax=9.0
    etamax=0.25
    etamin=wfutils.convert_q_to_eta(qmax)

    dim = 25

    model = roq_model(modelname,seglen,qmin,qmax,mchirp_ranges["%ds"%seglen][0],mchirp_ranges["%ds"%seglen][1],dim)

    model.add_parameter("mchirp",mchirp_ranges["%ds"%seglen][0],mchirp_ranges["%ds"%seglen][1],0,r"$\mathcal{M}_c$")
    model.add_parameter("q",qmin,qmax,0,r"$q$")
    model.add_parameter("eta",etamin,etamax,0,r"$\eta$")

    m_max,m = wfutils.convert_Mcq_to_m1m2(mchirp_ranges["%ds"%seglen][1],qmax)
    m,m_min = wfutils.convert_Mcq_to_m1m2(mchirp_ranges["%ds"%seglen][0],qmax)
    if m_min < 1.0:
      m_min = 1.0
    model.add_parameter("mass1",m_min,m_max,0,r"$m_1$")
    model.add_parameter("mass2",m_min,m_max,1,r"$m_2$")
    model.add_parameter("chi1L",-0.9,0.9,2,r"$\chi_{1L}$")
    model.add_parameter("chi2L",-0.9,0.9,3,r"$\chi_{2L}$")
    model.add_parameter("chip",0.0,0.9,4,r"$\chi_p$")
    model.add_parameter("thetaJ",0.0,pi,5,r"$\theta_J$")
    model.add_parameter("alpha0",0.0,2.0*pi,6,r"$\alpha_0$")
    #Note: column 7 is not used

    model.add_parameter("dchi0",-0.5,0.5,8,r"$\delta\hat{\varphi}_0$")
    model.add_parameter("dchi1",-1.0,1.0,9,r"$\delta\hat{\varphi}_1$")
    model.add_parameter("dchi2",-1.0,1.0,10,r"$\delta\hat{\varphi}_2$")
    model.add_parameter("dchi3",-0.5,0.5,11,r"$\delta\hat{\varphi}_3$")
    model.add_parameter("dchi4",-4.0,4.0,12,r"$\delta\hat{\varphi}_4$")
    model.add_parameter("dchi5l",-2.0,2.0,13,r"$\delta\hat{\varphi}_5^{(l)}$")
    model.add_parameter("dchi6",-4.0,4.0,14,r"$\delta\hat{\varphi}_6$")
    model.add_parameter("dchi6l",-10.0,10.0,15,r"$\delta\hat{\varphi}_6^{(l)}$")
    model.add_parameter("dchi7",-10.0,10.0,16,r"$\delta\hat{\varphi}_7$")

    model.add_parameter("dalpha2",-5.0,5.0,17,r"$\delta\hat{\alpha}_2$")
    model.add_parameter("dalpha3",-5.0,5.0,18,r"$\delta\hat{\alpha}_3$")
    model.add_parameter("dalpha4",-5.0,5.0,19,r"$\delta\hat{\alpha}_4$")

    model.add_parameter("dbeta2",-2.0,2.0,20,r"$\delta\hat{\beta}_2$")
    model.add_parameter("dbeta3",-2.0,2.0,21,r"$\delta\hat{\beta}_3$")

    model.add_parameter("dsigma2",-20.0,20.0,22,r"$\delta\hat{\sigma}_2$")
    model.add_parameter("dsigma3",-20.0,20.0,23,r"$\delta\hat{\sigma}_3$")
    model.add_parameter("dsigma4",-20.0,20.0,24,r"$\delta\hat{\sigma}_4$")

    #two error columns that are added to the validation files
    model.add_parameter("eim",-14,0,25,r"$\log_{10}\,\,{\rm Interpolation\,\,Error}$")
    model.add_parameter("err",-14,0,26,r"$\log_{10}\,\,{\rm Greedy\,\,Error}$")

    return model

  elif "TaylorF2" in modelname:

    seglen=32.

    mcompmin=1.0
    mcompmax=2.5
    etamin=wfutils.convert_m1m2_to_nu(mcompmax,mcompmin)
    etamax=0.25
    qmin=1.0
    qmax=mcompmax/mcompmin
    mchirpmin=wfutils.convert_m1m2_to_Mc(mcompmin,mcompmin)
    mchirpmax=wfutils.convert_m1m2_to_Mc(mcompmax,mcompmax)

    dim = 13
    model = roq_model(modelname,seglen,qmin,qmax,mchirpmin,mchirpmax,dim)

    model.add_parameter("mchirp",mchirpmin,mchirpmax,0,r"$\mathcal{M}_c$")
    model.add_parameter("q",qmin,qmax,0,r"$q$")
    model.add_parameter("eta",etamin,etamax,0,r"$\eta$")

    model.add_parameter("mass1",mcompmin,mcompmax,0,r"$m_1$")
    model.add_parameter("mass2",mcompmin,mcompmax,1,r"$m_2$")
    model.add_parameter("spin1z",-0.5,0.5,2,r"$S_{1z}$")
    model.add_parameter("spin2z",-0.5,0.5,3,r"$S_{2z}$")

    model.add_parameter("dchi0",-1.5,1.5,4,r"$\delta\hat{\varphi}_0$")
    model.add_parameter("dchi1",-1.5,1.5,5,r"$\delta\hat{\varphi}_1$")
    model.add_parameter("dchi2",-1.5,1.5,6,r"$\delta\hat{\varphi}_2$")
    model.add_parameter("dchi3",-1.5,1.5,7,r"$\delta\hat{\varphi}_3$")
    model.add_parameter("dchi4",-1.5,1.5,8,r"$\delta\hat{\varphi}_4$")
    model.add_parameter("dchi5l",-1.5,1.5,9,r"$\delta\hat{\varphi}_5^{(l)}$")
    model.add_parameter("dchi6",-1.5,1.5,10,r"$\delta\hat{\varphi}_6$")
    model.add_parameter("dchi6l",-1.5,1.5,11,r"$\delta\hat{\varphi}_6^{(l)}$")
    model.add_parameter("dchi7",-1.5,1.5,12,r"$\delta\hat{\varphi}_7$")

    #two error columns that are added to the validation files
    model.add_parameter("eim",-14,0,13,r"$\log_{10}\,\,{\rm Interpolation\,\,Error}$")
    model.add_parameter("err",-14,0,14,r"$\log_{10}\,\,{\rm Greedy\,\,Error}$")

    return model

  else:
    print "Error: Only know PhenomP and TaylorF2 model defaults"
    return None


