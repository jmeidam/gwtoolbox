__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

from os import path

from gwtoolbox.visualization.markup_py import oneliner as onel
from gwtoolbox.visualization.markup_py import page

def write_defaultstylesheet(outpath):
  style="""
  p,h1,h2,h3,h4,h5
  {
  font-family:&quot;Trebuchet MS&quot;, Arial, Helvetica, sans-serif;
  }

  p
  {
  font-size:14px;
  }

  h1
  {
  font-size:20px;
  }

  h2
  {
  font-size:18px;
  }

  h3
  {
  font-size:16px;
  }



  table
  {
  font-family:&quot;Trebuchet MS&quot;, Arial, Helvetica, sans-serif;
  width:100%;
  border-collapse:collapse;
  }
  td,th
  {
  font-size:12px;
  border:1px solid #B5C1CF;
  padding:3px 7px 2px 7px;
  }
  th
  {
  font-size:14px;
  text-align:left;
  padding-top:5px;
  padding-bottom:4px;
  background-color:#B3CEEF;
  color:#ffffff;
  }
  #postable tr:hover
  {
  background: #DFF4FF;
  }
  #covtable tr:hover
  {
  background: #DFF4FF;
  }
  #statstable tr:hover
  {
  background: #DFF4FF;
  }

  img {
      max-width: 1400px;
      max-height: 1400px;
      width:100%;
      eight:100%;
  }

  .ppsection
  {
  border-bottom-style:double;
  }
  """
  with open(outpath,'w') as f:
    f.write(style)


def write_collapse_script(outpath):
  script="""
  //&lt;![CDATA[
  function toggle_visibility(tbid,lnkid)
  {

    if(document.all){document.getElementById(tbid).style.display = document.getElementById(tbid).style.display == 'block' ? 'none' : 'block';}

    else{document.getElementById(tbid).style.display = document.getElementById(tbid).style.display == 'table' ? 'none' : 'table';}

    document.getElementById(lnkid).value = document.getElementById(lnkid).value == '[-] Collapse' ? '[+] Expand' : '[-] Collapse';

   }
   //]]&gt;
  """
  with open(outpath,'w') as f:
    f.write(script)


#collapse_script="""
#//&lt;![CDATA[
#function toggle_visibility(tbid,lnkid)
#{
#
#  if(document.all){document.getElementById(tbid).style.display = document.getElementById(tbid).style.display == 'block' ? 'none' : 'block';}
#
#  else{document.getElementById(tbid).style.display = document.getElementById(tbid).style.display == 'table' ? 'none' : 'table';}
#
#  document.getElementById(lnkid).value = document.getElementById(lnkid).value == '[-] Collapse' ? '[+] Expand' : '[-] Collapse';
#
# }
# //]]&gt;
#"""

class gwhtmlpage(page):

  def __init__(self,outpath,title="title",stylesheet="",scripts=["https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"]):

    #initialize the page parent class
    super(gwhtmlpage, self).__init__()

    self.outpath = outpath
    if stylesheet == "":
      fname = path.join(path.dirname(self.outpath),"gwstyle.cls")
      write_defaultstylesheet(fname)
      self.stylesheet = fname
    else:
      self.stylesheet = stylesheet

    # Add the collapse script
    coll_script = path.join(path.dirname(self.outpath),"collapse.js")
    write_collapse_script(coll_script)
    scripts.append(path.basename(coll_script))

    #call the markup_py init function
    self.init(title=title, css=( self.stylesheet ), script=scripts)


  def add_defaultdatatable(self,tablename,table_id,data,width="100%",numberformat="%.4f"):
    """
    first row in data must be the header
    the following rows are the data, and values may be strings or floats
    """
    self.table(table_border="1", id=table_id, style="display: table",width=width)

    #table name
    self.tr(bgcolor="#4682B4", height="40")
    fontobj=onel.font(onel.strong(tablename),size="3",color="white",face="tahoma")
    self.td(fontobj, colspan="%d"%len(data[0]))
    self.tr.close()

    #header
    self.tr()
    for v in data[0]:
      self.th(str(v))
    self.tr.close()

    #data
    for i in range(1,len(data)):
      self.tr()
      row = data[i]
      for v in row:
        if type(v) == type('a'):
          self.td(v)
        else:
          if int(v) == v:
            self.td("%d"%v)
          else:
            self.td(numberformat%v)
      self.tr.close()

    self.table.close()


def write_collapsible_sectionheader(page,title,tableid,linkid,bgcolor="#4682B4",textsize=8):
  """Write the header of a collabsible table
  This should go after a table opening statement.

  tableid: string
      ID of the table to collapse
  linkid: string
      Unique identifier (should not occur anywhere else on the page)

  """
  page.tr(bgcolor=bgcolor,height="50")

  page.td(width="5%")
  page.span(style="color:#ffffff;face:tahoma;size:%d"%textsize)
  page.a("Top",href="#")
  page.span.close()
  page.td.close()

  page.td(width="45%")
  page.span(style="color:#ffffff;face:tahoma;size:%d"%textsize)
  page.strong(title)
  page.span.close()
  page.td.close()

  page.td(align="center", bgcolor=bgcolor, width="50%")
  page.input(id=linkid, onclick="toggle_visibility('%s','%s');"%(tableid,linkid), type_="button", value="[+] Expand")
  page.td.close()

  page.tr.close()



