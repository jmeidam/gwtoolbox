__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

from os import environ,path
import matplotlib as mpl

# Use the Agg backend if a display is not available
try:
  environ["DISPLAY"]
  _DisplayAvailable=True
except:
  _DisplayAvailable=False

if not _DisplayAvailable:
  mpl.use('Agg')

import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
from matplotlib.ticker import FixedLocator
from matplotlib.colors import LinearSegmentedColormap,LogNorm,Normalize
from matplotlib import rc
import matplotlib.cm as cm
import numpy as np
from uuid import uuid1
plt.ioff()

#from seaborn import set_style,set_context,set_palette
#
#set_style('ticks')  # Plot layout, axis ticks, etc
#set_context('paper') # Thin lines (as opposed to 'presentation', with thick)
#set_palette('colorblind') # Matplotlib-like, but better adapted to colourblind people

_fig_width_pt = 3*246.0  # Get this from LaTeX using \showthe\columnwidth
_inches_per_pt = 1.0/72.27               # Convert pt to inch
_golden_mean = (2.2360679774997898-1.0)/2.0         # Aesthetic ratio
_fig_width = _fig_width_pt*_inches_per_pt  # width in inches
_fig_height = _fig_width*_golden_mean      # height in inches
_fig_size =  [_fig_width,_fig_height]

_fig_dpi = 100
_fig_save_dpi = 300

_def_markersize = 30

def calculate_fig_size(ratio):
  """ratio = height/width
  returns [width,height]"""
  height = _fig_width*ratio
  return [_fig_width,height]


TableauColors_Tableau20 = [(0.12156862745098039, 0.4666666666666667, 0.7058823529411765),
                           (0.6823529411764706, 0.7803921568627451, 0.9098039215686274),
                           (1.0, 0.4980392156862745, 0.054901960784313725),
                           (1.0, 0.7333333333333333, 0.47058823529411764),
                           (0.17254901960784313, 0.6274509803921569, 0.17254901960784313),
                           (0.596078431372549, 0.8745098039215686, 0.5411764705882353),
                           (0.8392156862745098, 0.15294117647058825, 0.1568627450980392),
                           (1.0, 0.596078431372549, 0.5882352941176471),
                           (0.5803921568627451, 0.403921568627451, 0.7411764705882353),
                           (0.7725490196078432, 0.6901960784313725, 0.8352941176470589),
                           (0.5490196078431373, 0.33725490196078434, 0.29411764705882354),
                           (0.7686274509803922, 0.611764705882353, 0.5803921568627451),
                           (0.8901960784313725, 0.4666666666666667, 0.7607843137254902),
                           (0.9686274509803922, 0.7137254901960784, 0.8235294117647058),
                           (0.4980392156862745, 0.4980392156862745, 0.4980392156862745),
                           (0.7803921568627451, 0.7803921568627451, 0.7803921568627451),
                           (0.7372549019607844, 0.7411764705882353, 0.13333333333333333),
                           (0.8588235294117647, 0.8588235294117647, 0.5529411764705883),
                           (0.09019607843137255, 0.7450980392156863, 0.8117647058823529),
                           (0.6196078431372549, 0.8549019607843137, 0.8980392156862745)]

TableauColors_ColorBlind10 = [(0.0, 0.4196078431372549, 0.6431372549019608),
                              (1.0, 0.5019607843137255, 0.054901960784313725),
                              (0.6705882352941176, 0.6705882352941176, 0.6705882352941176),
                              (0.34901960784313724, 0.34901960784313724, 0.34901960784313724),
                              (0.37254901960784315, 0.6196078431372549, 0.8196078431372549),
                              (0.7843137254901961, 0.3215686274509804, 0.0),
                              (0.5372549019607843, 0.5372549019607843, 0.5372549019607843),
                              (0.6352941176470588, 0.7843137254901961, 0.9254901960784314),
                              (1.0, 0.7372549019607844, 0.4745098039215686),
                              (0.8117647058823529, 0.8117647058823529, 0.8117647058823529)]

rcparameters = {'legend.fontsize': 20,#16
                'legend.handlelength':2,
                'legend.numpoints':2,
                'font.family':'serif',
                'axes.labelsize' : 24,
                'font.size' : 24,
                'xtick.labelsize' : 18,
                'ytick.labelsize' : 18,
                'axes.titlesize' : 18,
                'grid.alpha' : 0.5,
                'grid.color' : 'k',
                'grid.linestyle' : ':',
                'grid.linewidth' : 0.5,
                'figure.dpi' : _fig_dpi,
                'savefig.dpi' : _fig_save_dpi,
                'figure.figsize': _fig_size,
                'patch.facecolor' : TableauColors_ColorBlind10[4],
                'lines.markersize' : _def_markersize,
                'lines.linewidth' : 1.5,
                'axes.color_cycle' : TableauColors_Tableau20}

rc('text.latex', preamble = '\usepackage{txfonts}')

plt.rcParams.update(rcparameters)

def base_and_exp(x):

  exp = int(np.log10(abs(x)))
  base = (x/10**np.exp)
  if abs(base) >= 10.0:
    return base/10.0,exp+1
  elif abs(base) < 1.0:
    return base*10.0,exp-1
  else:
    return base,exp

#http://stackoverflow.com/questions/16834861/create-own-colormap-using-matplotlib-and-plot-color-scale
def make_colormap(seq):
    """Return a LinearSegmentedColormap
    seq: a sequence of floats and RGB-tuples. The floats should be increasing
    and in the interval (0,1).
    """
    seq = [(None,) * 3, 0.0] + list(seq) + [1.0, (None,) * 3]
    cdict = {'red': [], 'green': [], 'blue': []}
    for i, item in enumerate(seq):
        if isinstance(item, float):
            r1, g1, b1 = seq[i - 1]
            r2, g2, b2 = seq[i + 1]
            cdict['red'].append([item, r1, r2])
            cdict['green'].append([item, g1, g2])
            cdict['blue'].append([item, b1, b2])
    return LinearSegmentedColormap('CustomMap', cdict)

custom_colormap = make_colormap([(1.,1.,1.),TableauColors_Tableau20[2],TableauColors_Tableau20[0]])
custom_colormap_density_scatter = make_colormap([TableauColors_Tableau20[18],TableauColors_Tableau20[6]])

def rgb_to_abs_value(rgb):
  a = np.array(rgb)
  return np.sqrt(a.dot(a))

class custom_figure():
  def __init__(self,name=None,size=_fig_size,nxn=[1,1],dpi=_fig_dpi):
    if name == None:
      name = uuid1()
    self.fig = plt.figure(num=name,figsize=size,dpi=dpi)
    self.axes=[]
    self.nxn=nxn
    self.name=name
    self.colorbar=None
    self.leg=None

    self.is_residualplot = False

    if nxn[0] < 1 or nxn[1] < 1:
      print "Error in custom_figure: nxn must be at least 1x1"

  def redraw_fig(self):
    if not self.is_residualplot: self.fig.tight_layout()
    self.fig.canvas.draw()

  def close_fig(self):
    plt.close(self.fig)

  def show_fig(self):
    if not _DisplayAvailable:
      print "Warning: No display detected, must use Agg backend, can't show figure"
    else:
      if not self.is_residualplot: self.fig.tight_layout()
      self.fig.show()

  def save_fig(self,filename=None,dpi=_fig_save_dpi,close=True,tightlayout=True):
    if filename == None:
      filename = self.name
    if not self.is_residualplot and tightlayout==True: self.fig.tight_layout()
    if not _DisplayAvailable:
      if path.splitext(filename)[1] != ".png":
        print "Warning: No display detected, must use Agg backend, saving as png"
        filename = path.splitext(filename)[0]+".png"
    self.fig.savefig(filename,dpi=dpi)
    if close: self.close_fig()

  def set_xlabel(self,label,ax_index=1):
    ax = self.get_ax(ax_index)
    if ax:
      ax.set_xlabel(label)
    else:
      print "Error: Could not set xlabel"

  def set_ylabel(self,label,ax_index=1):
    ax = self.get_ax(ax_index)
    if ax:
      ax.set_ylabel(label)
    else:
      print "Error: Could not set ylabel"

  def set_xlimit(self,ax_index=1,xmin=None,xmax=None):
    """
    Adjust the x-axis limits by either providing xmax and/or xmin
    """
    if xmin==None and xmax==None:
      print "Warning in set_xlimit: No values given, nothing done."
      return None
    ax = self.get_ax(ax_index)
    if ax:
      [left,right]=ax.get_xlim()
      if xmin!=None:
        left=xmin
      if xmax!=None:
        right=xmax
      ax.set_xlim([left,right])
    else:
      print "Error: could not adjust xlimit"

  def set_ylimit(self,ax_index=1,ymin=None,ymax=None):
    """
    Adjust the y-axis limits by either providing ymax and/or ymin
    """
    if ymin==None and ymax==None:
      print "Warning in set_ylimit: No values given, nothing done."
      return None
    ax = self.get_ax(ax_index)
    if ax:
      [left,right]=ax.get_ylim()
      if ymin!=None:
        left=ymin
      if ymax!=None:
        right=ymax
      ax.set_ylim([left,right])
    else:
      print "Error: could not adjust xlimit"

  def add_ax(self,ax_index=1):
    if ax_index < 1:
      print "Error in add_ax: n needs to be >=1"
      return None
    if ax_index > self.nxn[0]*self.nxn[1]:
      print "Error in add_ax: n needs to be <= n*n (%d)"%(self.nxn[0]*self.nxn[1])
      return None
    ax = self.fig.add_subplot(self.nxn[0],self.nxn[1],ax_index)
    self.axes.append(ax)

  def get_ax(self,ax_index=1):
    """
    Returns an ax object if the index exists, otherwise None.
    """
    if len(self.axes) == 0:
      return None
    if ax_index <= len(self.axes) and ax_index > 0:
      return self.axes[ax_index-1]
    else:
      #print "Error in get_ax: index %d out of bounds (allowed: 1-%d)"%(ax_index,len(self.axes))
      return None

  def setup_residualplot(self,residual_fraction=0.3,left=0.1,width=0.8,bottom=0.1,height=0.8):
    if self.nxn[0]*self.nxn[1]>1:
      print "Error in setup_residualplot: n x n cannot be >1 for this type of plot"
      return

    self.is_residualplot = True
    #reset axes to be sure there are none yet
    self.axes=[]

    #add_axes([left, bottom, width, height])
    #[units are fraction of the image frame, from bottom left corner]
    height2 = residual_fraction*height
    ax1 = self.fig.add_axes([left,bottom+height2,width,height-height2])
    ax2 = self.fig.add_axes([left,bottom,width,height2])
    self.axes.append(ax1)
    self.axes.append(ax2)
    self.axes[0].set_xticklabels([])

  def set_yticks(self,values,ax_index=1):

    ax = self.get_ax(ax_index)
    if ax:
      ax.yaxis.set_major_locator( FixedLocator(values) )
    else:
      print "Error in set_yticks: ax index not found"

  def set_xticks(self,values,ax_index=1):

    ax = self.get_ax(ax_index)
    if ax:
      ax.xaxis.set_major_locator( FixedLocator(values) )
    else:
      print "Error in set_xticks: ax index not found"

  def add_hline(self,y,ax_index=1,xmin=0,xmax=1,style='--',color='k',alpha=1.0,label=None,extra={}):
    ax = self.get_ax(ax_index)
    if ax:
      ax.axhline(y,xmin=xmin,xmax=xmax,color=color,alpha=alpha,label=label,ls=style,**extra)
    else:
      print "Error in add_hline: ax index not found"

  def add_vline(self,x,ax_index=1,ymin=0,ymax=1,style='--',color='k',alpha=1.0,label=None,extra={}):
    ax = self.get_ax(ax_index)
    if ax:
      ax.axvline(x,ymin=ymin,ymax=ymax,color=color,alpha=alpha,label=label,ls=style,**extra)
    else:
      print "Error in add_hline: ax index not found"

  def set_xticklabel_sci(self,ax_index=1):
    ax = self.get_ax(ax_index)
    if ax:
      self.fig.canvas.draw()
      nums = [item for item in ax.get_xticks()]
      labelsnew = []
      for n in nums:
        if n!=0.0:
          base,exp = base_and_exp(n)
          if exp != 0:
            labelsnew.append(r"%.1f$e^{%d}$"%(base,exp))
          else:
            labelsnew.append(r"%.1f"%base)
        else:
          labelsnew.append("0.0")
      ax.set_xticklabels(labelsnew)
    else:
      print "Error in set_xticklabel_style: ax index not found"

  def set_yticklabel_sci(self,ax_index=1):
    ax = self.get_ax(ax_index)
    if ax:
      self.fig.canvas.draw()
      nums = [item for item in ax.get_yticks()]
      labelsnew = []
      for n in nums:
        if n!=0.0:
          base,exp = base_and_exp(n)
          if exp != 0:
            labelsnew.append(r"%.1f$e^{%d}$"%(base,exp))
          else:
            labelsnew.append(r"%.1f"%base)
        else:
          labelsnew.append("0.0")
      ax.set_yticklabels(labelsnew)
    else:
      print "Error in set_yticklabel_style: ax index not found"

  def del_ytick(self,tick,ax_index=1):
    ax = self.get_ax(ax_index)
    if tick != 'first' and tick != 'last':
      print "Error in del_ytick: tcik must be 'first' or 'last'"
      return None
    if ax:
      ylim = ax.get_ylim()
      yticks = ax.get_yticks()
      if tick == 'first':
        ax.set_yticks(yticks[1:])
      else:
        ax.set_yticks(yticks[:-1])
      ax.set_ylim(ylim)
    else:
      print "Error in del_ytick: ax index not found"
      return None

  def get_lines(self,ax_index=1):

    ax = self.get_ax(ax_index)
    if ax:
      return ax.get_lines()
    else:
      print "Error in get_lines: ax index not found"
      return None

  def add_line(self,line,ax_index=1):

    ax = self.get_ax(ax_index)
    if ax:
      ax.add_line(line)
    else:
      print "Error in add_line: ax index not found"

  def add_title(self,title,ax_index=1):

    ax = self.get_ax(ax_index)
    if ax:
      ax.set_title(title)
    else:
      print "Error in add_title: ax index not found"

  def add_colorbar(self,vmin,vmax,label="",labelpad=1,cmap='gnuplot2',ax_index=1):
    sm = cm.ScalarMappable(cmap=cmap, norm=Normalize(vmin=vmin, vmax=vmax))
    sm._A = []
    ax = self.get_ax(ax_index=ax_index)
    if ax:
      self.colorbar = self.fig.colorbar(sm, ax=ax)
      if label != "":
        self.colorbar.set_label(label, labelpad=labelpad)
    else:
      print "Error in add_colorbar: ax index not found"

  def add_xyplot(self,x,y,ax_index=1,xlabel="",ylabel="",label=None,logy=False,logx=False,style='-',color=None,alpha=1.0,grid=True,redraw=False,extra={}):
    ax = self.get_ax(ax_index=ax_index)
    if ax==None:
      self.add_ax(ax_index=ax_index)
      ax = self.get_ax(ax_index=ax_index)

    if xlabel != "":
      ax.set_xlabel(xlabel)
    if ylabel != "":
      ax.set_ylabel(ylabel)

    if color:
      ax.plot(x, y, label=label, ls=style,c=color,alpha=alpha,**extra)
    else:
      ax.plot(x, y, label=label, ls=style,alpha=alpha,**extra)

    if logy:
      ax.set_yscale('log')
    if logx:
      ax.set_xscale('log')

    if grid:
      ax.grid(color='k', alpha=0.5, linestyle=':', linewidth=0.5)

    if redraw:
      self.redraw_fig()

  def add_errorbarplot(self,x,y,yerror,ax_index=1,xlabel="",ylabel="",label=None,logy=False,logx=False,style='-',marker='o',markersize=_def_markersize,color=None,alpha=1.0,grid=True,redraw=False,extra={}):
    ax = self.get_ax(ax_index=ax_index)
    if ax==None:
      self.add_ax(ax_index=ax_index)
      ax = self.get_ax(ax_index=ax_index)

    if xlabel != "":
      ax.set_xlabel(xlabel)
    if ylabel != "":
      ax.set_ylabel(ylabel)

    if color:
      ax.errorbar(x, y, yerr=yerror, ls=style, fmt=style+marker, markersize=markersize, c=color, label=label, alpha=alpha, **extra)
    else:
      ax.errorbar(x, y, yerr=yerror, ls=style, fmt=style+marker, markersize=markersize, label=label, alpha=alpha, **extra)

    if logy:
      ax.set_yscale('log')
    if logx:
      ax.set_xscale('log')

    if grid:
      ax.grid(color='k', alpha=0.5, linestyle=':', linewidth=0.5)

    if redraw:
      self.redraw_fig()

  def add_2dcolorplot(self,x,y,z,ax_index=1,label="",xlabel="",ylabel=None,logy=False,logx=False,colormap=None,grid=False,redraw=False,extra={}):
    """
    Input are regular x and y arrays and corresponding z array where:
    z[i*len(y)+j] <=> (x[i],y[j])
    """
    ax = self.get_ax(ax_index=ax_index)
    if ax==None:
      self.add_ax(ax_index=ax_index)
      ax = self.get_ax(ax_index=ax_index)

    if xlabel != "":
      ax.set_xlabel(xlabel)
    if ylabel != "":
      ax.set_ylabel(ylabel)

    X,Y = plt.meshgrid(x,y)

    Z=[]
    row=[]
    for i in range(len(x)):
      row=[]
      for j in range(len(y)):
        row.append(z[i*len(y)+j])
      Z.append(row)

    minz=min(z)
    maxz=max(z)

    if not extra.has_key("vmax"):
      extra["vmax"] = 10**np.ceil(np.log10(maxz))
    if not extra.has_key("vmin"):
      extra["vmin"] = 10**np.floor(np.log10(minz))

    if max(z) > 0.0 and min(z) > 0.0:
      colornorm = LogNorm()
    else:
      colornorm = None

    if colormap:
      p = ax.pcolormesh(X, Y, np.array(Z).T, cmap=colormap, norm = colornorm,**extra)
    else:
      p = ax.pcolormesh(X, Y, np.array(Z).T, cmap=custom_colormap, norm = colornorm,**extra)

    self.colorbar = self.fig.colorbar(p, ax=ax, label=label)
    if label!=None:
      self.colorbar.set_label(label, labelpad=1)

    if logy:
      print "logy does not work for 2d colorplot yet"#ax.set_yscale('log')
    if logx:
      print "logx does not work for 2d colorplot yet"#ax.set_xscale('log')

    if grid:
      ax.grid(color='k', alpha=0.5, linestyle=':', linewidth=0.5)

    if redraw:
      self.redraw_fig()

  def add_hexbinplot(self,x,y,ax_index=1,xlabel="",ylabel="",logy=False,logx=False,colormap=None,grid=False,redraw=False,addbar=False,extra={}):
    """
    Input are regular x and y arrays
    """
    ax = self.get_ax(ax_index=ax_index)
    if ax==None:
      self.add_ax(ax_index=ax_index)
      ax = self.get_ax(ax_index=ax_index)

    if xlabel != "":
      ax.set_xlabel(xlabel)
    if ylabel != "":
      ax.set_ylabel(ylabel)

    if colormap:
      p = ax.hexbin(x,y,cmap=colormap,**extra)
    else:
      p = ax.hexbin(x,y,**extra)

    if addbar:
      self.colorbar = self.fig.colorbar(p, ax=ax)

    if logy:
      print "logy does not work for hexbinplot yet"#ax.set_yscale('log')
    if logx:
      print "logx does not work for hexbinplot yet"#ax.set_xscale('log')

    if grid:
      ax.grid(color='k', alpha=0.5, linestyle=':', linewidth=0.5)

    if redraw:
      self.redraw_fig()

  def add_scatter(self,x,y,ax_index=1,xlabel="",ylabel="",label=None,marker='o',markerfillstyle='none',facecolors=TableauColors_ColorBlind10[4],markersize=_def_markersize,color=None,alpha=1.0,grid=True,redraw=False,extra={}):
    ax = self.get_ax(ax_index=ax_index)
    if ax==None:
      self.add_ax(ax_index=ax_index)
      ax = self.get_ax(ax_index=ax_index)
    self.lines = ax.get_lines()
    if xlabel != "":
      ax.set_xlabel(xlabel)
    if ylabel != "":
      ax.set_ylabel(ylabel)

    if color!=None:
      ax.scatter(x, y, label=label, marker=marker,s=markersize,edgecolors=color,facecolors=facecolors,alpha=alpha,**extra)
    else:
      ax.scatter(x, y, label=label, marker=marker,s=markersize,edgecolors='k',facecolors=facecolors,alpha=alpha,**extra)

    if grid:
      ax.grid(color='k', alpha=0.5, linestyle=':', linewidth=0.5)

    if redraw:
      self.redraw_fig()

  #found here: http://stackoverflow.com/questions/20105364/how-can-i-make-a-scatter-plot-colored-by-density-in-matplotlib
  def add_density_scatter(self,x,y,ax_index=1,xlabel="",ylabel="",label=None,marker='o',colormap=None,markersize=_def_markersize,alpha=1.0,grid=True,redraw=False,extra={}):
    ax = self.get_ax(ax_index=ax_index)
    if ax==None:
      self.add_ax(ax_index=ax_index)
      ax = self.get_ax(ax_index=ax_index)
    self.lines = ax.get_lines()
    if xlabel != "":
      ax.set_xlabel(xlabel)
    if ylabel != "":
      ax.set_ylabel(ylabel)

    if type(x)!=np.ndarray:
      print type(x)
      print "WARNING: in add_density_scatter: x is not an array, converting..."
      x=np.array(x)
    if type(y)!=np.ndarray:
      print type(y)
      print "WARNING: in add_density_scatter: y is not an array, converting..."
      y=np.array(y)

    # Calculate the point density
    xy = np.vstack([x,y])
    z = gaussian_kde(xy)(xy)

    # Sort the points by density, so that the densest points are plotted last
    idx = z.argsort()
    x, y, z = x[idx], y[idx], z[idx]

    if colormap:
      ax.scatter(x, y, c=z, cmap=colormap, label=label, marker=marker,s=markersize,edgecolors='',alpha=alpha,**extra)
    else:
#      ax.scatter(x, y, c=z, cmap=custom_colormap_density_scatter,  vmin=min(z), vmax=max(z), label=label, marker=marker,s=markersize,edgecolors='',alpha=alpha,**extra)
      ax.scatter(x, y, c=z, label=label, marker=marker,s=markersize,edgecolors='',alpha=alpha,**extra)

    if grid:
      ax.grid(color='k', alpha=0.5, linestyle=':', linewidth=0.5)

    if redraw:
      self.redraw_fig()

  def add_hist(self,h,ax_index=1,xlabel="",ylabel="",label=None,bins=50,histtype='stepfilled',color=None,alpha=1.0,normalize=1,redraw=False,extra={},binned_data=False,logx=False,logy=False):
    ax = self.get_ax(ax_index=ax_index)
    if not ax:
      self.add_ax(ax_index=ax_index)
      ax = self.get_ax(ax_index=ax_index)
    if xlabel != "":
      ax.set_xlabel(xlabel)
    if ylabel != "":
      ax.set_ylabel(ylabel)

    if type(h[0])==np.ndarray and type(h[0]) != list:

      if type(label) != np.ndarray and type(label) != list:
        print "Error in add_hist: If h is an array, label must be an array as well"
        return
      else:
        if len(label) != len(h):
          print "Error in add_hist: h and label do not have the same length"
          return

      if type(color) != np.ndarray and type(color) != list:
        print "Error in add_hist: If h is an array, color must be an array as well"
        return
      else:
        if len(color) != len(h):
          print "Error in add_hist: color must be same length as h"
          return

    if binned_data:
      if type(bins) != np.ndarray and type(bins) != list:
        print "Error in add_hist: If passing binned data, bins must be a list type object"
        return
      center = (bins[:-1] + bins[1:]) / 2
      y = center
      weights = h
      if logx:
        y = np.log10(y)
        bins = np.log10(bins)

    if color!=None:
      if binned_data:
        ax.hist(y,bins=bins,weights=weights,normed=normalize,alpha=alpha,color=color,histtype=histtype,label=label,**extra)
      else:
        ax.hist(h,bins=bins,normed=normalize,alpha=alpha,color=color,histtype=histtype,label=label,**extra)
    else:
      if binned_data:
        ax.hist(y,bins=bins,weights=weights,normed=normalize,alpha=alpha,histtype=histtype,label=label,**extra)
      else:
        ax.hist(h,bins=bins,normed=normalize,alpha=alpha,histtype=histtype,label=label,**extra)

    if logy:
      ax.set_yscale('log', nonposy='clip')

    if redraw:
      self.redraw_fig()

  def add_annotation(self,text,location,ax_index=1,pointat=None,textcoord='axes fraction',pointatcoord='data',
                          horalign='center',vertalign='bottom',textcolor='k',
                          arrowproperties=dict(arrowstyle="->", connectionstyle="angle3,angleA=0,angleB=-90"),
                          boxproperties=None,extra={}):
    ax = self.get_ax(ax_index=ax_index)
    if not ax:
      print "Weird: Adding annotation to a new ax without anything on it!"
      self.add_ax(ax_index=ax_index)
      ax = self.get_ax(ax_index=ax_index)

    if not pointat:
      ax.annotate(text, xy=(location[0], location[1]), xytext=(location[0], location[1]),
              arrowprops=dict(alpha=0.0),
              bbox=boxproperties,
              textcoords=textcoord,xycoords=pointatcoord,
              horizontalalignment=horalign, verticalalignment=vertalign,
              color=textcolor,**extra
              )
    else:
      ax.annotate(text, xy=(pointat[0], pointat[1]), xytext=(location[0], location[1]),
              arrowprops=arrowproperties,
              bbox=boxproperties,
              textcoords=textcoord,xycoords=pointatcoord,
              horizontalalignment=horalign, verticalalignment=vertalign,
              color=textcolor,**extra
              )

  def add_legend(self,ax_index=1,location='best',ncols=1,alpha=0.7,redraw=False,fontsize=None,extra={}):
    ax = self.get_ax(ax_index=ax_index)
    if not ax:
      print "Error in add_legend: no ax found at index %d"%ax_index
      return None

    prop=None
    if fontsize != None:
      prop={'size':fontsize}

    self.leg = ax.legend(loc=location,ncol=ncols,prop=prop,**extra)
    if self.leg != None:
      self.leg.get_frame().set_alpha(alpha)

    if redraw:
      self.redraw_fig()

  def add_grid(self,ax_index=1,color='k',alpha=0.5,linestyle=':',linewidth=0.5,redraw=False):
    ax = self.get_ax(ax_index=ax_index)
    if not ax:
      print "Error in add_grid: no ax found at index %d"%ax_index
      return None

    ax.grid(color=color, alpha=alpha, linestyle=linestyle, linewidth=linewidth)

    if redraw:
      self.redraw_fig()


