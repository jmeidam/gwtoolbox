__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

import numpy as np
from os import path,makedirs,system
from fileinput import FileInput
import random
import subprocess
from cPickle import dump, load

"""
General utility functions that do not fit in any particular submodule
"""

def NextPow2(N):
  return 1<<(N-1).bit_length()

def Pow10Floor(x):
  """return the nearest power of 10 smaller than x"""
  return 10**np.floor(np.log10(x))

def Pow10Ceil(x):
  """return the nearest power of 10 greater than x"""
  return 10**np.ceil(np.log10(x))

def randparam(rmin, rmax, seed=None):
  """Return a random float between rmin and rmax"""
  random.seed(seed)
  return rmin + (rmax - rmin)*random.random()

def lnPLUS(x,y):
  """
  FUNCTION TO ADD IN LOGARITHMIC SPACE
  i.e.:
    log(a+b) = lnPLUS( log(a) , log(b) )
  """

  output = 0.0
  #print x, y
  if x>y:
    output = x+np.log(1.0+np.exp(y-x))
  else:
    output = y+np.log(1.0+np.exp(x-y))

  return output

################################################################################
####                      LIST/ARRAY TOOLS
################################################################################

def PadNextPow2(topad,value):
  N = len(topad)
  pow2 = NextPow2(N)
  padded = list(topad) + [value] * (pow2 - N)
  return np.array(padded)

def IsStrictlyIncreasing(L):
    yes = True
    for x,y in zip(L, L[1:]):
      if x>y:
        yes = False
    return yes

def IsStrictlyDecreasing(L):
    yes = True
    for x,y in zip(L, L[1:]):
      if x<y:
        yes = False
    return yes

def IsMonotonic(L):
  if IsStrictlyDecreasing(L) or IsStrictlyIncreasing(L):
    return True
  else:
    return False

def SortListFromColumn(list,idx):
  """
  sort list using column "idx"
  """
  L = len(list)
  sorted = list
  for i in range(L):
    for j in range(i+1,L):
      if sorted[j][idx] < sorted[i][idx]:
        sorted[j], sorted[i] = sorted[i], sorted[j]

  return sorted

def Combinations(iterable, r):
  """
  Finds unique combinations of elements in iterable
  of length r.
  For example:
    combinations('ABCD', 2) --> AB AC AD BC BD CD
    combinations(range(4), 3) --> 012 013 023 123
  """
  pool = tuple(iterable)
  n = len(pool)
  if r > n:
    return
  indices = range(r)
  yield tuple(pool[i] for i in indices)
  while True:
    for i in reversed(range(r)):
      if indices[i] != i + n - r:
        break
    else:
      return
    indices[i] += 1
    for j in range(i+1, r):
      indices[j] = indices[j-1] + 1
    yield tuple(pool[i] for i in indices)

################################################################################
####                      FILE IO
################################################################################

def EnsureDir(f, stripfilename=False):
  """
  Create folder if it does not exist

  stripfilename : provides possibility to pass a path including a filename
                  if set to True
  """
  if stripfilename:
    F=path.dirname(f)
  else:
    F=f
  if not path.exists(F):
      makedirs(F)


def file_len(fname):
  if path.isfile(fname):
    p = subprocess.Popen(['wc', '-l', fname], stdout=subprocess.PIPE,
                                              stderr=subprocess.PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])
  else:
    return 0

def replace_in_string(inputstring,lookup):
  """Replaces strings in the lookup dict with
  corresponding values.

  Parameters
  ----------
  inputstring: string
      string that has certain substrings that need replacing
  lookup: dict
      dictionary containing keys (substrings) and values (to be replaced with)

  Example
  ----------
  replace_in_string("filename_TEST_more.dat",{"TEST":123})
    --> "filename_123_more.dat"
  """
  newstring=inputstring
  for item in lookup.keys():
    newstring=newstring.replace(item,str(lookup[item]))
  return newstring


def replace_in_file(filename,var,string):
  """Replaces a sting in a file with something else
  Note: This will alter the file

  Parameters
  ----------
  filename: string
      path of the input file
  var: string
      The string to be searched for and replaced
  string: string
      The new string
  """
  for line in FileInput(filename,inplace=1):
      line = line.rstrip('\n').replace(var,string)
      print line,

def WriteObjectToPickle(obj,filename):
  """
  Write any object to a pickle file
  """
  with open(filename,'w') as fp:
    dump(obj,fp,2)

def ReadObjectFromPickle(filename):
  """
  Retrieve whatever object was stored in \"filename\"
  """
  fp = open(filename,'r')
  return load(fp)


def callback_spaces(option, opt_str, value, parser):
  """
  Function for parsing space separated arguments
  Use as follows:
    parser.add_option(...,action="callback", callback=callback_spaces,...)
  """
  assert value is None
  value = []
  def floatable(str):
    try:
      float(str)
      return True
    except ValueError:
      return False
  for arg in parser.rargs:
    # stop on --foo like options
    if arg[:2] == "--" and len(arg) > 2:
      break
    # stop on -a, but not on -3 or -3.0
    if arg[:1] == "-" and len(arg) > 1 and not floatable(arg):
      break
    value.append(arg)
  del parser.rargs[:len(value)]
  setattr(parser.values, option.dest, value)


def add_cronjob(timestamp,command):
  """Add a cron job to what is already present

  Parameters
  ----------
  timestamp: string
      typical cron timestap like "30 2 * * *"
  command: string
      command the crontab needs to run
  """
  #Write out the current crontab
  system("crontab -l > cron_tempfile")
  #echo new cron into cron file
  system("echo \"%s %s\" >> cron_tempfile"%(timestamp,command))
  #install new cron file
  system("crontab cron_tempfile")
  #remove temp file
  system("rm cron_tempfile")