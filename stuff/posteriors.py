# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 16:22:17 2016

@author: jeroen
"""

from numpy import linspace,cumsum,random,searchsorted,genfromtxt,exp
from scipy.stats import gaussian_kde
import tools as tb

class posterior():
  def __init__(self,sourcename,paramname,posteriorfile,kde_min,kde_max,headerfile="",kde_points=1000,kde_cov_factor=0.25):
    """
    Container for the posterior samples and the kde
    """

    self.file = posteriorfile
    self.paramname = paramname
    self.sourcename = sourcename
    self.posterior=[]

    self.xaxis = linspace(kde_min,kde_max,kde_points)

    with open(posteriorfile,'r') as f:
      if headerfile == "":
        header = f.readline().strip().split() #get header
      else:
        header = genfromtxt(headerfile,dtype=str)

      #create a parameter:index dictionary
      cols = {}
      for p,i in zip(header,range(len(header))):
        cols[p] = i

      if cols.has_key(paramname):
        data = f.readlines()
        c = cols[paramname]
        for line in data:
          self.posterior.append(float(line.split()[c]))
      elif paramname == "q":
        data = f.readlines()
        self.posterior = self._q_from_eta(data,cols)
      elif paramname == "eta":
        data = f.readlines()
        self.posterior = self._eta_from_q(data,cols)
      elif paramname in ["mass1","mass2"]:
        data = f.readlines()
        print "calculating %s from other parameters"%paramname
        if cols.has_key("logmc"):
          if paramname == "mass1":
            self.posterior = self._mass1_from_logmc_eta(data,cols)
          else:
            self.posterior = self._mass2_from_logmc_eta(data,cols)
        else:
          if paramname == "mass1":
            self.posterior = self._mass1_from_mc_eta(data,cols)
          else:
            self.posterior = self._mass2_from_mc_eta(data,cols)
      else:
        print "parameter %s not present in posterior file"%paramname

    #calculate kde
    density = gaussian_kde(self.posterior)
    density.covariance_factor = lambda : kde_cov_factor
    density._compute_covariance()
    self.kde = density(self.xaxis)

  def _eta_from_q(self,data,coldict):
    pos = []
    c = coldict["q"]
    for line in data:
      q = float(line.split()[c])
      eta = tb.convert_q_eta(q)
      pos.append(eta)
    return pos

  def _q_from_eta(self,data,coldict):
    pos = []
    c = coldict["eta"]
    for line in data:
      eta = float(line.split()[c])
      q = tb.convert_eta_q(eta)
      pos.append(q)
    return pos

  def _mass1_from_logmc_eta(self,data,coldict):
    pos = []
    c_eta = coldict["eta"]
    c_logmc = coldict["logmc"]
    for line in data:
      mc = exp(float(line.split()[c_logmc]))
      eta = float(line.split()[c_eta])
      M = mc*pow(eta,-3./5.)
      mass1,mass2 = tb.convert_Meta_to_m1m2(M,eta)
      pos.append(mass1)
    return pos

  def _mass2_from_logmc_eta(self,data,coldict):
    pos = []
    c_eta = coldict["eta"]
    c_logmc = coldict["logmc"]
    for line in data:
      mc = exp(float(line.split()[c_logmc]))
      eta = float(line.split()[c_eta])
      M = mc*pow(eta,-3./5.)
      mass1,mass2 = tb.convert_Meta_to_m1m2(M,eta)
      pos.append(mass2)
    return pos

  def _mass1_from_mc_eta(self,data,coldict):
    pos = []
    c_eta = coldict["eta"]
    c_logmc = coldict["mc"]
    for line in data:
      mc = float(line.split()[c_logmc])
      eta = float(line.split()[c_eta])
      M = mc*pow(eta,-3./5.)
      mass1,mass2 = tb.convert_Meta_to_m1m2(M,eta)
      pos.append(mass1)
    return pos

  def _mass2_from_mc_eta(self,data,coldict):
    pos = []
    c_eta = coldict["eta"]
    c_logmc = coldict["mc"]
    for line in data:
      mc = float(line.split()[c_logmc])
      eta = float(line.split()[c_eta])
      M = mc*pow(eta,-3./5.)
      mass1,mass2 = tb.convert_Meta_to_m1m2(M,eta)
      pos.append(mass2)
    return pos


def generate_rand_from_pdf(pdf, x_axis, N):
  cdf = cumsum(pdf)
  cdf = cdf / cdf[-1]
  values = random.rand(N)
  value_bins = searchsorted(cdf, values)
  random_from_cdf = x_axis[value_bins]
  return random_from_cdf

