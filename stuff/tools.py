# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 13:56:26 2015

@author: jeroen
"""

from numpy import pi,sqrt,zeros,ceil,floor,log10,log,exp,cos,sin,array,vdot
from cPickle import dump, load
from os import path,makedirs
from random import seed,random

MSUN_SEC = 4.925492321898864e-06
MSUN_SI  = 1.98892e+30
MNEUTRON_SI = 1.6749286e-27
MPROTON_SI = 1.6726231e-27
MELECTRON_SI = 9.1093897e-31
PC_SI = 3.085677581491367e+16 #parsec in m
C_SI = 299792458.0
G_SI = 6.67384e-11


#### GW TOOLBOX ####

def fISCO(M):
  M=M*MSUN_SEC
  return pow(6.0,-3./2.)/(pi*M)

def chirptime(m1,m2,flow):
  M=m1+m2
  eta = m1*m2/(M*M)
  Mchirp = M*eta**(3./5.)
  A = (1.21/Mchirp)**(5./3.)
  B = (100./flow)**(8./3.)
  return 2.18*A*B

def bandtime(m1,m2,f0,f1):
  """
  Calculate the time an inspiral spends within the frequency band f0 < f < f1.
  Formula is obtained from integrating 0PN df/dt (see e.g. Maggiore Eq. 4.18) from f0 to f1
  """
  Mc = Mchirp(m1,m2)
  Mc *= MSUN_SEC
  A = 5.0*pow(pi,-8.0/3.0)*pow(Mc,-5.0/3.0)/256.0
  B0 = pow(f0,-8./3.)
  B1 = pow(f1,-8./3.)
  return A*B0 - A*B1

def NextPow2(N):
  return 1<<(N-1).bit_length()

def pow10floor(x):
    return 10**floor(log10(x))

def pow10ceil(x):
    return 10**ceil(log10(x))

def getFrequencyIndex(f,df):
  return ceil(f/df)

def calc_tau0(M,eta,flow):
  return 5.0*pow(pi*M*MSUN_SEC*flow,-5./3.)/(256.0*pi*flow*eta)

def calc_tau3(M,eta,flow):
  return pow(pi*M*MSUN_SEC*flow,-2./3.)/(8.0*flow*eta)

def createFrequencySeries(fmax,df):
  N = int(NextPow2( int(fmax/df) )+1)
  f = zeros(N)
  for i in range(len(f)):
    f[i] = i*df
  return f

def Ncyc(m1,m2,fmin,fmax):
  """
  The amount of cycles between the frequency band fmin < f < fmax
    (Maggiore Eq. 4.23)
  """
  m1 = m1*MSUN_SEC
  m2 = m2*MSUN_SEC
  A = pow(pi,-8./3.)/32.0
  Mc = Mchirp(m1,m2)
  C = A*pow(Mc,-5./3.)
  if fmin >= fmax:
    return 0.0
  else:
    return C*pow(fmin,-5./3.)-C*pow(fmax,-5./3.)

def generateSpinVector(s,mag=None,theta=None,phi=None):

  """
  s: seed
  mag: magnitude
  theta: angle1
  phi: angle2
  """

  seed(s)

  if not mag:
    mag = random()
  if not theta:
    theta = -1.0 + random()*pi
  if not phi:
    phi = 2.0*random()*pi

  x=mag*cos(theta)*sin(phi)
  y=mag*sin(theta)*sin(phi)
  z=mag*cos(phi)

  return array([x,y,z])


#### MASS PARAMETER CONVERSIONS ####

def convert_Meta_to_m1m2(M,eta):
  """Convert total mass and symm mass ratio to m1,m2"""
  m1 = 0.5*M*(1.0+sqrt(1.0-4.0*eta))
  m2 = M-m1
  return m1,m2

def convert_Mceta_to_M(Mc,eta):
  """Convert chirp mass and symmetric mass ratio to total mass"""
  return Mc*pow(eta,-3./5.)

def convert_Mceta_to_m1m2(Mc,eta):
  M = Mc*pow(eta,-3./5.)
  m1,m2 = convert_Meta_to_m1m2(M,eta)
  return m1,m2

def convert_q_to_eta(q):
  """Convert mass ratio (which is >= 1) to symmetric mass ratio"""
  x = 1+q
  return q/x/x

def convert_eta_to_q(nu):
  """Convert symmetric mass ratio to mass ratio (which is >= 1)"""
  return (1.+sqrt(1.-4.*nu)-2.*nu)/(2.*nu)

def convert_m1m2_to_Mc(m1,m2):
  """Chirp mass from m1, m2"""
  return (m1*m2)**(3./5.)/(m1+m2)**(1./5.)

def convert_m1m2_to_nu(m1,m2):
  """Symmetric mass ratio from m1, m2"""
  return m1*m2/(m1+m2)**2

def convert_Mq_to_Mc(M, q):
  """Convert mass ratio, total mass pair to chirp mass"""
  return M*convert_q_to_eta(q)**(3./5.)

def convert_Mq_to_m1m2(M, q):
  """Convert total mass, mass ratio pair to m1, m2"""
  m2 = M/(1.+q)
  m1 = M-m2
  return [m1, m2]

def convert_Mcq_to_M(Mc, q):
  """Convert mass ratio, chirp mass to total mass"""
  return Mc*convert_q_to_eta(q)**(-3./5.)

def convert_Mcq_to_m1m2(Mc,q):
  eta = convert_q_to_eta(q)
  M = Mc*pow(eta,-3./5.)
  m1,m2 = convert_Meta_to_m1m2(M,eta)
  return m1,m2

def convert_Meta_to_Mc(M, eta):
  """Convert total mass and symmetric mass ratio to chirp mass"""
  return M*eta**(3./5.)


def Mchirp(m1,m2):
  M=m1+m2
  eta = m1*m2/(M*M)
  return M*eta**(3./5.)

def calc_eta(m1,m2):
  M=m1+m2
  M2 = M*M
  return m1*m2/M2



#### vector tools ####

def dot_product(a,b):

  assert len(a) == len(b)
  return vdot(a, b)

def normalize(h):

  A = dot_product(h,h)

  new = h/sqrt(abs(A))

  return new

def calculate_error(h1,h2):
  h1_n = normalize(h1)
  h2_n = normalize(h2)
  diff = h1_n - h2_n
  err = dot_product(diff,diff)
  return abs(err)



#### LIST TOOLS ####

def pad_NextPow2(topad,value):
  N = len(topad)
  pow2 = NextPow2(N)
  padded = list(topad) + [value] * (pow2 - N)
  return array(padded)

def is_strictly_increasing(L):
    yes = True
    for x,y in zip(L, L[1:]):
      if x>y:
        yes = False
    return yes

def is_strictly_decreasing(L):
    yes = True
    for x,y in zip(L, L[1:]):
      if x<y:
        yes = False
    return yes

def is_monotonic(L):
  if is_strictly_decreasing(L) or is_strictly_increasing(L):
    return True
  else:
    return False

def vectorfromlist(list,columnidx):
  """
  Outputs requested column of "list" as a vector
  """
  vector = []
  for line in list:
    vector.append(line[columnidx])

  return vector

def sort_list_from_column(list,idx):
  """
  sort list using column "idx"
  """
  L = len(list)
  sorted = list
  for i in range(L):
    for j in range(i+1,L):
      if sorted[j][idx] < sorted[i][idx]:
        sorted[j], sorted[i] = sorted[i], sorted[j]

  return sorted





#### FILE INPUT/OUTPUT ####

def WriteObjectToPickle(obj,filename):
  """
  Write any object to a pickle file
  """
  with open(filename,'w') as fp:
    dump(obj,fp,2)

def ReadObjectFromPickle(filename):
  """
  Retrieve whatever object was stored in \"filename\"
  """
  fp = open(filename,'r')
  return load(fp)

def ensure_dir(f, stripfilename=False):
  """
  Create folder if it does not exist
  """
  if stripfilename:
    F=path.dirname(f)
  else:
    F=f
  if not path.exists(F):
      makedirs(F)



#### GENERAL TOOLS ####

def randparam(rmin, rmax):
  """
  Return a random float between rmin and rmax
  """
  return rmin + (rmax - rmin)*random()

def lnPLUS(x,y):
  """
  FUNCTION TO ADD IN LOGARITHMIC SPACE
  i.e.:
    log(a+b) = lnPLUS( log(a) , log(b) )
  """

  output = 0.0
  #print x, y
  if x>y:
    output = x+log(1.0+exp(y-x))
  else:
    output = y+log(1.0+exp(x-y))

  return output

def evaluate_polinomial(x,pol,N,print_coefficients=False):
  """
  Evaluate N order polynomial at x.
  pol : polynomial object from pylab.polyfit()
  print_coefficients : Does not evaluate, only prints coeffs.
  """
  if not print_coefficients:
    n=N
    y=pol[N]
    for i in range(N):
      n = N-i
      y+=pol[i]*x**n
    return y
  else:
    n=N
    y="y = %.3f"%pol[N]
    for i in range(N):
      n = N-i
      if pol[i] > 0.0:
        y+=" + %.3f x^%d"%(pol[i],n)
      else:
        y+=" - %.3f x^%d"%(abs(pol[i]),n)
    print y


