#! python

__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

import ConfigParser
from optparse import OptionParser
import sys
import os

from gwtoolbox.roqpipe import pipeutils as pu
from gwtoolbox.roqpipe import modelspecs as ms
from gwtoolbox import genutils as gu
from glob import glob
import ast
import time

usage=""" %prog [options] baserun_folder/validations_setup.cfg
Setup a validation run

The user must specify a cfg file from the run to validate.
It is assumed that this run was completed properly
and EIM files are present in the same folder.

The folder structure will be as follows:
 /path/to/rundir/
   baserun_folder : home of GreedyPoints.txt, masterconfig.ini and the data files
                    (folder may be located anywhere, but will we copied here)
   validationN/
     results/       : resulting bad_points* files and such
     points/        : files containing the validationpoints
     jobs/          : home of all the scripts and std out/err files
   runN/          : If bad_points were found, an enrichment run is setup in here
   quadvalidationN/
     results/       : resulting bad_points* files and such
     points/        : files containing the validationpoints
     jobs/          : home of all the scripts and std out/err files
   quadrunN/      : If bad_points were found, an enrichment run is setup in here

NOTE: Most arguments are not required if roq_validate is performed
      on a run that was generated with roq_pipe
      Most settings will be drawn from the masterconfig.ini in the baserun directory
      Minimal call for using $(pwd) as the rundir:
        roq_validate.py /path/to/baserun/validations_setup.cfg
"""

def determine_validationnumber(runpath,quadratic=False):
  """
  Automatically determine the validation number by searching for
  folders named validationN and quadvalidationN
  """
  if quadratic:
    globbed = glob(os.path.join(runpath,"quadvalidation*"))
    globbed=[os.path.basename(F) for F in globbed]
    numbers=[F.strip("quadvalidation") for F in globbed]
  else:
    globbed = glob(os.path.join(runpath,"validation*"))
    globbed=[os.path.basename(F) for F in globbed]
    numbers=[F.strip("validation") for F in globbed]

  Ns=[]
  # Convert the numbers to ints, ignore anything behind "validation" that was not int
  for num in numbers:
    try:
      Ns.append(int(num))
    except:
      pass

  if len(Ns)>0:
    N = max(Ns)+1
  else:
    N = 1

  return N

parser=OptionParser(usage)
parser.add_option("-r","--run-path",default=None,action="store",type="string",help="Directory to run in (default: $PWD)",metavar="RUNDIR")
parser.add_option("-v", "--validation",default=None,action="store",type=int, help="The validation number (default: determined from currently present validation folders)",metavar="NUM")
parser.add_option("-q", "--quadratic",dest="quadratic", action="store_true", default=False,help="Perform validation on quadratic run (default: False)")
parser.add_option("-P", "--quadpoints",dest="quadpointsfile", action="store", default="", type="string",help="Location to quadpoints file that will be used for validation. (default: use file from cfg)",metavar="PATH")
parser.add_option("-W", "--quadweights",dest="quadweightsfile", action="store", default="", type="string",help="Location to quadweights file that will be used for validation. (default: use file from cfg)",metavar="PATH")
parser.add_option("-T", "--request-runtime",default=8,action="store",type=int, help="The runtime in hours that the validation is expected to use. (default: 8)",metavar="HOURS")
parser.add_option("-d", "--disable-generatepoints",dest="disable_gen", action="store_true", default=False,help="Do not generate the validation points (use this is points are already present in validationN/points folder)")
parser.add_option("-k", "--kerr-bound",dest="kerrbound",action="store",default=0.0, type=float, help="allows to set a different value for the kerr bound (i.e. chi1L^2+chip^2 <= kerrbound) (default: 1.0)")

parser.add_option("-l", "--ldlibpath",default=None,action="store",type="string", help="Location string for LD_LIBRARY_PATH (default: from masterconfig.ini if present)",metavar="path1:path2:...")
parser.add_option("-p", "--pythonpath",default=None,action="store",type="string", help="Location string for PYTHONPATH (default: from masterconfig.ini if present)",metavar="path1:path2:...")
parser.add_option("-n", "--nonGRparam",dest="nongrparam",default=None,action="store",type="string", help="The non-GR parameter used in this validation (default: from masterconfig.ini if present)",metavar="PARAM")
parser.add_option("-x", "--nonGRparam-min",dest="nongrmin", action="store",default=None, type=float, help="overwrite default min nonGRparam value (default: from masterconfig.ini if present)",metavar="MIN")
parser.add_option("-X", "--nonGRparam-max",dest="nongrmax", action="store",default=None, type=float, help="overwrite default max nonGRparam value (default: from masterconfig.ini if present)",metavar="MAX")
parser.add_option("-z", "--model",dest="model", action="store", default=None, type="string",help="model name like PhenomP_plus (default: from masterconfig.ini if present)",metavar="MODEL")
parser.add_option("-m","--mc-min",dest="mcmin",default=None,action="store", type=float, help="min chirp mass (default: from masterconfig.ini if present)",metavar="MIN")
parser.add_option("-M","--mc-max",dest="mcmax",default=None,action="store", type=float, help="max chirp mass (default: from masterconfig.ini if present)",metavar="MAX")
parser.add_option("-s", "--seglen",default=None,action="store",type=int, help="segment length in seconds (default: from masterconfig.ini if present)",metavar="SEGLEN")



(opts,args)=parser.parse_args()

if len(args)!=1:
  parser.print_help()
  print 'Error: must specify one cfg file'
  sys.exit(1)

print ""
print "Trying to read original cfg..."
cfg = pu.greedycpp_cfg()
if os.path.isfile(args[0]):
  cfg.read(args[0])
else:
  exit("Error: cfg file not found (%s)"%args[0])

if opts.run_path:
  rundir = os.path.normpath(opts.run_path)
else:
  exit("Error: Must specify run path")

if opts.validation:
  validation = opts.validation
else:
  validation = determine_validationnumber(rundir,quadratic=opts.quadratic)

baserundir = os.path.dirname(args[0])


#check that the baserundir is in the rundir, otherwise copy it there
#For example: If the rundir is /home/user/dchi3/8s
#             And the baserundir is /data/tunnel/user/dchi3/8s/run0
#             then the folder run0 will be copied to
#             /home/user/dchi3/8s/run0
if os.path.dirname(baserundir) != rundir:
  print "copying %s to the validation rundir"%(os.path.basename(baserundir))
  os.system("cp -r %s %s"%(baserundir,rundir))
  baserundir=os.path.join(rundir,os.path.basename(baserundir))


inifile=os.path.join(baserundir,"masterconfig.ini")

if opts.quadratic:
  jobsdir=os.path.join(rundir,"quadvalidation%d"%validation,"jobs")
  resultsdir=os.path.join(rundir,"quadvalidation%d"%validation,"results")
  pointsdir=os.path.join(rundir,"quadvalidation%d"%validation,"points")
else:
  jobsdir=os.path.join(rundir,"validation%d"%validation,"jobs")
  resultsdir=os.path.join(rundir,"validation%d"%validation,"results")
  pointsdir=os.path.join(rundir,"validation%d"%validation,"points")
gu.EnsureDir(jobsdir)
gu.EnsureDir(resultsdir)
gu.EnsureDir(pointsdir)

statefile=os.path.join(jobsdir,"statefile.txt")
qsubs_validation=os.path.join(jobsdir,"qsubs_validation.sh")
qsubs_binning=os.path.join(jobsdir,"qsubs_binning.sh")


# try to read a few things from the masterconfig that should be present in the
# rundir if this was created with roq_pipe.py
ldlibpath=""
pythonpath=""
nonGRparam=""
generatepoints_script=""
binsfromdata_script=""
verifyOMP=""
greedycpp=""
eim=""
greedyfiletype="npy"
nonGRparam_min=None
nonGRparam_max=None
kerrbound=0.0
mcmin=0.0
mcmax=0.0
seglen=0
modelname=""
cp=None

print""
print "Trying to read options from the master config..."

if os.path.isfile(inifile):
  cp=ConfigParser.ConfigParser()
  cp.optionxform = str
  cp.readfp(open(inifile))
  if cp.has_option("greedycppengine","ldlibpath"):
    ldlibpath=cp.get("greedycppengine","ldlibpath")
  if cp.has_option("greedycppengine","pythonpath"):
    pythonpath=cp.get("greedycppengine","pythonpath")
  if cp.has_option("greedycppengine","output-data-format"):
    greedyfiletype=cp.get("greedycppengine","output-data-format")

  if cp.has_option("bins","generate-validationfile"):
    generatepoints_script=cp.get("bins","generate-validationfile")
  if cp.has_option("bins","bins-from-data"):
    binsfromdata_script=cp.get("bins","bins-from-data")
  if cp.has_option("bins","verify"):
    verifyOMP=cp.get("bins","verify")
  if cp.has_option("bins","eim"):
    eim=cp.get("bins","eim")
  if cp.has_option("bins","greedycpp"):
    greedycpp=cp.get("bins","greedycpp")

  if cp.has_option("validation","nonGRparam"):
    nonGRparam=cp.get("validation","nonGRparam")
  if cp.has_option("validation","nonGRparam-min"):
    nonGRparam_min=cp.getfloat("validation","nonGRparam-min")
  if cp.has_option("validation","nonGRparam-max"):
    nonGRparam_max=cp.getfloat("validation","nonGRparam-max")
  if cp.has_option("validation","kerr-bound"):
    kerrbound=cp.getfloat("validation","kerr-bound")

  if cp.has_option("validation","seglen"):
    seglen=cp.getint("validation","seglen")
  if cp.has_option("validation","mc-min"):
    mcmin=cp.getint("validation","mc-min")
  if cp.has_option("validation","mc-max"):
    mcmax=cp.getint("validation","mc-max")

  if opts.quadratic:
    if cp.has_option("model","model-quadname"):
      modelname=cp.get("model","model-quadname")
  else:
    if cp.has_option("model","model-name"):
      modelname=cp.get("model","model-name")

  print "Done"

else:
  print "NOTE: Could not find masterconfig.ini in the folder where it should be: validation section could not be parsed"
  print "      At the moment this file should really be present."

# Overwrite masterconfig settings with commandline arguments
if opts.ldlibpath:
  ldlibpath = opts.ldlibpath
if opts.pythonpath:
  pythonpath = opts.pythonpath
if opts.nongrparam:
  nonGRparam = opts.nongrparam
if opts.nongrmin:
  nonGRparam_min = opts.nongrmin
if opts.nongrmax:
  nonGRparam_max = opts.nongrmax
if opts.kerrbound != 0.0:
  kerrbound = opts.kerrbound
if opts.mcmin:
  mcmin = opts.mcmin
if opts.mcmax:
  mcmax = opts.mcmax
if opts.seglen:
  seglen = opts.seglen
if opts.model:
  modelname = opts.model
#TODO: allow for reading exectuables and greedy filetype from commandline in case for whatever reason the ini file is not used

if ldlibpath == "":
  exit("Error: Unable to determine LD_LIBRARY_PATH from masterconfig and no --ldlibpath provided.")
if pythonpath == "":
  exit("Error: Unable to determine PYTHONPATH from masterconfig and no --pythonpath provided.")
if nonGRparam == "":
  exit("Error: Unable to determine nonGRparam from masterconfig and no --nonGRparam provided.")
if seglen == 0:
  if mcmin == 0.0 or mcmax == 0.0:
    exit("Error: If seglen not provided somewhere, must provide mc-min and mc-max")
if modelname == "":
  print "Warning: No modelname was found, using PhenomP"
  modelname = "PhenomP"

# hardcoded number of jobs and points:
# TODO: This should also be in the commandline arguments, but I don't feel like doing that now
Njobs = 100
Npoints = 100000

# overwrite mcmin and mcmax if seglen was given
specs = None
if seglen != 0:
  specs = ms.roq_get_modeldefaults(modelname,seglen)
  mchirp = specs.get_parameter("mchirp")
  mcmin = mchirp.min
  mcmax = mchirp.max

print ""
print "Creating script to cat bad_points* files..."
catscript=pu.cat_script("catscript.sh",baserundir,jobsdir,resultsdir,nonGRparam,Njobs,mcmin=mcmin,mcmax=mcmax,seglen=seglen)
catscriptfile=catscript.writetofile()
os.system("chmod +x %s"%catscriptfile)
print "Script location : %s"%catscriptfile



# This is especially important for the errors since these are 10M points
print "Creating scripts to bin bad_points and/or errors..."
paramstobin=["err","eim"]
if cp.has_option("binning","paramstobin"):
  paramstobin+=ast.literal_eval(cp.get("binning","paramstobin"))

if cp.has_option("binning","nbins"):
  nbins=cp.getint("binning","nbins")
else:
  nbins=60
if cp.has_option("binning","logerrmin"):
  logerrmin=cp.getfloat("binning","logerrmin")
else:
  logerrmin=-14
if cp.has_option("binning","logerrmax"):
  logerrmax=cp.getfloat("binning","logerrmax")
else:
  logerrmax=0

binscripts=[]
for binparam in paramstobin:
  if binparam not in ["err","eim"]:
    if cp.has_option("binning","%s-min"%binparam):
      binmin=cp.getfloat("binning","%s-min"%binparam)
    else:
      if specs:
        p=specs.get_parameter(binparam)
        binmin=p.min
      else:
        exit("Error: no param-min provided and no seglen given. Cannot load default model specs.")
    if cp.has_option("binning","%s-max"%binparam):
      binmax=cp.getfloat("binning","%s-max"%binparam)
    else:
      if specs:
        p=specs.get_parameter(binparam)
        binmax=p.max
      else:
        exit("Error: no param-max provided and no seglen given. Cannot load default model specs.")
    if cp.has_option("binning","%s-col"%binparam):
      column=cp.getint("binning","%s-col"%binparam)
    else:
      if specs:
        p=specs.get_parameter(binparam)
        column=p.index
      else:
        exit("Error: no param-col provided and no seglen given. Cannot load default model specs.")
    if cp.has_option("binning","%s-bins"%binparam):
      nbins=cp.getint("binning","%s-bins"%binparam)
  else:
    binmin=logerrmin
    binmax=logerrmax
    if binparam == "err":
      column = 26
      if "TaylorF2" in modelname:
        column = 14
    else:
      column = 25
      if "TaylorF2" in modelname:
        column = 13

  binscript=pu.binning_script("binning_%s.sh"%binparam,jobsdir,resultsdir,pointsdir,nonGRparam,binparam,binmin,binmax,column,[1,Njobs],
                              bins_from_data_script=binsfromdata_script,nbins=nbins,mcmin=mcmin,mcmax=mcmax,seglen=seglen,pythonpath=pythonpath)
  binscriptfile=binscript.writetofile()
  binscripts.append(binscriptfile)
  os.system("chmod +x %s"%binscriptfile)
  print "Script location : %s"%binscriptfile

with open(qsubs_binning,"w") as bfp:
  for scr in binscripts:
    bfp.write("qsub -q short -l walltime=01:00:00 -o %s_out -e %s_err %s\n"%(scr,scr,scr))


# This script already creates a cfg file in case the number of bad points > 0
# The bash script will only generate a run folder and copy the cfg there, if
# a followup run is required.
print ""
print "Creating script for the followup greedy run..."
followuprunscript=pu.enrichment_sweep_script("run_followup.sh",jobsdir,catscript.newtrainingset,
                                             baserundir,validation,executable="/bin/bash",
                                             ldlibpath=ldlibpath,greedy=greedycpp,eim=eim,
                                             quadpoints=opts.quadpointsfile,quadweights=opts.quadweightsfile,
                                             quadratic=opts.quadratic)
followuprunscriptfile=followuprunscript.writetofile()
os.system("chmod +x %s"%followuprunscriptfile)
print "Script location : %s"%followuprunscriptfile

qsubs=""
qsub_fp=open(qsubs_validation,"w")
#create a fresh state file
qsub_fp.write("if [ ! -f %s ]; then touch %s; else rm %s && touch %s ; fi\n"%(statefile,statefile,statefile,statefile))



print ""
print "Generating validation scripts and submit file..."
runtime=opts.request_runtime
qsubstring = "qsub"
if runtime < 1:
  runtime = 1
  print "Warning: I only understand runtimes > 1h, bacause it's less of a hassle and it is unlikely the validation run will take less than an hour anyway. runtime set to 1"
if runtime > 4:
  qsubstring+=" -q long"
else:
  qsubstring+=" -q short"
qsubstring += " -l walltime=%d:00:00"%runtime
validations = []
generate_points = True
if opts.disable_gen:
  generate_points = False
for i in range(1,Njobs+1):
  validations.append(pu.validation_script(jobsdir,resultsdir,pointsdir,catscript,followuprunscript,statefile,validation,baserundir,
                                          nonGRparam,i,modelname,
                                         seglen=seglen,pointsperfile=Npoints,nfiles=Njobs,mcmin=mcmin,mcmax=mcmax,
                                         kerrbound=kerrbound,nongrmin=nonGRparam_min,nongrmax=nonGRparam_max,
                                         Nlastfile=Njobs,ldlibpath=ldlibpath,pythonpath=pythonpath,
                                         generatepoints_script=generatepoints_script,verifyOMP=verifyOMP,greedyfiletype=greedyfiletype,
                                         generate_points=generate_points
                                         ))

  filename = validations[i-1].writetofile()
  qsub_fp.write("%s -o %s_out -e %s_err %s\n"%(qsubstring,filename,filename,filename))

print "Script location : %s/submit_validation*"%jobsdir
qsub_fp.close()
print ""
print "Submit the main jobs with %s"%qsubs_validation
print "When those are finished, you can submit binscripts with %s"%qsubs_binning
print ""




