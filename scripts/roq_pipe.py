#! python

__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

import ConfigParser
from optparse import OptionParser
import sys
import os
import time

from gwtoolbox.roqpipe import pipeutils as pu
from gwtoolbox import genutils as gu

usage=""" %prog [options] config.ini
Setup a greedycpp run for the Nikhef GRID or Stoomboot

The user must specify an ini file which will contain the main config options.
From this all the required files to submit the run will be generated.

Currently allowed sites:
 - nikhefgrid (submit scripts built for JDL and grid commands are produced)
 - stoomboot (submit scripts built for qsub)
 - local (job is a simple bash script)

NOTE: Submitting must be done manually.
      This script only sets up files and folders.

If site=nikhefgrid, look in the file "gridcommands" for all the commands
needed to upload files, run the job and retrieve results.

"""
parser=OptionParser(usage)
parser.add_option("-r","--run-path",default=None,action="store",type="string",help="Directory to run pipeline in (default: $PWD)",metavar="RUNDIR")
parser.add_option("-s", "--grid-site",default="nikhefgrid",action="store",type="string", help="Specify the site on which the job will run (nikhefgrid)",metavar="SITE")
parser.add_option("-I", "--delegation-id",default="",action="store",type="string", help="Delegation ID used to submit with glite-ce-job-submit.\n(nothing will be submitted, but the commands will be printed to a file for convenience)",metavar="ID")
parser.add_option("-q", "--quadratic",default=False,action="store_true", help="Build basis for the quadratic part. config.ini must have model-quadname option")


(opts,args)=parser.parse_args()

if len(args)!=1:
  parser.print_help()
  print 'Error: must specify one ini file'
  sys.exit(1)

inifile=args[0]

cp=ConfigParser.ConfigParser()
cp.optionxform = str
cp.readfp(open(inifile))

if not cp.has_section('paths'):
  cp.add_section('paths')
if opts.run_path is not None:
  cp.set('paths','rundir',os.path.abspath(opts.run_path))

if not cp.has_option('paths','rundir'):
  print 'Warning: No --run-path specified, using %s'%(os.getcwd())
  cp.set('paths','rundir',os.path.abspath(os.getcwd()))

gu.EnsureDir(cp.get('paths','rundir'))

if not cp.has_section('grid'):
  cp.add_section('grid')
cp.set('grid','site',opts.grid_site)


quadraticmode = opts.quadratic
job = pu.greedycpp_job(cp,quadratic=quadraticmode)

# Save the final configuration that is being used
conffilename=os.path.join(os.path.abspath(opts.run_path),'masterconfig.ini')
with open(conffilename,'w') as conffile:
  cp.write(conffile)

if job.site == "nikhefgrid":
  ID="$ID"
  if opts.delegation_id != "":
    ID = opts.delegation_id

  gridcommands="# This is not a script to execute from the command line\n"
  gridcommands+="# It is meant as a step-by-step intruction.\n"
  gridcommands+="# The commands within this script can be copied and executed when appropriate.\n"
  gridcommands+="# first initialize your voms certificate\n"
  gridcommands+="voms-proxy-init -voms virgo -valid 24:00\n"
  gridcommands+="\n"
  gridcommands+="# Make sure the remote directories exist:\n"
  for rf in job.remotefiles:
    gridcommands+="lfc-mkdir -p %s/%s\n"%(job.lfchome,os.path.dirname(rf.get_remote()))
  gridcommands+="# And make sure all the remote files are uploaded to the srm:\n"
  for rf in job.remotefiles:
    gridcommands+="lcg-cr --vo virgo -d %s/%s -l lfn:%s/%s \"file://%s\"\n"%(job.srmvirgo,rf.get_remote(),job.lfchome,rf.get_remote(),rf.get_local())
  gridcommands+="\n"
  gridcommands+="# Before submitting, either delegate the certificate to the remote site\n"
  gridcommands+="glite-ce-delegate-proxy -e gazon.nikhef.nl:8443 %s\n"%ID
  gridcommands+="# or renew it, if it was already present\n"
  gridcommands+="glite-ce-proxy-renew -e gazon.nikhef.nl:8443 %s\n"%ID
  gridcommands+="\n"
  if job.memory > 8192:
    queue = "himem"
  else:
    if job.time > 0:
      queue = "medium"
    else:
      queue = "short"
  gridcommands+="# submit the job\n"
  if job.memory >= 100000:
    print "NOTE: Memory request exceeds 100GB: Remember to ask grid-support to set up a reservation."
    gridcommands+="# NOTE: Memory request exceeds 100GB: Remember to ask grid-support to set up a reservation.\n"
  jobIDs=os.path.join(job.rundir,"jobIDs")
  gridcommands+="glite-ce-job-submit --resource gazon.nikhef.nl:8443/cream-pbs-%s -o %s --delegationId %s %s\n"%(queue,jobIDs,ID,job.jdlfile)
  gridcommands+="\n"
  gridcommands+="# When the job is finished, retrieve the stderr and stdout (and the rest of the sandbox)\n"
  gridcommands+="glite-ce-job-output $(tail -n1 %s)\n"%jobIDs
  gridcommands+="\n"
  gridcommands+="# Finally retrieve and unpack the greedycpp output, if the job was successful\n"
  gridcommands+="lcg-cp --vo virgo lfn:%s/%s file://%s\n"%(job.lfchome,job.remoteout.get_remote(),job.remoteout.get_local())
  gridcommands+="tar -zxvf %s"%job.remoteout.get_local()
  with open(os.path.join(job.rundir,"gridcommands"),"w") as f:
    f.write(gridcommands)
elif job.site == "stoomboot":
  subfile = os.path.join(job.rundir,"run.sh")
  commands=["export LD_LIBRARY_PATH=%s"%job.ldlibpath]
  commands.append("export PYTHONPATH=%s"%job.pythonpath)
  commands.append("export OMP_NUM_THREADS=%d"%job.numthreads)
  commands.append("%s %s"%(job.greedycpp,job.cfgfile))
  commands.append("%s %s %s/ %s"%(job.eim,os.path.join(job.rundir,"run_settings.cfg"),job.rundir,job.output_format))
  sub="echo \"#submit file for stoomboot\" > %s\n"%subfile
  for c in commands:
    sub+="echo \"%s\" >> %s\n"%(c,subfile)
  qsub="qsub"
  if job.time != 0:
    qsub += " -l walltime=%s"%(time.strftime('%H:%M:%S', time.gmtime(job.time*60)))
  if job.memory != 0:
    if job.memory >= 8000 and job.memory <= 48000:
      qsub += " -l mem=%dmb"%(job.memory)
    if job.memory > 48000:
      exit("Error: Requested memory is too large for stoomboot (max 48GB on gravwav queue)")
  if job.memory >= 8000:
    qsub += " -q gravwav"
    if job.time > 2880:
      exit("Error: Requested memory exceeds 8GB, so must use gravwav queue,\nbut requested time is too large for this queue (max 48hrs)")
  else:
    if job.time <= 240:
      qsub += " -q short"
    elif job.time > 240 and job.time <= 5760:
      qsub += " -q long"
    else:
      exit("Error: Requested time is too large for normal stoomboot queue (max 96hrs)")
  if job.numthreads > 1:
    print "WARNING: I do not think you can use more than a single thread on stoomboot"
    qsub += " -l nodes=1:ppn=%d"%job.numthreads
  qsub += " -e %s -o %s %s\n"%(subfile+"_err.txt",subfile+"_out.txt",subfile)
  sub+=qsub
  with open(os.path.join(job.rundir,"submit.sh"),"w") as f:
    f.write(sub)
else: #local
  commands="export LD_LIBRARY_PATH=%s\n"%job.ldlibpath
  commands+="export PYTHONPATH=%s\n"%job.pythonpath
  commands+="export OMP_NUM_THREADS=%d\n"%job.numthreads
  commands+="%s %s\n"%(job.greedycpp,job.cfgfile)
  commands+="%s %s %s/ %s"%(job.eim,os.path.join(job.rundir,"run_settings.cfg"),job.rundir,job.output_format)
  with open(os.path.join(job.rundir,"run.sh"),"w") as f:
    f.write(commands)





