#! python

__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

from optparse import OptionParser

from gwtoolbox.tiger import utils as tu
from gwtoolbox import genutils as gu


usage="""  collect_bayes.py [options]
  Collects the Bayes factors from finished subhypotheses
  The list of parameters provided will be turned into subypotheses

  The option --bayesfile-string can be 1 or 2 corresponding
  to files of the folllowing format:
    1: %s (from posterior_samples folder)
    2: %s (from engine folder)

  The resuting Bayes factors will be collected in columns
  with a header indicating the subhypotheses.
  There will also be a column for gps time and event number
"""%(tu._bayesfilename1,tu._bayesfilename2)



def main():
  parser=OptionParser(usage)
  parser.add_option("-r","--rundir",dest="rundir", default="./", action="store", type="string",help="The rundirectory containing the hypothesis folders like dchi2dchi3 (.)",metavar="RUNDIR")
  parser.add_option("-b","--bayesdir",dest="bayesdir", default="posterior_files", action="store", type="string",help="Directory to find Bayes files, like \"engine\" or \"posterior_files\". An empty string is also allowed: \"\" (posterior_files)",metavar="BAYESDIR")
  parser.add_option("-f","--bayesfile-string",dest="bfilestring", default=1, action="store", type=int,help="type of bayes file string, see help (1)",metavar="BSTRING")
  parser.add_option("-p","--parameters",dest="parameters",action="callback", callback=gu.callback_spaces,help="Space separated list of parameters (GR)",default="GR",metavar="PARAMS")
  parser.add_option("-o","--outfile",dest="outfile", default="bayes.dat", action="store", type="string",help="Output file path (./bayes.dat)",metavar="BAYESFILE")
  parser.add_option("-t","--timeslidesfile",dest="timeslidesfile", default="", action="store", type="string",help="If timeslides were used, these can be stored as well if the timeslidesfile is provided",metavar="TSFILE")
  parser.add_option("-v","--verbose",action="store_true", dest="verbose", default=False, help="Print a line for each event that is worked on to keep track of progress.")

  (opts,args)=parser.parse_args()

  rundir=opts.rundir
  bdir=opts.bayesdir
  bfiletype=opts.bfilestring
  parameters=opts.parameters
  outfile=opts.outfile
  timeslidesfile=opts.timeslidesfile
  verbose=opts.verbose

  proj=tu.tiger_project(parameters,rundir=rundir,bdir=bdir,bfiletype=bfiletype,
                        outfile=outfile,timeslidesfile=timeslidesfile,verbose=verbose)
  proj.write()



if __name__ == "__main__":
	# START THE MAIN FUNCTION IF RUN AS A SCRIPT. OTHERWISE, JUST LOADS THE CLASS
	# AND FUNCTION DEFINITIONS
	exit(main())

