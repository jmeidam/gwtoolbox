#! python

__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

from optparse import OptionParser
import numpy as np
from os import path,system


usage="""  roq_padbasis.py [options]
  This script is a quick tool to pad the TaylorF2 basis files so they go
  up to 512 (power of 2) instead of the 400Hz at which they should be
  cut off in the TIGER pipeline.

  The ROQ functionality in LALInference does not work for basis that do not
  have a length of a power of 2.

  example:
  roq_padbasis.py -l B_linear.npy -q B_quadratic.npy --out /outdir

  Will save a new padded B_linear.npy and B_quadratic.npy into /outdir

  If the files already exist in that folder, it will copy them
  and add "_bckp" to the names

"""


def main():

  parser=OptionParser(usage)
  parser.add_option("-o","--out",dest="output", action="store", default="./", type="string",help="output folder (./)")
  parser.add_option("-l","--linear",dest="linear", action="store", type="string",help="input linear basis")
  parser.add_option("-q","--quadratic",dest="quadratic",action="store", type="string", help="input quadratic basis")
  parser.add_option("-v","--verbose",dest="verbose",action="store_true",default=False, help="show some size details and such")
  parser.add_option("-s","--segment-length",dest="seglen",action="store", default=512, type=int, help="segment length of longest waveform in the training set (512)")
  parser.add_option("-f","--fmin",dest="fmin",action="store", default=20, type=int, help="minimum frequency (20)")
  parser.add_option("-F","--fmax",dest="fmax",action="store", default=512, type=int, help="maximum frequency that should be reached by padding (512)")

  (opts,args) = parser.parse_args()

  basis_quad=basis_linear=[]

  if not opts.quadratic and not opts.linear:
    exit("Error: must specify either a quadratic or a linear basis file or both.")
  if opts.quadratic:
    basis_quad=np.load(opts.quadratic)
  if opts.linear:
    basis_linear=np.load(opts.linear)

  verb=opts.verbose
  fmin=opts.fmin
  fmax=opts.fmax
  seglen=opts.seglen


  final_length=(fmax-fmin)*seglen + 1


  #LINEAR
  if len(basis_linear) > 0:
    print "padding linear basis..."
    current_length=len(basis_linear[0])
    dim_lin=len(basis_linear[:,0])
    Npad=final_length-current_length

    new_basis_lin=[]
    for i in range(dim_lin):
      new=np.lib.pad(basis_linear[i],(0,Npad), 'constant', constant_values=0)
      new_basis_lin.append(new)

    new_basis_lin=np.array(new_basis_lin)

  #QUADRATIC
  if len(basis_quad) > 0:
    print "padding quadratic basis..."
    current_length=len(basis_quad[0])
    dim_quad=len(basis_quad[:,0])
    Npad=final_length-current_length

    new_basis_quad=[]
    for i in range(dim_quad):
      new=np.lib.pad(basis_quad[i],(0,Npad), 'constant', constant_values=0)
      new_basis_quad.append(new)

    new_basis_quad=np.array(new_basis_quad)



  if verb:
    if len(basis_quad) >0: print "dim quad: %d"%dim_quad
    if len(basis_linear) >0: print "dim lin: %d"%dim_lin
    print "current length: %d"%current_length
    print "final length: %d"%final_length
    print "Npad: %d"%Npad
    print ""

    if len(basis_quad) >0: print "final length check quad: %d"%len(new_basis_quad[0])
    if len(basis_linear) >0: print "final length check lin: %d"%len(new_basis_lin[0])
    if len(basis_quad) >0: print "final dim check quad: %d"%len(new_basis_quad[:,0])
    if len(basis_linear) >0: print "final dim check lin: %d"%len(new_basis_lin[:,0])

  print "saving..."
  if len(basis_quad) >0:
    nameq=path.join(opts.output,"B_quadratic.npy")
    if path.isfile(nameq):
      system("cp %s %s"%(nameq,nameq+"_bckp"))
    np.save(nameq,new_basis_quad)
  if len(basis_linear) >0:
    namel=path.join(opts.output,"B_linear.npy")
    if path.isfile(namel):
      system("cp %s %s"%(namel,namel+"_bckp"))
    np.save(namel,new_basis_lin)



if __name__ == "__main__":
	# START THE MAIN FUNCTION IF RUN AS A SCRIPT. OTHERWISE, JUST LOADS THE CLASS
	# AND FUNCTION DEFINITIONS
	exit(main())


