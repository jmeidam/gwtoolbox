#! python

__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

import numpy as np
from optparse import OptionParser
import random
from os import path

from gwtoolbox.roqpipe import modelspecs as specs
from gwtoolbox.waveforms import utils as wfutils
import gwtoolbox.genutils as gu

usage="""  roq_generate_validation_file.py [options]
  generate one or a set of validation files
"""

def get_rand_params_PhenomP(param_intervals,iteration,kerrbound=1.0,nonGRparamname=""):

  q_max = param_intervals["q"][1]
  chi1_min = param_intervals["chi1L"][0]
  chi1_max = param_intervals["chi1L"][1]
  chi2_min = param_intervals["chi2L"][0]
  chi2_max = param_intervals["chi2L"][1]
  chip_min = param_intervals["chip"][0]
  chip_max = param_intervals["chip"][1]
  thetaJ_min = param_intervals["thetaJ"][0]
  thetaJ_max = param_intervals["thetaJ"][1]
  alpha0_min = param_intervals["alpha0"][0]
  alpha0_max = param_intervals["alpha0"][1]
  Mc_min = param_intervals["mchirp"][0]
  Mc_max = param_intervals["mchirp"][1]

  dX_min = 0.0
  dX_max = 0.0
  if nonGRparamname != "":
    dX_min = param_intervals[nonGRparamname][0]
    dX_max = param_intervals[nonGRparamname][1]

  eta_max = 0.25
  eta_min = wfutils.convert_q_to_eta(q_max)

  m1, m2 = 0.0, 0.0
  chibound = 0.0

  chi1Lbound = 1.0
  chi1L = 0.9
  nonGRparam = 0.0

  X = 0
  while m1 < 1.0 or m2 < 1.0 or chibound >= kerrbound or chi1L < chi1Lbound:

    random.seed()
    chi1L = gu.randparam(chi1_min, chi1_max)
    chi2L = gu.randparam(chi2_min, chi2_max)
    chip = gu.randparam(chip_min, chip_max)
    thetaJ = gu.randparam(thetaJ_min, thetaJ_max)
    alpha0 = gu.randparam(alpha0_min, alpha0_max)

    Mc = gu.randparam(Mc_min, Mc_max)
    eta = gu.randparam(eta_min, eta_max)

    m1, m2 = wfutils.convert_Mceta_to_m1m2(Mc, eta)
    chibound = np.sqrt(chip*chip+chi1L*chi1L)

    if nonGRparamname != "":
      nonGRparam = gu.randparam(dX_min, dX_max)
    else:
      nonGRparam = 0.0

    chi1Lbound = 0.4 - 7.0*eta

    X+=1
    #if X>5000:
    #  print "WARNING: no vars within bounds found after 5000 iterations"
    #  print "         point %d is bad"%iteration
    #  break

  #if m1 < 1.0 or m2 < 1.0 or chibound >= 1.0 or chi1L < chi1Lbound:
  #  print "values not good before returning"

  results={"mass1":m1,"mass2":m2,"chi1L":chi1L,"chi2L":chi2L,"chip":chip,
           "thetaJ":thetaJ,"alpha0":alpha0}
  if nonGRparamname != "":
    results[nonGRparamname]=nonGRparam

  return results


def get_rand_params_TaylorF2(param_intervals,iteration,nonGRparamname=""):

  Mc_min = param_intervals["mchirp"][0]
  Mc_max = param_intervals["mchirp"][1]
  spin1z_min = param_intervals["spin1z"][0]
  spin1z_max = param_intervals["spin1z"][1]
  spin2z_min = param_intervals["spin2z"][0]
  spin2z_max = param_intervals["spin2z"][1]

  dX_min = 0.0
  dX_max = 0.0
  if nonGRparamname != "":
    dX_min = param_intervals[nonGRparamname][0]
    dX_max = param_intervals[nonGRparamname][1]

  eta_max = 0.25

  random.seed()
  spin1z = gu.randparam(spin1z_min, spin1z_max)
  spin2z = gu.randparam(spin2z_min, spin2z_max)

  #For TayorF2 we only ensure that m1 and m2 are between mcomp_min and mcomp_max
  #and that m1>m2
  mcomp_max,m = wfutils.convert_Mceta_to_m1m2(Mc_max, eta_max)
  mcomp_min,m = wfutils.convert_Mceta_to_m1m2(Mc_min, eta_max)
  m1 = gu.randparam(mcomp_min, mcomp_max)
  m2 = gu.randparam(mcomp_min, m1)

  if nonGRparamname != "":
    nonGRparam = gu.randparam(dX_min, dX_max)
  else:
    nonGRparam = 0.0

  results={"mass1":m1,"mass2":m2,"spin1z":spin1z,"spin2z":spin2z}
  if nonGRparamname != "":
    results[nonGRparamname]=nonGRparam

  return results

def main():

  parser=OptionParser(usage)
  parser.add_option("-o","--out",dest="out", action="store", default="./", type="string",help="output folder to write files to")
  parser.add_option("-z","--model",dest="model", action="store", default="PhenomP", type="string",help="model name")
  parser.add_option("-s","--seglen",dest="seglen", action="store", default=8, type=int,help="maximum length of the signals")
  parser.add_option("-n","--n-points",dest="npoints",action="store", type=int, help="Number of random points to generate")
  parser.add_option("-N","--n-files",dest="nfiles",action="store", type=int, help="Split random points over n files")
  parser.add_option("-m","--mc-min",dest="mcmin",action="store", type=float, help="min chirp mass")
  parser.add_option("-M","--mc-max",dest="mcmax",action="store", type=float, help="max chirp mass")
  parser.add_option("-p","--nonGRparam",dest="param", action="store", default="", type="string",help="nonGRparameter to use")
  parser.add_option("-f","--filename-out",dest="fnameout", default="nothing", action="store", type="string",help="ouput filename (optional)")
  parser.add_option("-i","--file-index",dest="fileindex",action="store",default=-1,type=int, help="To parallelize process, choose --n-file=1 and pass index to this argument")
  parser.add_option("-k","--kerr-bound",dest="kerrbound",action="store",default=-1.0, type=float, help="allows to set a different value for the kerr bound (i.e. chi1L^2+chip^2 <= kerrbound, default=1.0)")
  parser.add_option("-x","--nonGRparam-min",dest="nongrmin", action="store", type=float, help="overwrite default min nonGRparam value")
  parser.add_option("-X","--nonGRparam-max",dest="nongrmax", action="store", type=float, help="overwrite default max nonGRparam value")

  (opts,args) = parser.parse_args()

  Npoints = opts.npoints
  Nfiles = opts.nfiles

  if "PhenomP" not in opts.model and "TaylorF2" not in opts.model:
    exit("Error: Currently roq_generate_validation_file.py only works for PhenomP and TaylorF2.")

  model = specs.roq_get_modeldefaults(opts.model,opts.seglen)
  param_intervals = {}


  #Handle the nonGR parameter if provided
  if opts.param != "":
    nonGRparam = model.get_parameter(opts.param)
    if opts.nongrmin:
      param_min = opts.nongrmin
    else:
      param_min = nonGRparam.min
    if opts.nongrmax:
      param_max = opts.nongrmax
    else:
      param_max = nonGRparam.max
    param_intervals[nonGRparam.name] = (param_min,param_max)
  else:
    nonGRparam = None

  #Set chirpmass values
  mchirp = model.get_parameter("mchirp")
  if opts.mcmin:
    param_min = opts.mcmin
  else:
    param_min = mchirp.min
  if opts.mcmax:
    param_max = opts.mcmax
  else:
    param_max = mchirp.max
  param_intervals["mchirp"] = (param_min,param_max)

  param_intervals["q"] = (model.get_parameter("q").min,model.get_parameter("q").max)

  #Add the additional model parameters and print out the ranges
  print "--------------------------------------------------------------"
  print "Generating validationfile with the following parameter limits:"
  print "  mchirp: (%.2f,%.2f)"%(param_intervals["mchirp"][0],param_intervals["mchirp"][1])
  print "  q:      (%.2f,%.2f)"%(param_intervals["q"][0],param_intervals["q"][1])
  print "  eta:    (%.2f,%.2f)"%(wfutils.convert_q_to_eta(param_intervals["q"][1]),0.25)
  print
  if "PhenomP" in opts.model:
    param_intervals["chi1L"] = (model.get_parameter("chi1L").min,model.get_parameter("chi1L").max)
    param_intervals["chi2L"] = (model.get_parameter("chi2L").min,model.get_parameter("chi2L").max)
    param_intervals["chip"] = (model.get_parameter("chip").min,model.get_parameter("chip").max)
    param_intervals["thetaJ"] = (model.get_parameter("thetaJ").min,model.get_parameter("thetaJ").max)
    param_intervals["alpha0"] = (model.get_parameter("alpha0").min,model.get_parameter("alpha0").max)
    print "  chi1L:  (%.2f,%.2f)"%(param_intervals["chi1L"][0],param_intervals["chi1L"][1])
    print "  chi2L:  (%.2f,%.2f)"%(param_intervals["chi2L"][0],param_intervals["chi2L"][1])
    print "  chip:   (%.2f,%.2f)"%(param_intervals["chip"][0],param_intervals["chip"][1])
    print "  thetaJ: (%.2f,%.2f)"%(param_intervals["thetaJ"][0],param_intervals["thetaJ"][1])
    print "  alpha0: (%.2f,%.2f)"%(param_intervals["alpha0"][0],param_intervals["alpha0"][1])
  else:
    param_intervals["spin1z"] = (model.get_parameter("spin1z").min,model.get_parameter("spin1z").max)
    param_intervals["spin2z"] = (model.get_parameter("spin2z").min,model.get_parameter("spin2z").max)
    print "  spin1z:  (%.2f,%.2f)"%(param_intervals["spin1z"][0],param_intervals["spin1z"][1])
    print "  spin2z:  (%.2f,%.2f)"%(param_intervals["spin2z"][0],param_intervals["spin2z"][1])

  if nonGRparam:
    print
    print "  %s: (%.2f,%.2f)"%(nonGRparam.name,param_intervals[nonGRparam.name][0],param_intervals[nonGRparam.name][1])
  print "--------------------------------------------------------------"




  #Set kerrbound (Only has effect for PhenomP)
  if opts.kerrbound == -1.0:
    kb = 1.0
  else:
    kb = opts.kerrbound
    print "Using bound sqrt(chi1L^2+chip^2) < %.3f"%kb

  # When the script is used to produce a single file
  # (to parallelize with external scripts), an index must be provided
  file_index=-1
  if Nfiles == 1:
    if opts.fileindex == -1:
      raise Exception("Must specify file-index if n-files = 1")
    file_index = opts.fileindex

  filelength = int(np.floor(Npoints/Nfiles))

  if opts.fnameout != "nothing":
    filename = path.join(opts.out,opts.fnameout)
  else:
    filename = path.join(opts.out,"validationfile_Mc%.1f-%.1f_%d.txt")

  print "Generating %d files with a total of %d points."%(Nfiles,Npoints)

  for iF in range(Nfiles):

    # determine how many points are to be generated
    if iF == Nfiles-1:
      # the last file may contain less points if the number of points
      # is not a multiple of the number of files
      pointstogenerate = Npoints - (Nfiles-1)*filelength
    else:
      pointstogenerate = filelength

    points = np.zeros((pointstogenerate,model.dim))
    print "Working on file %d/%d with %d points..."%(iF+1,Nfiles,pointstogenerate)

    # Generate the points
    for i in range(pointstogenerate):
      if "PhenomP" in opts.model:
        params = get_rand_params_PhenomP(param_intervals,i,kerrbound=kb,nonGRparamname=nonGRparam.name)
        points[i][0]=params["mass1"]
        points[i][1]=params["mass2"]
        points[i][2]=params["chi1L"]
        points[i][3]=params["chi2L"]
        points[i][4]=params["chip"]
        points[i][5]=params["thetaJ"]
        points[i][6]=params["alpha0"]
      else:
        params = get_rand_params_TaylorF2(param_intervals,i,nonGRparamname=nonGRparam.name)
        points[i][0]=params["mass1"]
        points[i][1]=params["mass2"]
        points[i][2]=params["spin1z"]
        points[i][3]=params["spin2z"]
      if nonGRparam != None:
        points[i][nonGRparam.index]=params[nonGRparam.name]

    if opts.fnameout != "nothing":
      wf = filename
    else:
      if file_index == -1:
        wf = filename%(param_intervals["mchirp"][0],param_intervals["mchirp"][1],iF)
      else:
        wf = filename%(param_intervals["mchirp"][0],param_intervals["mchirp"][1],file_index)
    np.savetxt(wf,points)
    print "writing file %s"%wf









if __name__ == "__main__":
	# START THE MAIN FUNCTION IF RUN AS A SCRIPT. OTHERWISE, JUST LOADS THE CLASS
	# AND FUNCTION DEFINITIONS
	exit(main())
