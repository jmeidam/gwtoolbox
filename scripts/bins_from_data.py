#! python

__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

from optparse import OptionParser
import numpy as np
from gwtoolbox.waveforms import utils as wfutils
from os import path

usage="""  bins_from_data.py [options]
  read in a large file and return the binned values.
  Returned file has two columns: counts and bins.
  The last element in counts is a dummy to make both columns the same length
  The bins are always N+1 in length

  A histogram plot can be generated from this using the standard hist function
  in the following way (log example):
      center = (bins[:-1] + bins[1:]) / 2
      pylab.hist(log10(center), bins=log10(bins), weights=counts )

  The script can create a combined histogram of multiple datafiles, provided
  they have the same columns. To do this, specify the input filename with
  the string ITER that will be replaced with an iteration number between
  min and max given with --combine min,max. For example:
    --in /path/to/data_ITER.dat --combine 1,100
"""

Nbinsdefault=40

def hist_fromfile(fin,counts,index,bins,calc_eta=False,calc_mchirp=False,square=False,lengthoffile=0,reportfrac=0):
  if path.isfile(fin):
    # only try to hist a file that exists
    with open(fin,"r") as infile:
      i=0
      for line in infile:
        data=line.strip().split()
        if calc_eta:
          m1,m2 = float(data[index]),float(data[index+1])
          value = wfutils.convert_m1m2_to_nu(m1,m2)
        elif calc_mchirp:
          m1,m2 = float(data[index]),float(data[index+1])
          value = wfutils.convert_m1m2_to_Mc(m1,m2)
        else:
          value = float(data[index])
        if square:
          value *= value
        htemp, jnk = np.histogram(value, bins)
        counts += htemp
        #print counts
        if lengthoffile > 0:
          if (i%reportfrac) == 0:
            print "%.1f pc done"%(100.0*i/float(lengthoffile))
        i+=1
  return counts

def callback_comma(option, opt, value, parser):
  setattr(parser.values, option.dest, value.split(','))

def main():

  parser=OptionParser(usage)
  parser.add_option("-o","--out",dest="output", action="store", default="./bins.txt", type="string",help="output file")
  parser.add_option("-i","--in",dest="input", action="store", type="string",help="input file")
  parser.add_option("-n","--n-bins",dest="nbins",action="store", type=int, help="Number of bins to use (%d)"%Nbinsdefault)
  parser.add_option("-e","--bin-min",dest="binmin",action="store", default=-1.0, type=float, help="lower limit of histogram range, power of 10 if --log is set.")
  parser.add_option("-E","--bin-max",dest="binmax",action="store", default=1.0, type=float, help="upper limit of histogram range, power of 10 if --log is set.")
  parser.add_option("-L","--file-length",dest="length",action="store", type=int, help="if length of input file is provided, progress is printed in percentage")
  parser.add_option("-l","--log",dest="uselog",action="store_true",default=False, help="bin logarithmicallly")
  parser.add_option("-s","--square",dest="square",action="store_true",default=False, help="square the value. Use for eim and greedy errors")
  parser.add_option("-c","--column",dest="column",action="store", type=int, default=-1, help="index of column to histogram")
  parser.add_option("-x","--eta",dest="eta",action="store_true",default=False, help="calculate eta from the two mass columns. NOTE: --column must be that of mass1, script assumes the next column is that of mass2")
  parser.add_option("-y","--mchirp",dest="mchirp",action="store_true",default=False, help="calculate chirp mass from the two mass columns. NOTE: --column must be that of mass1, script assumes the next column is that of mass2")
  parser.add_option("-C","--combine",dest="combine",action="callback",type='string',callback=callback_comma, help="combine multiple files into one histogram. Argument must be \"min,max\" Input must have name as filename_ITER_something.txt for this to work. ITER will be replaced with numbers between min,max")

  (opts,args) = parser.parse_args()

  uselog = opts.uselog
  square = opts.square
  calc_eta = opts.eta
  calc_mchirp = opts.mchirp

  if calc_eta and calc_mchirp:
    print "error: must set either --eta or --mchirp, not both"
    return 1

  combine_min = -1
  combine_max = -1
  if opts.combine:
    if len(opts.combine)!=2:
      print "error: --combine argument must have 2 values e.g. \"0,99\""
      return 1
    else:
      combine_min = int(opts.combine[0])
      combine_max = int(opts.combine[1])

  if opts.nbins:
    Nbins = opts.nbins
  else:
    Nbins = Nbinsdefault

  binmin = opts.binmin
  binmax = opts.binmax

  if opts.column!=-1:
    index = opts.column
  else:
    print "error: must provide column index"
    return 1

  if opts.input:
    fin = opts.input
  else:
    print "error: Must provide input file with --in"
    return 1

  fout = opts.output

  # the lenth of the file to bin is optional. It is only used if
  # progress needs to be printed while running.
  if opts.length:
    lengthoffile=opts.length
  else:
    lengthoffile=0
  reportfraction=lengthoffile/100.0 #print at every percent

  print binmin,binmax,Nbins
  if uselog:
    bins = np.logspace(int(binmin), int(binmax), Nbins)
  else:
    bins = np.linspace(binmin, binmax, Nbins)

  print "input filename %s"%fin
  if lengthoffile > 0:
    print "length is %d"%lengthoffile

  # the counts array will be updated at each call of hist_fromfile
  counts = np.zeros(Nbins-1,dtype=int)
  if combine_min != -1:
    Nfilestoread = (combine_max+1)-combine_min
    print "binning %d files"%Nfilestoread
    for iter in range(combine_min,combine_max+1):
      curfile = fin.replace("ITER",str(iter))
      print "reading %s"%curfile
      counts = hist_fromfile(curfile,counts,index,bins,calc_eta=calc_eta,calc_mchirp=calc_mchirp,square=square,lengthoffile=lengthoffile,reportfrac=reportfraction)
  else:
    print "reading %s"%fin
    counts = hist_fromfile(fin,counts,index,bins,calc_eta=calc_eta,calc_mchirp=calc_mchirp,square=square,lengthoffile=lengthoffile,reportfrac=reportfraction)

  print "writing to %s"%fout
  print counts,bins
  with open(fout,"w") as f:
    f.write("#counts bins\n")
    for i in range(len(bins)-1):
      f.write("%d %.9e\n"%(counts[i],bins[i]))

    # NOTE: Add upper bin edge, the 0 counts is not to be used in plotting a histogram
    #       these are just to make the columns have equal length
    f.write("0 %.9e"%bins[-1])










if __name__ == "__main__":
	# START THE MAIN FUNCTION IF RUN AS A SCRIPT. OTHERWISE, JUST LOADS THE CLASS
	# AND FUNCTION DEFINITIONS
	exit(main())
