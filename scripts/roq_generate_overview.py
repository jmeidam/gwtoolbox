__author__ = "Jeroen Meidam"
__copyright__ = "Copyright 2016"
__email__ = "jmeidam@nikhef.nl"

import numpy as np

import gwtoolbox.genutils as gu
import gwtoolbox.waveforms.utils as wfutils

from gwtoolbox.roqpipe import modelspecs
from gwtoolbox.visualization import plotting as pl
from gwtoolbox.visualization import gw_html as html
import gwtoolbox.toolboxtypes.toolboxtypes as tbt

from uuid import uuid4

from os import path

"""
From basedir, this script requires the following structure:
basedir/param/seglen/
  validation#_bins_eim.txt
  validation#_bins_err.txt
  quadvalidation#_bins_err.txt
  quadvalidation#_bins_err.txt
  run0
  run1
  ..
  runN/
    GreedyPoints.txt
  quadrun0
  quadrun1
  ..
  quadrunM/
    GreedyPoints.txt
  pe_results/
    roq.hdf5
    noroq.hdf5 (optional)

The last run and quadrun are assumed to be the final basis for the basis plots.

The output dir will contain the html page, style files, java script and
a figures folder containing all the figures used in the html page.
"""

settings={"parameters":["dchi6l"],
          "basedir":"/location/to/basedir/",
          "sections":["errors","basispoints","pe_results"],
          "output":"/location/to/basedir/overview",
          "modelname":"PhenomP",
          "pe_params":["param","mchirp"],
          "seglens":[4,8,16],
          "pointsets":[["eta","chi1L","chip"],["chip","chi1L"],["","chi1L"]],
          "replot":True} #replot figures even if they are already present

all_seglens=[4,8,16,32,64,128]

f_ranges={"4s":(20,1024),
         "8s":(20,1024),
         "16s":(20,2048),
         "32s":(20,2048),
         "64s":(20,2048),
         "128s":(20,4096)}

mchirp_ranges={'4s':(12.3,23.0),
               '8s':(7.9,14.8),
               '16s':(5.2,9.5),
               '32s':(3.4,6.2),
               '64s':(2.2,4.2),
               '128s':(1.4,2.6)}

param_html_tex={"dchi0":"\(\delta\hat{\\varphi}_0\)",
"dchi1":"\(\delta\hat{\\varphi}_1\)",
"dchi2":"\(\delta\hat{\\varphi}_2\)",
"dchi3":"\(\delta\hat{\\varphi}_3\)",
"dchi4":"\(\delta\hat{\\varphi}_4\)",
"dchi5l":"\(\delta\hat{\\varphi}_5^{(l)}\)",
"dchi6":"\(\delta\hat{\\varphi}_6\)",
"dchi6l":"\(\delta\hat{\\varphi}_6^{(l)}\)",
"dchi7":"\(\delta\hat{\\varphi}_7\)",
"dalpha2":"\(\delta\hat{\\alpha}_2\)",
"dalpha3":"\(\delta\hat{\\alpha}_3\)",
"dalpha4":"\(\delta\hat{\\alpha}_4\)",
"dbeta2":"\(\delta\hat{\\beta}_2\)",
"dbeta3":"\(\delta\hat{\\beta}_3\)"}


class section(object):

  def __init__(self,param,seglen,sectiontype,sectionid,sectiontitle):

    self.seglen=seglen
    self.param=param
    self.type=sectiontype
    self.plots_linear=[]
    self.plots_quadratic=[]
    self.id=sectionid
    self.title=sectiontitle

  def add_plot(self,filename,part):
    if part not in ["linear","quadratic"]:
      print "Error: part must be linear or quadratic"
    else:
      if part == "linear":
        self.plots_linear.append(filename)
      else:
        self.plots_quadratic.append(filename)



def plot_histograms(inputfiles,labels,figurename,xlabel=r"$x$",ylabel=r"${\rm counts}$",binscol=1,frombins=True,datacols=[],nbins=40,logx=False,logy=False,normalize=0,histtype='step',colors=[],ylim=[],xlim=[],verbose=False):

  fig = pl.custom_figure()

  if datacols==[]:
    if verbose: print "No datacols provided: using default 0 (counts/data) and 1 (bins) for all files"
  else:
    if len(datacols) != len(inputfiles):
      print "Error: Can't figure out what columns to use, len(datacols)!=len(inputfiles)"
      return
    if len(colors)>0 and len(colors)!=len(inputfiles):
      print "Error: len(colors)!=len(inputfiles)"
      return


  for i in range(len(inputfiles)):

    data = np.genfromtxt(inputfiles[i])
    if frombins:
      if len(datacols)==0:
        col = 0
      else:
        col = datacols[i]
      histdata=data[:,col][:-1]
      bins=data[:,binscol]
    else:
      if len(datacols)==0:
        col = 0
      else:
        col = datacols[i]
      histdata=data[:,col]
      bins=nbins
    if len(colors) > 0:
      color = colors[i]
    else:
      color = None

    if verbose: print len(bins),len(histdata)
    fig.add_hist(histdata,bins=bins,binned_data=frombins,logx=logx,label=labels[i],histtype=histtype,color=color,extra={'lw':1.5},normalize=normalize,logy=logy)

  fig.add_legend()
  fig.set_xlabel(xlabel)
  fig.set_ylabel(ylabel)

  if len(ylim) == 2:
    fig.set_ylimit(ymin=ylim[0],ymax=ylim[1])
  if len(xlim) == 2:
    fig.set_xlimit(xmin=xlim[0],xmax=xlim[1])

  gu.EnsureDir(figurename, stripfilename=True)
  if verbose: print figurename
  fig.save_fig(filename=figurename)



def create_errorplot(datadir,figpath,part,ylim=[0.1,1e8]):

  if part not in ["linear","quadratic"]:
    print "Error: part must be linear or quadratic"
    return None

  if part == "linear":
    file_eim=path.join(datadir,"validation%s_bins_eim.txt")
    file_err=path.join(datadir,"validation%s_bins_err.txt")
  else:
    file_eim=path.join(datadir,"quadvalidation%s_bins_eim.txt")
    file_err=path.join(datadir,"quadvalidation%s_bins_err.txt")

  eim=[]
  err=[]
  labels=[]
  nval=1
  while path.isfile(file_eim%nval) and path.isfile(file_err%nval):
    print "  %s error files found for validation %d"%(part,nval)
    eim.append(file_eim%nval)
    err.append(file_err%nval)
    labels.append(r"${\rm validation\,%d}$"%nval)
    nval+=1

  plot_histograms(eim,labels,figpath,ylim=ylim,logx=True,logy=True,xlabel=r"$\rm \log_{10}{\rm \,\,Interpolation\,\,Error}$",ylabel=r"${\rm Counts}$")





def create_3dscatter(GreedyPoints,paramnames,figpath,part,xlim={},ylim={},zlim={},plotlinefunction=None,legend=False,colormap='gnuplot2'):
  """GreedyPoints is a dictionary where the keys must contain the three parameters
  The values are toolboxtypes.parameter objects

  """
  fig = pl.custom_figure()
  xparam = GreedyPoints[paramnames[0]]
  yparam = GreedyPoints[paramnames[1]]
  zparam = GreedyPoints[paramnames[2]]
  xlimit = [xparam.min-0.01*(xparam.max-xparam.min),xparam.max+0.01*(xparam.max-xparam.min)]
  ylimit = [yparam.min-0.01*(yparam.max-yparam.min),yparam.max+0.01*(yparam.max-yparam.min)]
  zlimit = [zparam.min,zparam.max]
  if xlim.has_key("min"): xlimit[0]=xlim["min"]
  if xlim.has_key("max"): xlimit[1]=xlim["max"]
  if ylim.has_key("min"): ylimit[0]=ylim["min"]
  if ylim.has_key("max"): ylimit[1]=ylim["max"]
  if zlim.has_key("min"): zlimit[0]=zlim["min"]
  if zlim.has_key("max"): zlimit[1]=zlim["max"]

  fig.add_scatter(xparam.vals,yparam.vals,xlabel=xparam.texl,ylabel=yparam.texl,label=None,marker='o',markerfillstyle='none',facecolors=zparam.vals,color=None,alpha=1.0,extra={"cmap":'gnuplot2'})
  fig.set_xlimit(xmin=xlimit[0],xmax=xlimit[1])
  fig.set_ylimit(ymin=ylimit[0],ymax=ylimit[1])

  fig.add_colorbar(zlimit[0],zlimit[1],label=zparam.texl,labelpad=1,cmap='gnuplot2')

  if plotlinefunction:
    xline=plotlinefunction.get_x()
    yline=plotlinefunction.get_y()
    fig.add_xyplot(xline,yline,style=plotlinefunction.style,color=plotlinefunction.color,label=plotlinefunction.label)
  if legend: fig.add_legend()

  fig.save_fig(filename=figpath)



def create_2dscatter(GreedyPoints,paramnames,figpath,part,xlim={},ylim={},plotlinefunction=None,legend=False):
  """GreedyPoints is a dictionary where the keys must contain the three parameters
  The values are toolboxtypes.parameter objects

  """
  fig = pl.custom_figure()
  xparam = GreedyPoints[paramnames[0]]
  yparam = GreedyPoints[paramnames[1]]
  xlimit = [xparam.min-0.01*(xparam.max-xparam.min),xparam.max+0.01*(xparam.max-xparam.min)]
  ylimit = [yparam.min-0.01*(yparam.max-yparam.min),yparam.max+0.01*(yparam.max-yparam.min)]
  if xlim.has_key("min"): xlimit[0]=xlim["min"]
  if xlim.has_key("max"): xlimit[1]=xlim["max"]
  if ylim.has_key("min"): ylimit[0]=ylim["min"]
  if ylim.has_key("max"): ylimit[1]=ylim["max"]

  fig.add_scatter(xparam.vals,yparam.vals,xlabel=xparam.texl,ylabel=yparam.texl,label=None,marker='o',markerfillstyle='none',color=None,alpha=1.0)
  fig.set_xlimit(xmin=xlimit[0],xmax=xlimit[1])
  fig.set_ylimit(ymin=ylimit[0],ymax=ylimit[1])

  if plotlinefunction:
    xline=plotlinefunction.get_x()
    yline=plotlinefunction.get_y()
    fig.add_xyplot(xline,yline,style=plotlinefunction.style,color=plotlinefunction.color,label=plotlinefunction.label)
  if legend: fig.add_legend()

  fig.save_fig(filename=figpath)


def create_peplot(pe_dir,thisparam,xlabel,figpath,xlim={}):

  kde_min=None
  kde_max=None
  if xlim.has_key("min"): kde_min=xlim["min"]
  if xlim.has_key("max"): kde_max=xlim["max"]

  roqfile = path.join(pe_dir,"roq.hdf5")
  noroqfile = path.join(pe_dir,"noroq.hdf5")

  plot_roq=False
  plot_noroq=False
  if path.isfile(roqfile):
    plot_roq=True
  if path.isfile(noroqfile) and plot_roq:
    plot_noroq=True

  if not plot_roq:
    return
  else:
    post_roq=tbt.posterior("roq",thisparam,roqfile,kde_min=kde_min,kde_max=kde_max,HDF5=True)
    fig = pl.custom_figure()
    fig.add_xyplot(post_roq.xaxis,post_roq.get_kde(),label=r"$\rm ROQ$")
    if plot_noroq:
      post_noroq=tbt.posterior("noroq",thisparam,noroqfile,kde_min=kde_min,kde_max=kde_max,HDF5=True)
      fig.add_xyplot(post_noroq.xaxis,post_noroq.get_kde(),label=r"$\rm No\,\,ROQ$")
    fig.set_xlabel(xlabel)
    fig.set_ylabel(r"${\rm Probability\,\,Density}$")
    fig.add_legend()
    fig.save_fig(filename=figpath)


def add_plotssection(htmlpage,section_obj,tableid="table"):

  htmlpage.div(id=section_obj.id, class_="ppsection")
  htmlpage.table(align="center",border="0",cellpadding="5",cellspacing="0")
  linkID=str(uuid4()).split('-')[0]
  collapseID=tableid+"_collapse_%s"%linkID
  html.write_collapsible_sectionheader(htmlpage,section_obj.title,collapseID,linkID,textsize=12)
  htmlpage.tr()

  htmlpage.td(colspan="3")
  htmlpage.table(id=collapseID, style="display: none")

  htmlpage.tr()
  htmlpage.th("Linear part")
  htmlpage.th("Quadratic part")
  htmlpage.tr.close()

  for i in range(len(section_obj.plots_linear)):
    plot_lin = section_obj.plots_linear[i]
    plot_quad = section_obj.plots_quadratic[i]
    htmlpage.tr()
    htmlpage.td(width="49%")
    htmlpage.img(src=path.join("figures",path.basename(plot_lin)),style="display:block;",width="100%")
    htmlpage.td.close()
    htmlpage.td(width="49%")
    htmlpage.img(src=path.join("figures",path.basename(plot_quad)),style="display:block;",width="100%")
    htmlpage.td.close()
    htmlpage.tr.close()

  htmlpage.table.close()
  htmlpage.td.close()

  htmlpage.tr.close()
  htmlpage.table.close()
  htmlpage.div.close()



def add_pesection(htmlpage,section_obj,tableid="table"):

  htmlpage.div(id=section_obj.id, class_="ppsection")
  htmlpage.table(align="center",border="0",cellpadding="5",cellspacing="0")
  linkID=str(uuid4()).split('-')[0]
  collapseID=tableid+"_collapse_%s"%linkID
  html.write_collapsible_sectionheader(htmlpage,section_obj.title,collapseID,linkID,textsize=12)
  htmlpage.tr()

  htmlpage.td(colspan="3")
  htmlpage.table(id=collapseID, style="display: none")

  htmlpage.tr()
  htmlpage.th("Posteriors")
  htmlpage.tr.close()

  for i in range(len(section_obj.plots_linear)):
    plot_lin = section_obj.plots_linear[i]
    htmlpage.tr()
    htmlpage.td(width="49%",style="text-align: center;width: 49%")
    htmlpage.img(src=path.join("figures",path.basename(plot_lin)),style="display:block;",width="50%",align="center")
    htmlpage.td.close()
    htmlpage.tr.close()

  htmlpage.table.close()
  htmlpage.td.close()

  htmlpage.tr.close()
  htmlpage.table.close()
  htmlpage.div.close()




def spinbound(x_range,params={}):
  if params.has_key("bound"):
    bound=params["bound"]
  else:
    bound=0.98
  y=[]
  for x in x_range:
    y.append(np.sqrt(bound-x*x))
  return np.array(y)


def etabound(x_range,params={}):
  y=[]
  for x in x_range:
    y.append(0.4-7.0*x)
  return np.array(y)


class function_object():
  def __init__(self,functionpointer,label,xmin,xmax,N=100,log=False,style=None,color=None,params={}):
    self.f=functionpointer
    self.label=label
    self.style=style
    self.color=color
    self.params=params
    self.log=log
    self.xmin=xmin
    self.xmax=xmax
    self.N=N

  def get_x(self):
    if self.log:
      return np.logspace(self.xmin,self.xmax,self.N)
    else:
      return np.linspace(self.xmin,self.xmax,self.N)


  def get_y(self):
    x = self.get_x()
    return self.f(x,self.params)



class pointsplot():

  def __init__(self,xname,yname,zname="",function_object=None):
    self.xname=xname
    self.yname=yname
    self.zname=zname
    self.function=function_object





def search_for_points(search,data):
  """e.g. search_for_points({"chip":(0.49,0.51),"chi1L":(0.87,0.9)},data)"""
  N = len(data['mass1'].data)
  operators = np.zeros(N,dtype=int)
  for S in search.keys():
    paramdata = data[S].data
    i=0
    for paramval in paramdata:
      if paramval >= search[S][0] and paramval <= search[S][1]:
        operators[i]+=1
      i+=1

  found = []
  for n in range(N):
    if operators[n] == len(search.keys()):
      found.append(n)

  return found



def init_data(param,settings):


  ## Create summary data for the table
  summarydata=[]
  AllGreedyPoints={}
  header = ["seglen bin","\(f\)-range [\({\\rm Hz}\)]","\(\mathcal{M}_c\)-range [\(M_{\odot}\)]","Waveform length at Nyquist","Basis size linear","Basis size quadratic","Theoretical speedup"]
  summarydata.append(header)
  for seglen in all_seglens:
    GreedyPoints_lin={}
    GreedyPoints_quad={}
    model = modelspecs.roq_get_modeldefaults(settings["modelname"],seglen)
    curdir = path.join(settings["basedir"],param,"%ds"%seglen)
    segstring="%ds"%seglen
    frange="%d - %d"%(f_ranges[segstring][0],f_ranges[segstring][1])
    mcrange="%.1f - %.1f"%(model.mchirpmin,model.mchirpmax)

    bestrun_lin=0
    bestrun_quad=0
    filename_lin=""
    filename_quad=""
    while path.isfile(path.join(curdir,"run%d"%bestrun_lin,"GreedyPoints.txt")):
      filename_lin=path.join(curdir,"run%d"%bestrun_lin,"GreedyPoints.txt")
      bestrun_lin+=1
    while path.isfile(path.join(curdir,"quadrun%d"%bestrun_quad,"GreedyPoints.txt")):
      filename_quad=path.join(curdir,"quadrun%d"%bestrun_quad,"GreedyPoints.txt")
      bestrun_quad+=1
    len_lin=gu.file_len(filename_lin)
    len_quad=gu.file_len(filename_quad)

    #store greedypoints as parameter objects in the GreedyPoints dictionary
    if len_lin>0:
      data_lin = np.genfromtxt(filename_lin)
      for pname in ["mass1","mass2","chi1L","chi2L","chip","alpha0","thetaJ",param]:
        p = model.get_parameter(pname)
        GreedyPoints_lin[pname]=tbt.parameter(pname,data_lin[:,p.index],pmin=p.min,pmax=p.max,texlabel=p.latex)
      m1 = GreedyPoints_lin["mass1"]
      m2 = GreedyPoints_lin["mass2"]
      GreedyPoints_lin["mchirp"] = wfutils.convert_m1m2_to_Mc(m1,m2)
      GreedyPoints_lin["mchirp"].texl = r"$\mathcal{M}_c$"
      GreedyPoints_lin["mchirp"].min = model.get_parameter("mchirp").min
      GreedyPoints_lin["mchirp"].max = model.get_parameter("mchirp").max
      GreedyPoints_lin["eta"] = wfutils.convert_m1m2_to_nu(m1,m2)
      GreedyPoints_lin["eta"].texl = r"$\eta$"
      GreedyPoints_lin["eta"].min = model.get_parameter("eta").min
      GreedyPoints_lin["eta"].max = model.get_parameter("eta").max
      GreedyPoints_lin["q"] = wfutils.convert_m1m2_to_q(m1,m2)
      GreedyPoints_lin["q"].texl = r"$q$"
      GreedyPoints_lin["q"].min = model.get_parameter("q").min
      GreedyPoints_lin["q"].max = model.get_parameter("q").max
    if len_quad>0:
      data_quad = np.genfromtxt(filename_quad)
      for pname in ["mass1","mass2","chi1L","chi2L","chip","alpha0","thetaJ",param]:
        p = model.get_parameter(pname)
        GreedyPoints_quad[pname]=tbt.parameter(pname,data_quad[:,p.index],pmin=p.min,pmax=p.max,texlabel=p.latex)
      m1 = GreedyPoints_quad["mass1"]
      m2 = GreedyPoints_quad["mass2"]
      GreedyPoints_quad["mchirp"] = wfutils.convert_m1m2_to_Mc(m1,m2)
      GreedyPoints_quad["mchirp"].texl = r"$\mathcal{M}_c$"
      GreedyPoints_quad["mchirp"].min = model.get_parameter("mchirp").min
      GreedyPoints_quad["mchirp"].max = model.get_parameter("mchirp").max
      GreedyPoints_quad["eta"] = wfutils.convert_m1m2_to_nu(m1,m2)
      GreedyPoints_quad["eta"].texl = r"$\eta$"
      GreedyPoints_quad["eta"].min = model.get_parameter("eta").min
      GreedyPoints_quad["eta"].max = model.get_parameter("eta").max
      GreedyPoints_quad["q"] = wfutils.convert_m1m2_to_q(m1,m2)
      GreedyPoints_quad["q"].texl = r"$q$"
      GreedyPoints_quad["q"].min = model.get_parameter("q").min
      GreedyPoints_quad["q"].max = model.get_parameter("q").max

    if len_lin == 0:
      N_L = "?"
    else:
      N_L = len_lin
    if len_quad == 0:
      N_Q = "?"
    else:
      N_Q = len_quad

    N_nyq = (f_ranges[segstring][1]-f_ranges[segstring][0])*seglen + 1
    if len_lin+len_quad != 0:
      speedup = float(N_nyq)/float(len_lin+len_quad)
    else:
      speedup = "?"

    summarydata.append([segstring,frange,mcrange,N_nyq,N_L,N_Q,speedup])

    if len_lin > 0 and len_quad > 0:
      AllGreedyPoints["%s_%ds_linear"%(param,seglen)]=GreedyPoints_lin
      AllGreedyPoints["%s_%ds_quadratic"%(param,seglen)]=GreedyPoints_quad


  all_sections = {}
  for seglen in settings["seglens"]:

    curdir = path.join(settings["basedir"],param,"%ds"%seglen)

    run0 = path.join(curdir,"run0","GreedyPoints.txt")

    seglen_sections = []

    if "errors" in settings["sections"] and path.isfile(run0):
      print "working on %s - %ds : errors section"%(param,seglen)
      section_errors = section(param,seglen,"errors","%ds_errors"%seglen,"%ds - Interpolation errors"%seglen)

      #create the error plots
      for part in ["linear","quadratic"]:
        figpath = path.join(settings["output"],"figures","%s_%ds_errors_%s.png"%(param,seglen,part))
        if not path.isfile(figpath) or settings["replot"]:
          create_errorplot(curdir,figpath,part)
          print "    created figure %s"%path.basename(figpath)
        else:
          print "    using figure %s"%path.basename(figpath)
        section_errors.add_plot(figpath,part)
      seglen_sections.append(section_errors)


    if "pe_results" in settings["sections"]:
      section_pe = section(param,seglen,"pe_results","%ds_pe_results"%seglen,"%ds - PE Results"%seglen)
      if AllGreedyPoints.has_key("%s_%ds_%s"%(param,seglen,"linear")): #only add this section for seglen if greedypoints exist
        print "working on %s - %ds : PE results section"%(param,seglen)
        for pe_param in settings["pe_params"]:
          thisparam = param
          if pe_param != "param":
            thisparam = pe_param
          figpath = path.join(settings["output"],"figures","%s_%ds_pe_result_%s.png"%(param,seglen,thisparam))
          if not path.isfile(figpath) or settings["replot"]:
            pe_dir = path.join(curdir,"pe_results")
            p=model.get_parameter(thisparam)
            create_peplot(pe_dir,thisparam,p.latex,figpath,xlim={})
            if path.isfile(figpath):
              section_pe.add_plot(figpath,"linear")
              print "    created figure %s"%path.basename(figpath)
          else:
            print "    using figure %s"%path.basename(figpath)
            section_pe.add_plot(figpath,"linear")
      seglen_sections.append(section_pe)


    if "basispoints" in settings["sections"] and path.isfile(run0):
      section_basispoints = section(param,seglen,"basispoints","%ds_basispoints"%seglen,"%ds - Basis Points"%seglen)
      for part in ["linear","quadratic"]:
        if AllGreedyPoints.has_key("%s_%ds_%s"%(param,seglen,part)): #only add this section for seglen if greedypoints exist
          print "working on %s - %ds : basispoints section"%(param,seglen)
          for pointset_fromsettings in settings["pointsets"]:
            pointset=[]
            for i in range(len(pointset_fromsettings)):
              if pointset_fromsettings[i] == "":
                pointset.append(param)
              else:
                pointset.append(pointset_fromsettings[i])
            print "  pointset:",pointset
            figpath = path.join(settings["output"],"figures","%s_%ds_GreedyPoints_%s_%s.png"%(param,seglen,part,"-".join(pointset)))
            if not path.isfile(figpath) or settings["replot"]:
              pltln = None
              leg=False
              if pointset[0]=="eta" and pointset[1]=="chi1L":
                pltln = function_object(etabound,r'$\chi_1 = 0.4-7\eta$',0.01,0.25,style="--")
                leg=True
              if (pointset[0]=="chip" and pointset[1]=="chi1L") or (pointset[0]=="chi1L" and pointset[1]=="chip"):
                pltln = function_object(spinbound,r'$\chi_{1L}^2 + \chi_p^2 = 0.98$',0.0,1.0,style="--")
                leg=True
              if len(pointset) == 3:
                create_3dscatter(AllGreedyPoints["%s_%ds_%s"%(param,seglen,part)],pointset,figpath,part,xlim={},ylim={},zlim={},plotlinefunction=pltln,legend=leg,colormap='gnuplot2')
                print "    created figure %s"%path.basename(figpath)
              else:
                create_2dscatter(AllGreedyPoints["%s_%ds_%s"%(param,seglen,part)],pointset,figpath,part,xlim={},ylim={},plotlinefunction=pltln,legend=leg)
                print "    created figure %s"%path.basename(figpath)
            else:
              print "    using figure %s"%path.basename(figpath)
            section_basispoints.add_plot(figpath,part)
      seglen_sections.append(section_basispoints)


    all_sections["%ds"%seglen]=seglen_sections

  return all_sections,summarydata,AllGreedyPoints



gu.EnsureDir(path.join(settings["output"],"figures"))

for param in settings["parameters"]:

  all_sections,summary_data,all_greedypoints = init_data(param,settings)

  page = html.gwhtmlpage(path.join(settings["output"],"%s_roq_overview.html"%param),title="roq overview %s"%param)

  page.h1("ROQ overview for %s"%param_html_tex[param],align="center")
  page.div(class_="ppsection",id="summary")
  page.h2("Summary")
  page.table()
  page.td(id="maintable")
  page.div(id="maintable_div",style="&quot;color:#000000&quot;")
  page.add_defaultdatatable("numbers","numbers",summary_data)
  page.div.close()
  page.td.close()
  page.td(id="toc")
  page.div(id="toc_div",style="&quot;color:#000000&quot;")
  for seglen in settings["seglens"]:
    if all_sections.has_key("%ds"%seglen):
      for this_section in all_sections["%ds"%seglen]:
        page.a(this_section.title,href="#%s"%this_section.id)
        page.br()
  page.div.close()
  page.td.close()
  page.table.close()
  page.div.close()

  for seglen in settings["seglens"]:
    if all_greedypoints.has_key("%s_%ds_linear"%(param,seglen)):
      page.h2("Seglen bin: %ds"%seglen,align="center")
      for this_section in all_sections["%ds"%seglen]:
        if this_section.type == "pe_results":
          add_pesection(page,this_section,tableid=this_section.type)
        else:
          add_plotssection(page,this_section,tableid=this_section.type)

  #write out the page
  with open(page.outpath,"w") as f:
    f.write(str(page))
